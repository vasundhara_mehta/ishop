//
//  ListGridViewController.m
//  IShop
//
//  Created by Hashim on 5/1/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "ListGridViewController.h"
#import "SWRevealViewController.h"
#import "ListGrid.h"
#import "SearchViewController.h"
#import "AddCartViewController.h"
#import "RefineViewController.h"
#import "MySingletonClass.h"
#import "UIImageView+WebCache.h"
#import "ProductDetailView.h"
#import "RCHBackboard.h"
#import "TopSliderViewController.h"
#import "RegistrationViewController.h"
#import "SignInViewController.h"
#import "FavouriteViewController.h"
#import "Currency_Size_VC.h"
#import "UserProfileViewController.h"
#import "ASIFormDataRequest.h"
#import "UtilityServices.h"
#import "iShopServices.h"
#import "Constants.h"
#import "JSON.h"
#import "DashboardView.h"
#import "CustomCell.h"
#import "CMSPageViewController.h"

#define kNotificationRefreshProductItem @"RefreshProductItem"
#define kNotificationAddFavoriteProduct @"AddFavoriteProduct"


@interface ListGridViewController ()<CurrencyDelegate>

@end

@implementation ListGridViewController
@synthesize myCollectionView;
@synthesize dataArray,tooglBackBtn,tooglBackBtnBottom,pickerView,pickerToolbar,filterTxtField,categoryId,progressHud,bigGridBtn,smallGridBtn;
@synthesize btnSignIn, btnSignOut,arrayRecommended,isRefined,refinedArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    //collection view layOut
    isRecomonded = NO;
    isSearch = NO;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshProductItem:) name:kNotificationRefreshProductItem object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addFavoriteProduct:) name:kNotificationAddFavoriteProduct object:nil];
}

-(IBAction)actionDashboard:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"]){
        DashboardView *dash=[[DashboardView alloc] initWithNibName:@"DashboardView" bundle:nil];
        [self.navigationController pushViewController:dash animated:YES];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign in to save" message:@"You'll need to sign in to view or add to your saved items. Don't worry, you only have to do it once." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 102;
        [alert show];
        
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Refined"] isEqualToString:@"Yes"]) {
        [refineBtn setImage:[UIImage imageNamed:@"RefineCheck.png"] forState:UIControlStateNormal];
    }
    else
    {
        [refineBtn setImage:[UIImage imageNamed:@"Refine.png"] forState:UIControlStateNormal];
    }
    btnCurrency.tag=1001;
    btnSize.tag=2001;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [btnCurrency setTitle:[NSString stringWithFormat:@"CURRENCY: %@",objAppDelegate.CurrentCurrency] forState:UIControlStateNormal];
    NSLog(@"current symbolllll %@ %@",objAppDelegate.CurrentCurrency,objAppDelegate.currencySymbol);
    [self getYourBagItem];
   
    [self.myCollectionView registerClass:[ListGrid class] forCellWithReuseIdentifier:@"ListGrid"];
    [self.myCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    self.myCollectionView.backgroundColor=[UIColor whiteColor];
    
    [self.smallGridBtn setImage:[UIImage imageNamed:@"Item2.png"] forState:UIControlStateNormal];
    [self.smallGridBtn setImage:[UIImage imageNamed:@"Item2Hover.png"] forState:UIControlStateSelected];
    [self.bigGridBtn setImage:[UIImage imageNamed:@"Item1.png"] forState:UIControlStateNormal];
    [self.bigGridBtn setImage:[UIImage imageNamed:@"Item1Hover.png"] forState:UIControlStateSelected];
    isSelectedTypeGird = 1;
    if(objAppDelegate.isSelecteGrid == 1)
    {
        isSelectedTypeGird = 1;
        [self.bigGridBtn setSelected:YES];
        [self.smallGridBtn setSelected:NO];
        [self isSelected1];
    }
    else if(objAppDelegate.isSelecteGrid == 2)
    {
        isSelectedTypeGird = 2;
        [self.bigGridBtn setSelected:NO];
        [self.smallGridBtn setSelected:YES];
        [self isSelected2];
    }
    else if (objAppDelegate.isSelecteGrid == 0)
    {
    }
    
    BOOL boolFromPrefs = [[NSUserDefaults standardUserDefaults] boolForKey:@"isListGrid"];
    
    if(boolFromPrefs == YES)
    {
        categoryId = objAppDelegate.selectedCateId;
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isListGrid"];
    }
    
    NSLog(@"this is category id%@",categoryId);
    
    if (objAppDelegate.ChangeCurrency == NO)
    {
        if (!objAppDelegate.isCheck)
        {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Refined"] isEqualToString:@"Yes"])
            {
                objAppDelegate.dataArray=self.refinedArray;
                [myCollectionView reloadData];
            }
            else
            {
                if (self.doUpdate)
                {
                     [self getTheProductByCategoryFromWebService];
                }
//                [self getTheProductByCategoryFromWebService];
            }

           // [self getTheProductByCategoryFromWebService];
        }
    }
    else
    {
        objAppDelegate.ChangeCurrency = NO;
    }
    //code for reveal view
    revealController = [self revealViewController];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    self.navigationController.navigationBarHidden = YES;
    
    [self.tooglBackBtn setImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
    
    [self.tooglBackBtn addTarget:self action:@selector(getLeftSlideData:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.tooglBackBtnBottom addTarget:self action:@selector(getLeftSlideData:) forControlEvents:UIControlEventTouchUpInside];
    
    if (objAppDelegate.isSelecteGrid == 0)
    {
        //set images on grid button by outlets
        [self.bigGridBtn setSelected:YES];
        [self.smallGridBtn setSelected:NO];
        
        if (!objAppDelegate.isCheck)
        {
            UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
            [flowLayout setItemSize:CGSizeMake(300, 300)];
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
            [self.myCollectionView setCollectionViewLayout:flowLayout];
        }
    }
    
    //topMenuSliderView.hidden = YES;
    CGRect frame = [topMenuSliderView frame];
    frame.origin.y = -323;
    frame.size.width = 320;
    frame.size.height = 378;
    [topMenuSliderView setFrame:frame];
    CGRect frameCollectionView = self.myCollectionView.frame;
    frameCollectionView.origin.y = 111;
    self.myCollectionView.frame = frameCollectionView;
    self.myCollectionView.userInteractionEnabled = YES;
    
    isVisibleTopView = NO;
    // menuView.hidden = YES;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        lblWelcomeuser.text = [NSString stringWithFormat:@"Hi %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"]];
        self.btnSignIn.hidden = YES;
        self.btnSignOut.hidden = NO;
        self.btnJoin.hidden = YES;
        self.btnMyAccount.hidden = NO;
    }
    else
    {
        lblWelcomeuser.text = @"Welcome to iShop";
        self.btnSignIn.hidden = NO;
        self.btnSignOut.hidden = YES;
        self.btnJoin.hidden = NO;
        self.btnMyAccount.hidden = YES;
    }
    if (objAppDelegate.isCheck)
    {
        //[self.progressHud hide:YES];
        [self.myCollectionView reloadData];
    }
    
    arrContaintList = [[NSMutableArray alloc]init];
    arrContaintList = [[NSUserDefaults standardUserDefaults]objectForKey:@"searchProductsList"];
    
    [_tableViewTopContent reloadData];
}

- (void) getLeftSlideData:(id)sender
{
    [UIView beginAnimations:@"animationOff" context:NULL];
    [UIView setAnimationDuration:0.2f];
    //topMenuSliderView.hidden = YES;
    CGRect frame = [topMenuSliderView frame];
    frame.origin.y = -323;
    frame.size.width = 320;
    frame.size.height = 378;
    [topMenuSliderView setFrame:frame];
    CGRect frameCollectionView = self.myCollectionView.frame;
    frameCollectionView.origin.y = 111;
    self.myCollectionView.frame = frameCollectionView;
    self.myCollectionView.userInteractionEnabled = YES;
    
    isVisibleTopView = NO;
    
    [UIView commitAnimations];
    revealController = [self revealViewController];
    [revealController revealToggle:sender];
}

-(void)refreshProductItem:(NSNotification *)notification
{
    [self getYourBagItem];
}

-(void)addFavoriteProduct:(NSNotification *)notification
{
    [self getTheProductByCategoryFromWebService];
    [self.myCollectionView reloadData];
    //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void) getYourBagItem
{
//    lblBagCount.text = [NSString stringWithFormat:@"%i" ,[objAppDelegate.arrAddToBag count]];
//    lblDownBagCount.text = [NSString stringWithFormat:@"%i", [objAppDelegate.arrAddToBag count]];
    int totalItem = 0;
    for(NSMutableDictionary *mDict in objAppDelegate.arrAddToBag)
    {
        totalItem = totalItem + [[ mDict valueForKey:@"prd_qty"]intValue];
    }
    lblDownBagCount.text = [NSString stringWithFormat:@"%d",totalItem];
    lblBagCount.text = [NSString stringWithFormat:@"%d",totalItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [objAppDelegate.dataArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // [self.progressHud hide:YES];
    static NSString *cellIdentifier = @"ListGrid";
    
    ListGrid *cell = (ListGrid *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Refined"] isEqualToString:@"Yes"]) {
        [cell.img setImageWithURL:[NSURL URLWithString:[[objAppDelegate.dataArray valueForKey:@"prd_thumb"]objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"]];

    }else
    [cell.img setImageWithURL:[NSURL URLWithString:[[objAppDelegate.dataArray valueForKey:@"prd_img"]objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"]];
    
    if (isSelectedTypeGird == 1)
    {
        cell.img.contentMode = UIViewContentModeScaleAspectFit;
    }
    else
    {
        cell.img.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    imgProduct = cell.img;
    cell.titlelalbe.text = [[objAppDelegate.dataArray valueForKey:@"prd_name"]objectAtIndex:indexPath.row];
    
    cell.priceLbl.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,[[[objAppDelegate.dataArray valueForKey:@"prd_price"]objectAtIndex:indexPath.row] floatValue]];
    
    //new dev
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        NSString *customer_id  = [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"];
        
        NSString *strPro  =[NSString stringWithFormat:@"%@", [[objAppDelegate.dataArray objectAtIndex:indexPath.row] valueForKey:@"customer_ids"]] ;
        
        if([strPro isEqualToString:@"<null>"])
        {
            [cell.btnFavourite setImage:[UIImage imageNamed:@"star_dark.png"] forState:UIControlStateNormal];
        }
        else
        {
            NSMutableArray *mArraycustomer_id = [[objAppDelegate.dataArray objectAtIndex:indexPath.row] valueForKey:@"customer_ids"];
            
            if ([mArraycustomer_id containsObject:customer_id])
            {
                [cell.btnFavourite setImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.btnFavourite setImage:[UIImage imageNamed:@"star_dark.png"] forState:UIControlStateNormal];
            }
        }
    }
    else
    {
        [cell.btnFavourite setImage:[UIImage imageNamed:@"star_dark.png"] forState:UIControlStateNormal];
    }
    //end new dev
    
    cell.btnFavourite.tag = indexPath.row;
    [cell.btnFavourite addTarget:self action:@selector(btnFavouriteAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (cell.frame.size.width == 146)
    {
        NSLog(@"%f", cell.frame.size.width);
        NSLog(@"%f", cell.frame.size.height);
        cell.titlelalbe.frame = CGRectMake(2, 192, 140, 20);
        cell.priceLbl.frame = CGRectMake(2, 212, 100, 20);
        
        NSLog(@"condition satisfied");
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ProductDetailView *objProductDetailView;
    if (IS_IPHONE5)
    {
        objProductDetailView = [[ProductDetailView alloc] initWithNibName:@"ProductDetailView" bundle:nil];
    }
    else
    {
        objProductDetailView = [[ProductDetailView alloc] initWithNibName:@"ProductDetailView_iphone3.5" bundle:nil];
    }
    
    objProductDetailView.mutDictProductDetail = [objAppDelegate.dataArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:objProductDetailView animated:YES];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    NSLog(@"sizing footer");
    return CGSizeMake(0, 0);
    // return CGSizeMake(50,80);
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
          viewForSupplementaryElementOfKind:(NSString *)kind
                                atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableView;
    if(kind == UICollectionElementKindSectionHeader)
    {
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
    }
    return reusableView;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 10, 0, 10); // top, left, bottom, right
}

#pragma mark - Webservice Methods

-(void)getTheProductByCategoryFromWebService
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    NSString *jsonString;
    if (!objAppDelegate.isCheckSearchType)
    {
        jsonString = [NSString stringWithFormat:@"http://inspirefnb.com/fashionworld/iosapi.php?methodName=getProductsByCatId&cat_id=%@",self.categoryId];
    }
    else
    {
        jsonString = [NSString stringWithFormat:@"http://inspirefnb.com/fashionworld/iosapi.php?methodName=getSearchByName&name=%@&long_description=&short_description=&sku=&",objAppDelegate.strSearchName];
    }
    
    NSString* encodedUrl = [jsonString stringByAddingPercentEscapesUsingEncoding:
                            NSASCIIStringEncoding];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:encodedUrl]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *connectionError)
     {
         // handle response
         objAppDelegate.dataArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        // NSLog(@"this is data --- %@",objAppDelegate.dataArray);
         [hud hide:YES];
         
         if(isRecomonded)
         {
             [self filterByRecomondedAndPrice];
         }
         
         [self RefreshCurrencydelegatemethod:objAppDelegate.currencySymbol withvalue:objAppDelegate.currencyValue];
         
         if ([objAppDelegate.dataArray count] != 0)
         {
             // [self.myCollectionView reloadData];
         }
         else
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"No product found for this category!!!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }
     }];
}

#pragma mark - UIAlertView Deleagte method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"button index --%i", buttonIndex);
    if(alertView.tag == 101 || alertView.tag == 102)
    {
        if (buttonIndex == 0)
        {
            SignInViewController *objSignInViewController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
            [self.navigationController pushViewController:objSignInViewController animated:YES];
        }
        else if(buttonIndex == 1)
        {
            RegistrationViewController *objRegistrationViewController;
            if (IS_IPHONE5)
            {
                objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil];
            }
            else
            {
                objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController_iphone3.5" bundle:nil];
            }
            
            [self.navigationController pushViewController:objRegistrationViewController animated:YES];
        }
    }
    else
    {
        if(buttonIndex == 1)
        {
            objAppDelegate.isUserLogin = NO;
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRemember"];
            self.btnSignIn.hidden = NO;
            self.btnSignOut.hidden = YES;
            self.btnJoin.hidden = NO;
            self.btnMyAccount.hidden = YES;
            lblWelcomeuser.text = @"Welcome to iShop";
        }
    }
}

-(IBAction)btnActionMyAccount:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"]){
        DashboardView *dash=[[DashboardView alloc] initWithNibName:@"DashboardView" bundle:nil];
        [self presentViewController:dash animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign in to save" message:@"You'll need to sign in to view or add to your saved items. Don't worry, you only have to do it once." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 102;
        [alert show];
        
    }

    /*
    UserProfileViewController *objUserProfileViewController;
    if (IS_IPHONE5)
    {
        objUserProfileViewController = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    }
    else
    {
        objUserProfileViewController = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController_iphone3.5" bundle:nil];
    }
    [self.navigationController pushViewController:objUserProfileViewController animated:YES];
     */
}


#pragma mark - CUSTOM method

- (void) signIn
{
    SignInViewController *objSignInViewController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
    [self.navigationController pushViewController:objSignInViewController animated:YES];
}

- (void)ChangeGridIntoSmall
{
    
}

#pragma mark - UIBUtton ACTION method

- (IBAction)topNavigationSliderAction:(id)sender
{
    if (isVisibleTopView)
    {
        [UIView beginAnimations:@"animationOff" context:NULL];
        [UIView setAnimationDuration:0.4f];
        //topMenuSliderView.hidden = NO;
        CGRect frame = [topMenuSliderView frame];
        frame.origin.y = -323;
        [topMenuSliderView setFrame:frame];
        
        CGRect frameCollectionView = self.myCollectionView.frame;
        frameCollectionView.origin.y = 111;
        self.myCollectionView.frame = frameCollectionView;
        self.myCollectionView.userInteractionEnabled = YES;
        //menuView.hidden = YES;
        isVisibleTopView = NO;
        [UIView commitAnimations];
    }
    else
    {
        [UIView beginAnimations:@"animationOff" context:NULL];
        [UIView setAnimationDuration:0.4f];
        //topMenuSliderView.hidden = NO;
        CGRect frame = [topMenuSliderView frame];
        frame.origin.y = 0;
        frame.size.width = 320;//Some value
        frame.size.height = 378;//some value
        [topMenuSliderView setFrame:frame];
        
        CGRect frameCollectionView = self.myCollectionView.frame;
        frameCollectionView.origin.y = 378;
        self.myCollectionView.frame = frameCollectionView;
        self.myCollectionView.userInteractionEnabled = NO;
        
        isVisibleTopView = YES;
        [self.view  bringSubviewToFront:topMenuSliderView];
        [UIView commitAnimations];
        //menuView.hidden = NO;
    }
}

- (IBAction) btnJoinAction:(id)sender
{
    RegistrationViewController *objRegistrationViewController;
    if (IS_IPHONE5)
    {
        objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil];
    }
    else
    {
        objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController_iphone3.5" bundle:nil];
    }
    
    [self.navigationController pushViewController:objRegistrationViewController animated:YES];
}

- (IBAction) btnSignInAction:(id)sender
{
    [UIView beginAnimations:@"animationOff" context:NULL];
    [UIView setAnimationDuration:0.2f];
    //topMenuSliderView.hidden = NO;
    CGRect frame = [topMenuSliderView frame];
    frame.origin.y = -323;
    frame.size.width = 320;
    frame.size.height = 378;
    [topMenuSliderView setFrame:frame];
    CGRect frameCollectionView = self.myCollectionView.frame;
    frameCollectionView.origin.y = 111;
    self.myCollectionView.frame = frameCollectionView;
    isVisibleTopView = NO;
    self.myCollectionView.userInteractionEnabled = YES;
    
    [UIView commitAnimations];
    
    [self performSelector:@selector(signIn) withObject:nil afterDelay:0.5];
}

- (IBAction)btnSignOutAction:(id)sender
{
    objAppDelegate.isUserLogin = NO;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRemember"];
    
    [objAppDelegate.arrAddToBag removeAllObjects];
    //[objAppDelegate.arrFavourite removeAllObjects];
    
    [self getYourBagItem];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"customer_id"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"firstname"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"lastname"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"email"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Do you want to Sign Out?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
}

- (IBAction) button1press:(id)sender
{
    isSelectedTypeGird = 2;
    objAppDelegate.isSelecteGrid = 2;
    
    [self isSelected2];
}

-(void) isSelected2
{
    [self.smallGridBtn setSelected:YES];
    [self.bigGridBtn setSelected:NO];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(146, 232)];
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
    flowLayout.minimumInteritemSpacing = 0.0f;
    flowLayout.minimumLineSpacing = 9.0f;
    
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.myCollectionView setCollectionViewLayout:flowLayout];
    [self.myCollectionView reloadData];
}

- (IBAction)button2Press:(id)sender
{
    isSelectedTypeGird = 1;
    objAppDelegate.isSelecteGrid = 1;
    [self isSelected1];
}

-(void) isSelected1
{
    isSelectedTypeGird = 1;
    [self.smallGridBtn setSelected:NO];
    [self.bigGridBtn setSelected:YES];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(300, 300)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.myCollectionView setCollectionViewLayout:flowLayout];
    [self.myCollectionView reloadData];
}

- (IBAction)SeacrhButtonPress:(id)sender
{
    isSearch = YES;
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    objAppDelegate.isCheckSearchType = YES;
    searchView = (SearchView *)[[[NSBundle mainBundle]loadNibNamed:@"SearchView" owner:self options:nil]objectAtIndex:0 ];
    [searchView.btnHideView addTarget:self action:@selector(hideSearchView:) forControlEvents:UIControlEventTouchUpInside];
    
    searchView.txtSearch.delegate = self;
    searchView.tableViewSearchResult.delegate = self;
    searchView.tableViewSearchResult.dataSource = self;
    [self.view addSubview:searchView];
    [searchView.tableViewSearchResult reloadData];

}
-(IBAction)hideSearchView:(id)sender
{
    isSearch = NO;
    [searchView removeFromSuperview];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter a search keyword" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    
    [textField resignFirstResponder];
    
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    objAppDelegate.isCheckSearchType = YES;
    objAppDelegate.strSearchName = textField.text;
    
    if(arrContaintList == nil)
    {
        arrContaintList = [[NSMutableArray alloc]init];
    }
    [arrContaintList addObject:textField.text];
    
    [[NSUserDefaults standardUserDefaults]setObject:arrContaintList forKey:@"searchProductsList"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self getTheProductByCategoryFromWebService];
    [self performSelector:@selector(hideSearchView:) withObject:nil];

    return YES;
}

#pragma mark - UITableView DataSource

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(isSearch)
    {
        return 44;
    }
    else
    {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(isSearch)
    {
        if([arrContaintList count] > 0)
        {
            UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
            viewHeader.backgroundColor = [UIColor whiteColor];
            
            UILabel *lblHeaderTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 222, 21)];
            lblHeaderTitle.text = @"YOUR RECENT SEARCHES:";
            lblHeaderTitle.font = [UIFont systemFontOfSize:11];
            lblHeaderTitle.textColor = [UIColor blackColor];
            [viewHeader addSubview:lblHeaderTitle];
            
            UIButton *btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
            btnClear.frame = CGRectMake(235, 5, 60, 23);
            [btnClear setImage:[UIImage imageNamed:@"clear_r"] forState:UIControlStateNormal];
            [btnClear addTarget:self action:@selector(actionClearSearchList:) forControlEvents:UIControlEventTouchUpInside];
            [viewHeader addSubview:btnClear];
            return viewHeader;
        }
        return nil;
    }
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isSearch)
    {
        return [arrContaintList count];
    }
    return 1 + [objAppDelegate.mArrayCMSPages count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isSearch)
    {
        static NSString *CellIdentifier = nil;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
        cell.textLabel.text = [arrContaintList objectAtIndex:indexPath.row] ;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        return cell;
    }
    else
    {
        static NSString *cellIdentifier = @"CMSContent";
        CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            NSArray *cellView = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
            cell = (CustomCell *)[cellView objectAtIndex:2];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
        //cell.accessoryType = UITableViewCellAccessoryCheckmark;
        if(indexPath.row == 0)
        {
            cell.lblPrdAttibuteTitle.text = [[NSString stringWithFormat:@"CURRENCY: %@",objAppDelegate.CurrentCurrency] uppercaseString];
            [cell.btnCell addTarget:self action:@selector(btnChangeCurrency:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.lblPrdAttibuteTitle.text = [[[objAppDelegate.mArrayCMSPages objectAtIndex:indexPath.row - 1]valueForKey:@"title"]uppercaseString];
            cell.btnCell.tag = indexPath.row - 1;
            [cell.btnCell addTarget:self action:@selector(actionGoToCMSPages:) forControlEvents:UIControlEventTouchUpInside];
        }
        return cell;
    }
}

#pragma mark - UITableView Delegates

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isSearch)
    {
        objAppDelegate.strSearchName = [arrContaintList objectAtIndex:indexPath.row];
        objAppDelegate.isCheckSearchType = YES;
        [self getTheProductByCategoryFromWebService];
        [self performSelector:@selector(hideSearchView:) withObject:nil];
    }
    else
    {
//        if(indexPath.row == 0)
//        {
//            Currency_Size_VC *currency_Size_view = [[Currency_Size_VC alloc] initWithNibName:@"Currency_Size_VC" bundle:nil];
//            currency_Size_view.delegateCurrency=self;
//            currency_Size_view.isSelectedValue = 1001;
//            currency_Size_view.delegate = self;
//            [self.navigationController pushViewController:currency_Size_view animated:YES];
//        }
//        else
//        {
//            CMSPageViewController *cMSPageViewController = [[CMSPageViewController alloc]initWithNibName:@"CMSPageViewController" bundle:nil];
//            
//            cMSPageViewController.strTitle = [[[objAppDelegate.mArrayCMSPages objectAtIndex:indexPath.row - 1]valueForKey:@"title"]uppercaseString];
//            cMSPageViewController.strDescreption = [[objAppDelegate.mArrayCMSPages objectAtIndex:indexPath.row - 1]valueForKey:@"content"];
//            [self.navigationController pushViewController:cMSPageViewController animated:YES];
//        }
    }
}

-(IBAction)actionGoToCMSPages:(id)sender
{
    CMSPageViewController *cMSPageViewController = [[CMSPageViewController alloc]initWithNibName:@"CMSPageViewController" bundle:nil];
    cMSPageViewController.strTitle = [[[objAppDelegate.mArrayCMSPages objectAtIndex:[sender tag]]valueForKey:@"title"]uppercaseString];
    cMSPageViewController.strDescreption = [[objAppDelegate.mArrayCMSPages objectAtIndex:[sender tag]]valueForKey:@"content"];
    [self.navigationController pushViewController:cMSPageViewController animated:YES];
}

-(IBAction)actionClearSearchList:(id)sender
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"searchProductsList"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    arrContaintList = [[NSMutableArray alloc]init];
    [searchView.tableViewSearchResult reloadData];
}

- (IBAction) btnAddToBagAction:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        AddCartViewController *addCart = [[AddCartViewController alloc]initWithNibName:@"AddCartViewController" bundle:nil];
        [self.navigationController pushViewController:addCart animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Please Sign In" message:@"You'll need to sign in to view your bag and make purchase. Don't worry, you only have to do it once." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 101;
        [alert show];
    }
}

- (IBAction)ShowPicker:(id)sender
{
    [self OpenThePickerView];
}

- (IBAction)refineButtonPress:(id)sender
{
    RefineViewController *refine = [[RefineViewController alloc]initWithNibName:@"RefineViewController" bundle:nil];
    refine.delegate=self;
    [self.navigationController pushViewController:refine animated:YES];
}

- (IBAction)btnCancelToolbarAction:(id)sender
{
    self.pickerView.hidden = YES;
    self.pickerToolbar.hidden = YES;
    myCollectionView.userInteractionEnabled = YES;
}

- (IBAction)btnDoneToolbarAction:(id)sender
{
    self.pickerView.hidden = YES;
    self.pickerToolbar.hidden = YES;
    myCollectionView.userInteractionEnabled = YES;
    
    [self getTheProductByCategoryFromWebService];
    
    isRecomonded = YES;
}

-(void)filterByRecomondedAndPrice
{
    NSSortDescriptor *sortDescriptor;
    
    switch (selectRow)
    {
        case 0:
        {
            sortDescriptor = [[NSSortDescriptor alloc]
                              initWithKey:@"product_recommended" ascending:NO];
        }
            break;
        case 1:
        {
            sortDescriptor = [[NSSortDescriptor alloc]
                              initWithKey:@"product_update_date" ascending:NO];
        }
            break;
        case 2:
        {
            sortDescriptor = [[NSSortDescriptor alloc]
                              initWithKey:@"prd_price" ascending:NO];
        }
            break;
        case 3:
        {
            sortDescriptor = [[NSSortDescriptor alloc]
                              initWithKey:@"prd_price" ascending:YES];
        }
            break;
        default:
            break;
    }
    [objAppDelegate.dataArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [self.myCollectionView reloadData];
}
- (IBAction)btnSaveItemAction:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        FavouriteViewController *objFavouriteViewController = [[FavouriteViewController alloc]initWithNibName:@"FavouriteViewController" bundle:nil];
        [self presentViewController:objFavouriteViewController animated:YES completion:nil];    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign in to save" message:@"You'll need to sign in to view or add to your saved items. Don't worry, you only have to do it once." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 102;
        [alert show];
    }
}

- (IBAction) btnHelpAction:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://inspirefnb.com/fashionworld/help"]];
}

- (IBAction) btnReportAction:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://inspirefnb.com/fashionworld/report"]];
}


- (IBAction) btnFavouriteAction:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Please wait...";
        hud.dimBackground = YES;
        
        NSString *product_id = [[objAppDelegate.dataArray objectAtIndex:[sender tag]]objectForKey:@"prd_id"];
        
        NSMutableDictionary *mDictProduct = [objAppDelegate.dataArray objectAtIndex:[sender tag]];
      
        NSString *add_remove_Favorite = @"0";
        randomNumber = arc4random() % 999999;
        if (![self matchFoundForEventId:product_id WithArray:objAppDelegate.arrFavourite])
        {
            //[objAppDelegate.arrFavourite addObject:[objAppDelegate.dataArray objectAtIndex:[sender tag]]];
            add_remove_Favorite = @"1";
        }
        
       // NSLog(@"Random No = %d",randomNumber);
        
        NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"prd_qty":[mDictProduct objectForKey:@"prd_qty"],@"product_id":product_id,@"prd_default_price":[mDictProduct valueForKey:@"prd_price"],@"prd_update_price":[mDictProduct valueForKey:@"prd_price"],@"uniq_id":[NSString stringWithFormat:@"%d",randomNumber],@"get":add_remove_Favorite};
        
        [iShopServices PostMethodWithApiMethod:@"GetUserFavoriteHistory" Withparms:parameters WithSuccess:^(id response)
         {
             // NSLog(@"Response = %@",[response JSONValue]);
             objAppDelegate.requestFor = ADDFAVORITES;
             [objAppDelegate getFavoriteHistory];
             //self.arrAddToBag = [response JSONValue];
         } failure:^(NSError *error)
         {
             NSLog(@"Error =%@",[error description]);
         }];
        
        
//        NSURL *url = [NSURL URLWithString:get_user_favorite];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
//        request.timeOutSeconds = 120;
//        
//        [request setPostValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"] forKey:@"customer_id"];
//        [request setPostValue:product_id forKey:@"product_id"];
//        [request setPostValue:add_remove_Favorite forKey:@"get"];
//        
//        [request setDidFinishSelector:@selector(requestFinishedForService:)];
//        [request setDidFailSelector:@selector(requestFailed:)];
//        [request setDelegate:self];
//        [request startAsynchronous];
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign in to save" message:@"You'll need to sign in to view or add to your saved items. Don't worry, you only have to do it once." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 102;
        [alert show];
    }
}

- (BOOL)matchFoundForEventId:(NSString *)eventId WithArray:(NSMutableArray *)ConditionArray
{
    index = 0;
    for (NSDictionary *dataDict in ConditionArray)
    {
        NSString *createId = [dataDict objectForKey:@"prd_id"];
        if ([eventId isEqualToString:createId])
        {
            randomNumber = [[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"uniq_id"]]intValue];
            return YES;
        }
        index++;
    }
    return NO;
}


#pragma mark - OpenAndCreatePickerView

- (void)OpenThePickerView
{
    myCollectionView.userInteractionEnabled = NO;
    self.arrayRecommended = [[NSMutableArray alloc] initWithObjects:nil];
	NSDictionary *dic1 = [[NSDictionary alloc] initWithObjectsAndKeys:@"Recommended", @"name",  nil];
	NSDictionary *dic2 = [[NSDictionary alloc] initWithObjectsAndKeys:@"What's New", @"name",  nil];
	NSDictionary *dic3 = [[NSDictionary alloc] initWithObjectsAndKeys:@"Price - High to Low", @"name",  nil];
	NSDictionary *dic4 = [[NSDictionary alloc] initWithObjectsAndKeys:@"Price - Low to High", @"name",  nil];
	
	[self.arrayRecommended addObject:dic1];
	[self.arrayRecommended addObject:dic2];
	[self.arrayRecommended addObject:dic3];
	[self.arrayRecommended addObject:dic4];
	[self.pickerView reloadAllComponents];
    
    self.pickerView.hidden = NO;
    self.pickerToolbar.hidden = NO;
}

- (IBAction)dismissActionSheet:(id)sender
{
    [self.actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark - UIPickerView DataSource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.arrayRecommended count];
}

#pragma mark - UIPickerView Delegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30.0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, self.pickerView.frame.size.width, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:16];
    
    NSString *strTitle = [[self.arrayRecommended objectAtIndex:row] objectForKey:@"name"];
    
    label.text = [NSString stringWithFormat:@"%@", strTitle];
    return label;
}

//If the user chooses from the pickerview, it calls this function;
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //Let's print in the console what the user had chosen;
    lblRecommended.text = [[self.arrayRecommended objectAtIndex:row] objectForKey:@"name"];
    if([[[self.arrayRecommended objectAtIndex:row] objectForKey:@"name"] isEqualToString:@"Price - High to Low"])
    {
        lblRecommended.text = @"High to Low";
    }
    else if ([[[self.arrayRecommended objectAtIndex:row] objectForKey:@"name"] isEqualToString:@"Price - Low to High"])
    {
        lblRecommended.text = @"Low to High";
    }
    
   // NSLog(@"row -- %i",row);
    selectRow = row;
}

#pragma mark - UITestField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.filterTxtField.inputAccessoryView = self.pickerToolbar;
    if(!isSearch)
    {
        self.pickerView.hidden=NO;
    }
    self.filterTxtField.inputView=self.pickerView;
}
- (IBAction)btnChangeCurrency:(id)sender
{
    Currency_Size_VC *Currency_Size_view = [[Currency_Size_VC alloc] initWithNibName:@"Currency_Size_VC" bundle:nil];
    Currency_Size_view.delegate=self;
    Currency_Size_view.delegateCurrency=self;
    Currency_Size_view.isSelectedValue = [sender tag];
    [self.navigationController pushViewController:Currency_Size_view animated:YES];
}

- (IBAction)btnChangeSize:(id)sender
{
    Currency_Size_VC *Currency_Size_view = [[Currency_Size_VC alloc] initWithNibName:@"Currency_Size_VC" bundle:nil];
    Currency_Size_view.delegateCurrency=self;
    Currency_Size_view.isSelectedValue = [sender tag];
    [self.navigationController pushViewController:Currency_Size_view animated:YES];
}

-(void)RefreshCurrencydelegatemethod:(NSString *)symbol withvalue:(NSString *)value
{
    for (int i=0; i<objAppDelegate.dataArray.count; i++)
    {
        NSString *prod_price=[NSString stringWithFormat:@"%@",[[objAppDelegate.dataArray objectAtIndex:i] objectForKey:@"prd_price"]];
        CGFloat final_price=[prod_price floatValue]*[value floatValue];
        [[objAppDelegate.dataArray objectAtIndex:i] setValue:[NSNumber numberWithFloat:final_price] forKey:@"prd_price"];
    }
    
    [myCollectionView reloadData];
}
@end
