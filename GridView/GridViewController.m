//
//  GridViewController.m
//  IShop
//
//  Created by Hashim on 4/29/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "GridViewController.h"
#import "SWRevealViewController.h"
#import "GridCell.h"
#import "SearchViewController.h"
#import "AddCartViewController.h"
#import "UIImageView+WebCache.h"
#import "MySingletonClass.h"
#import "ListGridViewController.h"
#import "RegistrationViewController.h"
#import "SignInViewController.h"
#import "FavouriteViewController.h"
#import "Currency_Size_VC.h"
#import "UserProfileViewController.h"
#import "SearchView.h"
#import "CustomCell.h"
#import "CMSPageViewController.h"
#import "DashboardView.h"

#define kNotificationRefreshProductItem @"RefreshProductItem"



@interface GridViewController ()<CurrencyDelegate>

@end

@implementation GridViewController
@synthesize myCollectionView;
@synthesize dataArray,tooglBackBtn,progressHud;
@synthesize btnSignIn, btnSignOut,tooglBackBtnBottom, btnJoin, btnMyAccount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // alloc MBProgressHud
    self.progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHud.labelText = @"Please wait...";
    self.progressHud.dimBackground = YES;
   
    isSearch = NO;

    [self getTheImagesFromWebServer];
    
    //Reveal view
    revealController = [self revealViewController];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    self.navigationController.navigationBarHidden = YES;
    [self.tooglBackBtn setImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
    [self.tooglBackBtn addTarget:self action:@selector(getLeftSlideData:) forControlEvents:UIControlEventTouchUpInside];
    [self.tooglBackBtnBottom addTarget:self action:@selector(getLeftSlideData:) forControlEvents:UIControlEventTouchUpInside];

    //collection view code
    [self.myCollectionView registerClass:[GridCell class] forCellWithReuseIdentifier:@"GridCell"];
    self.myCollectionView.backgroundColor = [UIColor whiteColor];
    [self.myCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    [self.myCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
    
    /* end of subclass-based cells block */
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(320, 357)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.myCollectionView setCollectionViewLayout:flowLayout];
    
     NSLog(@"--Bool--%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"cart_array"]);
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshProductItem:) name:kNotificationRefreshProductItem object:nil];
    
    arrContaintList = [[NSMutableArray alloc]init];
    arrContaintList = [[NSUserDefaults standardUserDefaults]objectForKey:@"searchProductsList"];
}

-(void)refreshProductItem:(NSNotification *)notification
{
    [self getYourBagItem];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    //topMenuSliderView.hidden = YES;
    CGRect frame = [topMenuSliderView frame];
    frame.origin.y = -323;
    frame.size.width = 320;
    frame.size.height = 378;
    [topMenuSliderView setFrame:frame];
    CGRect frameCollectionView = self.myCollectionView.frame;
    frameCollectionView.origin.y = 53;
    self.myCollectionView.frame = frameCollectionView;
    self.myCollectionView.userInteractionEnabled = YES;

    isVisibleTopView = NO;
   // menuView.hidden = YES;
    
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"--Bool--%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"firstname"]);

    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        lblWelcomeuser.text = [NSString stringWithFormat:@"Hi %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"]];
        self.btnSignIn.hidden = YES;
        self.btnSignOut.hidden = NO;
        self.btnJoin.hidden = YES;
        self.btnMyAccount.hidden = NO;
    }
    else
    {
        lblWelcomeuser.text = @"Welcome to iShop";
        self.btnSignIn.hidden = NO;
        self.btnSignOut.hidden = YES;
        self.btnJoin.hidden = NO;
        self.btnMyAccount.hidden = YES;
    }
    
    [btnCurrency setTitle:[NSString stringWithFormat:@"CURRENCY: %@",objAppDelegate.CurrentCurrency] forState:UIControlStateNormal];
    [self getYourBagItem];
    [_tableViewTopContent reloadData];

}

- (void) getLeftSlideData:(id)sender
{
    //topMenuSliderView.hidden = NO;
    CGRect frame = [topMenuSliderView frame];
    frame.origin.y = -323;
    frame.size.width = 320;
    frame.size.height = 378;
    [topMenuSliderView setFrame:frame];
    CGRect frameCollectionView = self.myCollectionView.frame;
    frameCollectionView.origin.y = 53;
    self.myCollectionView.frame = frameCollectionView;
    self.myCollectionView.userInteractionEnabled = YES;

   // menuView.hidden = YES;
    isVisibleTopView = NO;
    
    revealController = [self revealViewController];
    [revealController revealToggle:sender];
    [UIView commitAnimations];
}

-(void) getYourBagItem
{
//    lblBagCount.text = [NSString stringWithFormat:@"%i" ,[objAppDelegate.arrAddToBag count]];
//    lblDownBagCount.text = [NSString stringWithFormat:@"%i", [objAppDelegate.arrAddToBag count]];
    int totalItem = 0;
    for(NSMutableDictionary *mDict in objAppDelegate.arrAddToBag)
    {
        totalItem = totalItem + [[ mDict valueForKey:@"prd_qty"]intValue];
    }
    lblDownBagCount.text = [NSString stringWithFormat:@"%d",totalItem];
    lblBagCount.text = [NSString stringWithFormat:@"%d",totalItem];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self.myCollectionView = nil;
    self.dataArray = nil;
    // Dispose of any resources that can be recreated.
}

#pragma mark - webservice calling for product Images

-(void)getTheImagesFromWebServer
{
    NSString *jsonString=[NSString stringWithFormat:@"getSpleshScreen"];
    NSLog(@"this is json string%@",jsonString);
    
    [[MySingletonClass sharedSingleton] getDataFromJson:jsonString getData:^(NSArray *data,NSError *error)
    {
        if (error)
        {
            [self.progressHud hide:YES];
        }
        else
        {
            NSLog(@"this is data%@",data);
            self.dataArray = data;
            [self.progressHud hide:YES];
            [self.myCollectionView reloadData];
        }
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"GridCell";
   
    GridCell *cell =(GridCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *strUrl = [[self.dataArray valueForKey:@"url"]objectAtIndex:indexPath.row];
    
    NSURL *url = [[NSURL alloc] initWithString:strUrl];
    [cell.img setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"]];
    cell.img.contentMode = UIViewContentModeScaleAspectFill;
    
    cell.titlelalbe.text=[[self.dataArray valueForKey:@"cat_name"] objectAtIndex:indexPath.row];
    //  cell.img.contentMode=UIViewContentModeScaleToFill;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForFooterInSection:(NSInteger)section
{
    NSLog(@"sizing footer");
     return CGSizeMake(0,0);
 //   return CGSizeMake(300,300);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableView;
    if(kind == UICollectionElementKindSectionHeader)
    {
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
    } else {
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        reusableView.backgroundColor=[UIColor yellowColor];
        UIView *footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 500)];
        //  UIButton *freeDeliveryBtn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        footerView.backgroundColor=[UIColor whiteColor];
        [reusableView addSubview:footerView];
    }
    return reusableView;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Refined"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Desc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShortDesc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Sku"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MinPrice"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MaxPrice"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    revealController = [self revealViewController];
    ListGridViewController *listView = [[ListGridViewController alloc] init];
    listView.categoryId =[[self.dataArray valueForKey:@"cat_id"] objectAtIndex:indexPath.row];
    listView.doUpdate = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listView];
    [revealController pushFrontViewController:navigationController animated:YES];
}

#pragma mark - Action method

- (IBAction)topNavigationSliderAction:(id)sender
{
    if (isVisibleTopView)
    {
        [UIView beginAnimations:@"animationOff" context:NULL];
        [UIView setAnimationDuration:0.4f];
        //topMenuSliderView.hidden = NO;
        CGRect frame = [topMenuSliderView frame];
        frame.origin.y = -323;
        [topMenuSliderView setFrame:frame];
        
        CGRect frameCollectionView = self.myCollectionView.frame;
        frameCollectionView.origin.y = 53;
        self.myCollectionView.frame = frameCollectionView;
        self.myCollectionView.userInteractionEnabled = YES;

        //menuView.hidden = YES;
        isVisibleTopView = NO;
        [UIView commitAnimations];
    }
    else
    {
        [UIView beginAnimations:@"animationOff" context:NULL];
        [UIView setAnimationDuration:0.4f];
        //topMenuSliderView.hidden = NO;
        CGRect frame = [topMenuSliderView frame];
        frame.origin.y = 0;
        frame.size.width = 320;//Some value
        frame.size.height = 378;//some value
        [topMenuSliderView setFrame:frame];

        CGRect frameCollectionView = self.myCollectionView.frame;
        frameCollectionView.origin.y = 378;
        self.myCollectionView.frame = frameCollectionView;
        self.myCollectionView.userInteractionEnabled = NO;

        isVisibleTopView = YES;
        [self.view  bringSubviewToFront:topMenuSliderView];
        [UIView commitAnimations];
        //menuView.hidden = NO;
    }
}

- (IBAction)btnJoinAction:(id)sender
{
    RegistrationViewController *objRegistrationViewController;
    if (IS_IPHONE5)
    {
        objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil];
    }
    else
    {
        objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController_iphone3.5" bundle:nil];
    }
    [self.navigationController pushViewController:objRegistrationViewController animated:YES];
}

-(IBAction)btnActionMyAccount:(id)sender
{
    
    DashboardView *dash=[[DashboardView alloc] initWithNibName:@"DashboardView" bundle:nil];
    [self presentViewController:dash animated:YES completion:nil];

    /*
    UserProfileViewController *objUserProfileViewController;
    if (IS_IPHONE5)
    {
        objUserProfileViewController = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    }
    else
    {
        objUserProfileViewController = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController_iphone3.5" bundle:nil];
    }
    [self.navigationController pushViewController:objUserProfileViewController animated:YES];
     */
}

- (IBAction)btnSignInAction:(id)sender
{
    //topMenuSliderView.hidden = NO;
    CGRect frame = [topMenuSliderView frame];
    frame.origin.y = -323;
    frame.size.width = 320;
    frame.size.height = 378;
    [topMenuSliderView setFrame:frame];
    CGRect frameCollectionView = self.myCollectionView.frame;
    frameCollectionView.origin.y = 53;
    self.myCollectionView.frame = frameCollectionView;
    self.myCollectionView.userInteractionEnabled = YES;

   // menuView.hidden = YES;
    isVisibleTopView = NO;
    [UIView commitAnimations];

    [self performSelector:@selector(signIn) withObject:nil afterDelay:0.5];
}

- (void) signIn
{
    SignInViewController *objSignInViewController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
    [self.navigationController pushViewController:objSignInViewController animated:YES];
}

- (IBAction)btnSignOutAction:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRemember"];
    
    [objAppDelegate.arrAddToBag removeAllObjects];
   // [objAppDelegate.arrFavourite removeAllObjects];
    [self getYourBagItem];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"customer_id"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"firstname"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"lastname"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"email"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Do you want to Sign Out?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil]; 
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101 || alertView.tag == 102)
    {
        if (buttonIndex == 0)
        {
            SignInViewController *objSignInViewController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
            [self.navigationController pushViewController:objSignInViewController animated:YES];
        }
        else if(buttonIndex == 1)
        {
            RegistrationViewController *objRegistrationViewController;
            if (IS_IPHONE5)
            {
                objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil];
            }
            else
            {
                objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController_iphone3.5" bundle:nil];
            }
            [self.navigationController pushViewController:objRegistrationViewController animated:YES];
        }
    }
    else
    {
        if(buttonIndex == 1)
        {
            objAppDelegate.isUserLogin = NO;
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRemember"];
            self.btnSignIn.hidden = NO;
            self.btnSignOut.hidden = YES;
            self.btnJoin.hidden = NO;
            self.btnMyAccount.hidden = YES;
            lblWelcomeuser.text = @"Welcome to iShop";
        }
    }
}

-(IBAction)button1press:(id)sender
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(150,150)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.myCollectionView setCollectionViewLayout:flowLayout];
}

-(IBAction)button2Press:(id)sender
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(281, 318)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.myCollectionView setCollectionViewLayout:flowLayout];
}

-(IBAction)SeacrhButtonPress:(id)sender
{
    isSearch = YES;
    searchView = (SearchView *)[[[NSBundle mainBundle]loadNibNamed:@"SearchView" owner:self options:nil]objectAtIndex:0 ];
    [searchView.btnHideView addTarget:self action:@selector(hideSearchView:) forControlEvents:UIControlEventTouchUpInside];
    
    searchView.txtSearch.delegate = self;
    searchView.tableViewSearchResult.delegate = self;
    searchView.tableViewSearchResult.dataSource = self;
    [self.view addSubview:searchView];
    [searchView.tableViewSearchResult reloadData];
}

-(IBAction)hideSearchView:(id)sender
{
    isSearch = NO;
    objAppDelegate.isCheckSearchType = NO;
    [searchView removeFromSuperview];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter a search keyword" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    objAppDelegate.isCheckSearchType = YES;
    
    [textField resignFirstResponder];
    
    objAppDelegate.strSearchName = textField.text;
    
    if(arrContaintList == nil)
    {
        arrContaintList = [[NSMutableArray alloc]init];
    }
    [arrContaintList addObject:textField.text];
    
    [[NSUserDefaults standardUserDefaults]setObject:arrContaintList forKey:@"searchProductsList"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Refined"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Desc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShortDesc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Sku"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MinPrice"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MaxPrice"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    ListGridViewController *listGridViewController = [[ListGridViewController alloc]initWithNibName:@"ListGridViewController" bundle:nil];
    listGridViewController.doUpdate = YES;
    [self.navigationController pushViewController:listGridViewController animated:YES];
    return YES;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(isSearch)
    {
        return 44;
    }
    else
    {
        return 0;
    }
}

#pragma mark - UITableView DataSource

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(isSearch)
    {
    if([arrContaintList count] > 0)
    {
        UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
        viewHeader.backgroundColor = [UIColor whiteColor];
        
        UILabel *lblHeaderTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 222, 21)];
        lblHeaderTitle.text = @"YOUR RECENT SEARCHES:";
        lblHeaderTitle.font = [UIFont systemFontOfSize:11];
        lblHeaderTitle.textColor = [UIColor blackColor];
        [viewHeader addSubview:lblHeaderTitle];
        
        UIButton *btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
        btnClear.frame = CGRectMake(235, 5, 60, 23);
        [btnClear setImage:[UIImage imageNamed:@"clear_r"] forState:UIControlStateNormal];
        [btnClear addTarget:self action:@selector(actionClearSearchList:) forControlEvents:UIControlEventTouchUpInside];
        [viewHeader addSubview:btnClear];
        return viewHeader;
    }
    return nil;
    }
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isSearch)
    {
        return [arrContaintList count];
    }
    return 1 + [objAppDelegate.mArrayCMSPages count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isSearch)
    {
        static NSString *CellIdentifier = nil;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
        cell.textLabel.text = [arrContaintList objectAtIndex:indexPath.row] ;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        return cell;
    }
    else
    {
            static NSString *cellIdentifier = @"CMSContent";
            CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil)
            {
                NSArray *cellView = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
                cell = (CustomCell *)[cellView objectAtIndex:2];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                cell.contentView.backgroundColor = [UIColor clearColor];
            }
            //cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
            if(indexPath.row == 0)
            {
                cell.lblPrdAttibuteTitle.text = [[NSString stringWithFormat:@"CURRENCY: %@",objAppDelegate.CurrentCurrency] uppercaseString];
                [cell.btnCell addTarget:self action:@selector(btnChangeCurrency:) forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                cell.lblPrdAttibuteTitle.text = [[[objAppDelegate.mArrayCMSPages objectAtIndex:indexPath.row - 1]valueForKey:@"title"]uppercaseString];
                cell.btnCell.tag = indexPath.row - 1;
                [cell.btnCell addTarget:self action:@selector(actionGoToCMSPages:) forControlEvents:UIControlEventTouchUpInside];

            }
            return cell;
    }
}

#pragma mark - UITableView Delegates

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Refined"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Desc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShortDesc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Sku"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MinPrice"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MaxPrice"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    if(isSearch)
    {
        objAppDelegate.strSearchName = [arrContaintList objectAtIndex:indexPath.row];
        ListGridViewController *listGridViewController = [[ListGridViewController alloc]initWithNibName:@"ListGridViewController" bundle:nil];
        listGridViewController.doUpdate = YES;
        [self.navigationController pushViewController:listGridViewController animated:YES];
    }
    else
    {
        if(indexPath.row == 0)
        {
           
            
            Currency_Size_VC *Currency_Size_view = [[Currency_Size_VC alloc] initWithNibName:@"Currency_Size_VC" bundle:nil];
            Currency_Size_view.delegateCurrency=self;
            Currency_Size_view.isSelectedValue=1001;
            [self.navigationController pushViewController:Currency_Size_view animated:YES];
            
        }
        else
        {
            CMSPageViewController *cMSPageViewController = [[CMSPageViewController alloc]initWithNibName:@"CMSPageViewController" bundle:nil];
            
            cMSPageViewController.strTitle = [[[objAppDelegate.mArrayCMSPages objectAtIndex:indexPath.row - 1]valueForKey:@"title"]uppercaseString];
            cMSPageViewController.strDescreption = [[objAppDelegate.mArrayCMSPages objectAtIndex:indexPath.row - 1]valueForKey:@"content"];
            [self.navigationController pushViewController:cMSPageViewController animated:YES];
        }
    }
}

-(IBAction)actionGoToCMSPages:(id)sender
{
    CMSPageViewController *cMSPageViewController = [[CMSPageViewController alloc]initWithNibName:@"CMSPageViewController" bundle:nil];
    
    cMSPageViewController.strTitle = [[[objAppDelegate.mArrayCMSPages objectAtIndex:[sender tag]]valueForKey:@"title"]uppercaseString];
    cMSPageViewController.strDescreption = [[objAppDelegate.mArrayCMSPages objectAtIndex:[sender tag]]valueForKey:@"content"];
    [self.navigationController pushViewController:cMSPageViewController animated:YES];
}


-(IBAction)actionClearSearchList:(id)sender
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"searchProductsList"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    arrContaintList = [[NSMutableArray alloc]init];
    [searchView.tableViewSearchResult reloadData];
}

-(IBAction)addCartButtonPress:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        AddCartViewController *addCart = [[AddCartViewController alloc]initWithNibName:@"AddCartViewController" bundle:nil];
        [self.navigationController pushViewController:addCart animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Please Sign in" message:@"You'll need to sign in to view your bag and make purchase. Don't worry, you only have to do it once." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 101;
        [alert show];
    }
}

- (IBAction)btnSaveItemAction:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        FavouriteViewController *objFavouriteViewController = [[FavouriteViewController alloc]initWithNibName:@"FavouriteViewController" bundle:nil];
        [self presentViewController:objFavouriteViewController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign in to save" message:@"You'll need to sign in to view or add to your saved items. Don't worry, you only have to do it once." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 102;
        [alert show];
    }
}

- (IBAction) btnHelpAction:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://inspirefnb.com/fashionworld/help"]];
}

- (IBAction) btnReportAction:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://inspirefnb.com/fashionworld/report"]];
}

- (IBAction)btnChangeCurrency:(id)sender
{
    Currency_Size_VC *Currency_Size_view = [[Currency_Size_VC alloc] initWithNibName:@"Currency_Size_VC" bundle:nil];
    Currency_Size_view.delegateCurrency=self;
    Currency_Size_view.isSelectedValue=1001;
    [self.navigationController pushViewController:Currency_Size_view animated:YES];
}

- (IBAction)btnChangeSize:(id)sender
{
    Currency_Size_VC *Currency_Size_view = [[Currency_Size_VC alloc] initWithNibName:@"Currency_Size_VC" bundle:nil];
     Currency_Size_view.delegateCurrency=self;
    Currency_Size_view.isSelectedValue=[sender tag];
    [self.navigationController pushViewController:Currency_Size_view animated:YES];
}
-(void)RefreshCurrencydelegatemethod:(NSString *)symbol withvalue:(NSString *)value
{
    /*
    for (int i=0; i<objAppDelegate.dataArray.count; i++)
    {
        
        NSString *prod_price=[NSString stringWithFormat:@"%@",[[objAppDelegate.dataArray objectAtIndex:i] objectForKey:@"prd_price"]];
        CGFloat final_price=[prod_price floatValue]*[objAppDelegate.currencyValue floatValue];
        
        [[objAppDelegate.dataArray objectAtIndex:i]setObject:[NSString stringWithFormat:@"%f",final_price] forKey:@"prd_price"];
        
    }
     */
}
@end
