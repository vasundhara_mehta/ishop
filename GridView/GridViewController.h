//
//  GridViewController.h
//  IShop
//
//  Created by Hashim on 4/29/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "SearchView.h"

@interface GridViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,MBProgressHUDDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet UIView *topMenuSliderView;
    IBOutlet UIView *menuView;
    BOOL isVisibleTopView;
    AppDelegate *objAppDelegate;
    
    IBOutlet UILabel *lblBagCount;
    SWRevealViewController *revealController;
    IBOutlet UILabel *lblDownBagCount;
    IBOutlet UILabel *lblWelcomeuser;
    
    SearchView *searchView ;
    BOOL isSearch;

    UITextField *txtSearchProduct;
    
    NSMutableArray *arrContaintList;
    IBOutlet UIButton *btnCurrency;
    
}

@property(nonatomic ,retain)IBOutlet UITableView *tableViewTopContent;
@property (nonatomic,strong) IBOutlet UICollectionView *myCollectionView;
@property (nonatomic,retain) NSArray *dataArray;
@property (nonatomic,retain) IBOutlet UIButton *tooglBackBtn;
@property (nonatomic,retain) IBOutlet UIButton *tooglBackBtnBottom;
@property (nonatomic,retain) MBProgressHUD *progressHud;
//Property Uibutton iVar
@property (nonatomic, retain) IBOutlet UIButton *btnSignOut;
@property (nonatomic, retain) IBOutlet UIButton *btnSignIn, *btnJoin,*btnMyAccount;

- (IBAction) SeacrhButtonPress:(id)sender;
- (IBAction) btnJoinAction:(id)sender;
- (IBAction) btnSignInAction:(id)sender;
- (IBAction) btnSignOutAction:(id)sender;
- (IBAction) btnHelpAction:(id)sender;
- (IBAction) btnReportAction:(id)sender;
- (IBAction)btnChangeCurrency:(id)sender;
- (IBAction)btnChangeSize:(id)sender;
- (IBAction)btnActionMyAccount:(id)sender;

@end
