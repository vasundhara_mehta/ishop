//
//  AppDelegate.m
//  IShop
//
//  Created by Hashim on 4/29/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
#import <GooglePlus/GooglePlus.h>
#import "iShopServices.h"
#import "JSON.h"
#import "Constants.h"


#define kNotificationDeleteProduct @"DeleteProduct"
#define kNotificationAddProduct @"AddProduct"
#define kNotificationEditProduct @"EditProduct"
#define kNotificationAddFavoriteProduct @"AddFavoriteProduct"
#define kNotificationRefreshProductItem @"RefreshProductItem"
#define kNotificationDeleteFavoriteProduct @"DeleteFavoriteProduct"
#define kNotificationEditFavoriteProduct @"EditFavoriteProduct"
#define kNotificationMoveToBagProduct @"MoveToBagProduct"




#define serviceURL @"http://inspirefnb.com/fashionworld/iosapi.php?methodName="
#define BASEURL @"http://inspirefnb.com/"

@interface AppDelegate () <GPPDeepLinkDelegate>

@end
@implementation AppDelegate

@synthesize splash;
@synthesize strPassword, selectedCateId;
@synthesize isUserLogin;
@synthesize arrAddToBag,strCartId,isPaymentComplete,dictUserInfo,arrFavourite,isCheck,isCheckSearchType,strSearchName;
@synthesize isSelecteGrid,selectedCurrentCurrency,selectedCurrentSize;
@synthesize requestFor;


static NSString * const kClientID =@"586890710364-pbld9i8gsg7ma3qu54ahn6jdpfc734rr.apps.googleusercontent.com";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	self.window = window;
    
    isCheckOut = NO;
    isSelecteGrid = 1;
    
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Refined"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Desc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShortDesc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Sku"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MinPrice"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MaxPrice"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    [GPPSignIn sharedInstance].clientID = kClientID;
    
    // Read Google+ deep-link data.
    [GPPDeepLink setDelegate:self];
    [GPPDeepLink readDeepLinkAfterInstall];

    self.currencySymbol=@"$";
    self.currencyValue=@"1";
    
    _ChangeCurrency=NO;
    self.CurrentCurrency=@"USD";
    self.strPassword = @"";
    isUserLogin = NO;
    isPaymentComplete = NO;
    self.strCustomerPassword = @"";
    self.strSearchName = @"";
    isCheck = NO;
    isCheckSearchType = NO;
    self.selectedCurrentCurrency = @"CURRENCY: USD";
    self.selectedCurrentSize = @"SIZES: US";
    
    self.arrAddToBag = [[NSMutableArray alloc] init];
    self.dictUserInfo = [[NSMutableDictionary alloc] init];
    self.arrFavourite = [[NSMutableArray alloc] init];
    self.MoreInfoarray_Recent= [[NSMutableArray alloc]init];
    
    if (IS_IPHONE5)
    {
        self.splash = [[SplashViewController alloc]initWithNibName:@"SplashViewController" bundle:nil];
    }
    else
    {
        self.splash = [[SplashViewController alloc]initWithNibName:@"SplashViewController_iphone3.5" bundle:nil];
    }
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.splash];
    
    navigationController.navigationBarHidden = YES;
	
    self.window.rootViewController = navigationController;
	[self.window makeKeyAndVisible];
    return YES;
}


-(void)getFavoriteHistory
{
    NSString *customer_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"];
    if(customer_id == nil)
    {
        customer_id = @"";
    }
    
    if(![customer_id isEqualToString:@""])
    {
        NSDictionary *parameters = @{@"customer_id": customer_id};
        
        [iShopServices PostMethodWithApiMethod:@"GetUserAllFavoriteHistory" Withparms:parameters WithSuccess:^(id response)
        {
            self.arrFavourite  = [response JSONValue];
            NSLog(@"Response array =%@",self.arrFavourite);
            if(requestFor == ADDFAVORITES)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationAddFavoriteProduct object:nil];
            }
            if(requestFor == DELETEFAVORITEPRODUCT)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationDeleteFavoriteProduct object:nil];
            }
            if(requestFor == EDITFAVORITEPRODUCT)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationEditFavoriteProduct object:nil];
            }
        } failure:^(NSError *error)
         {
             NSLog(@"Error =%@",[error description]);
         }];
    }
}

-(void)getCheckOutHistory
{
    isCheckOut = YES;
    
    NSString *customer_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"];
    if(customer_id == nil)
    {
        customer_id = @"";
    }
    
    if(![customer_id isEqualToString:@""])
    {
        NSDictionary *parameters = @{@"customer_id": customer_id};
        
        [iShopServices PostMethodWithApiMethod:@"GetUserAllCheckoutHistory" Withparms:parameters WithSuccess:^(id response)
        {
            self.arrAddToBag = [response JSONValue];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationRefreshProductItem object:nil];
            if(requestFor == ADDPRODUCT)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationAddProduct object:nil];
            }
            if(requestFor == DELETEPRODUCT)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationDeleteProduct object:nil];
            }
            if(requestFor == EDITPRODUCT)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationEditProduct object:nil];
            }
            if(requestFor == MOVETOBAGPRODUCT)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationMoveToBagProduct object:nil];
            }
        } failure:^(NSError *error)
         {
             NSLog(@"Error =%@",[error description]);
         }];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [GPPURLHandler handleURL:url
                  sourceApplication:sourceApplication
                         annotation:annotation];
}
- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink {
    // An example to handle the deep link data.
    UIAlertView *alert = [[UIAlertView alloc]
                           initWithTitle:@"Deep-link Data"
                           message:[deepLink deepLinkID]
                           delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil];
    [alert show];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [iShopServices GetMethodWithApiMethod:@"GetAllPages" WithSuccess:^(id response)
    {
        NSLog(@"CMS Pages response = %@",[response JSONValue]);
        _mArrayCMSPages = [response JSONValue];
    } failure:^(NSError *error)
    {
        NSLog(@"Error =%@",[error description]);
    }];
    
    //Get Check Out History
    [self getCheckOutHistory];
    
    //Get Favorite History
    [self getFavoriteHistory];
}



- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
