//
//  ViewController.h
//  IShop
//
//  Created by Hashim on 4/29/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>
{
    
    IBOutlet UITableView *listTbl;
    
}
@property(nonatomic,retain)IBOutlet UITableView *listTbl;
@property(nonatomic,retain)NSArray *categoryArray;
@property(nonatomic,retain)MBProgressHUD *progressHud;
@property(nonatomic,retain)UIWindow *window;
@end
