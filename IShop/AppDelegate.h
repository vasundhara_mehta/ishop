//
//  AppDelegate.h
//  IShop
//
//  Created by Hashim on 4/29/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplashViewController.h"

@class GTMOAuth2Authentication;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    BOOL isCheckOut;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) SplashViewController *splash;
@property (nonatomic,retain) NSString *strPassword;
@property (nonatomic, retain) NSMutableArray *arrAddToBag;
@property (nonatomic, retain) NSMutableArray *arrFavourite;
@property BOOL isUserLogin;
@property (nonatomic,retain) NSString *strCartId, *selectedCateId;
@property BOOL isPaymentComplete;
@property (nonatomic,retain) NSMutableDictionary *dictUserInfo;
@property (nonatomic,retain) NSString *strCustomerPassword;
@property (nonatomic,strong) NSMutableArray *MoreInfoarray_Recent;
@property (nonatomic,retain)  NSMutableArray *dataArray;
@property (nonatomic,retain) NSString *CurrentCurrency;
@property (nonatomic,retain) NSString *currencySymbol;
@property (nonatomic,strong) NSString *currencyValue;
@property (nonatomic,strong) NSString *strSearchName;
@property (nonatomic) int isSelecteGrid;
@property BOOL isCheck;
@property BOOL ChangeCurrency;
@property BOOL isCheckSearchType;
@property(nonatomic ,readwrite)int requestFor;
@property(nonatomic ,retain)NSMutableArray *mArrayCMSPages;



@property (nonatomic,retain) NSString *selectedCurrentCurrency;
@property (nonatomic,retain) NSString *selectedCurrentSize;

-(void)getFavoriteHistory;
-(void)getCheckOutHistory;


@end
