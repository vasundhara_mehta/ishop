//
//  iShopServices.h
//  IShop
//
//  Created by Avnish Sharma on 7/1/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface iShopServices : NSObject

+(void)PostMethodWithApiMethod:(NSString *)Strurl Withparms:(NSDictionary *)params WithSuccess:(void(^)(id response))success failure:(void(^)(NSError *error))failure;

+(void)GetMethodWithApiMethod:(NSString *)Strurl WithSuccess:(void(^)(id response))success failure:(void(^)(NSError *error))failure;

@end
