//
//  iShopServices.m
//  IShop
//
//  Created by Avnish Sharma on 7/1/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "iShopServices.h"
#import "JSON.h"

@implementation iShopServices
#define serviceURL @"http://inspirefnb.com/fashionworld/iosapi.php?methodName="
#define BASEURL @"http://inspirefnb.com/fashionworld"


// Class method for call service For (PostType)
+(void)PostMethodWithApiMethod:(NSString *)Strurl Withparms:(NSDictionary *)params WithSuccess:(void(^)(id response))success failure:(void(^)(NSError *error))failure
{
    NSString *path= [@"" stringByAppendingFormat:@"%@%@",serviceURL,Strurl];
    AFHTTPClient *httpclient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BASEURL]];
    [httpclient setParameterEncoding:AFFormURLParameterEncoding];
    NSMutableURLRequest *request = [httpclient requestWithMethod:@"POST" path:path parameters:params];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpclient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *strResponse = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        success (strResponse);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        failure(error);
    }];
    [operation start];
}


// Class method for call service For (GetType)
+(void)GetMethodWithApiMethod:(NSString *)Strurl WithSuccess:(void(^)(id response))success failure:(void(^)(NSError *error))failure
{
    NSString *strPath = [@"" stringByAppendingFormat: @"%@%@",serviceURL ,Strurl];
    AFHTTPClient * httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASEURL]];
    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:strPath parameters:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *strResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        success (strResponse);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    [operation start];
}


@end
