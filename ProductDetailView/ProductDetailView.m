//
//  ProductDetailView.m
//  IShop
//
//  Created by Hashim on 5/3/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "ProductDetailView.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import "ASIFormDataRequest.h"
#import "AddCartViewController.h"
#import <Social/Social.h>
#import "FavouriteViewController.h"
#import "MoreInfoCollectionCell.h"
#import "ViewFullSizeImageVC.h"
#import "SignInViewController.h"
#import "RegistrationViewController.h"
#import "SearchViewController.h"
#import "Currency_Size_VC.h"
#import "UserProfileViewController.h"
#import "BillingAddressViewController.h"
#import "Constants.h"
#import "PaymentDetailViewController.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import "iShopServices.h"
#import "CustomCell.h"
#import "JSON.h"
#import "ListGridViewController.h"
#import "CMSPageViewController.h"
#import "StarRatingView.h"
#import "ReviewAndRatingViewController.h"

#define kNotificationRefreshProductItem @"RefreshProductItem"
#define kNotificationAddProduct @"AddProduct"

#define kLabelAllowance 25.0f
#define kStarViewHeight 13.0f
#define kStarViewWidth 50.0f
#define kLeftPadding 4.0f

@interface ProductDetailView ()<CurrencyDelegate,GPPShareDelegate,GPPSignInDelegate>
{
    BOOL recent_is;
}

@property (weak, nonatomic) IBOutlet UIButton *buttonBuythelook;
@property (weak, nonatomic) IBOutlet UIButton *buttonWeRecom;
@property (weak, nonatomic) IBOutlet UIButton *buttonRecentView;
@property (weak, nonatomic) IBOutlet UIButton *buttonClearAll;

@property (weak, nonatomic) IBOutlet UILabel *lblButTheItems;
@property (weak, nonatomic) IBOutlet UILabel *lblRecentItems;
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionMoreInfo;
@property (weak, nonatomic) IBOutlet UIPageControl *pagecontroller;

- (IBAction) BuyTheLookMethod:(id)sender;
- (IBAction) WeRecommendMethod:(id)sender;
- (IBAction) RecentViewMethod:(id)sender;
- (IBAction) ClearAllMethod:(id)sender;
- (IBAction) change_image_via_page:(id)sender;

@end

@implementation ProductDetailView

@synthesize scrollViewProductDetail,scrolViewPrductImages,mutDictProductDetail,arrProductColorAndSize,arrProductColor,arrProductSize,popupView,popupViewSave;
@synthesize activityIndicator, pickerToolbar1,pickerViewColorandSize;
@synthesize btnPickerCancel, btnPickerDone, btnJoin, btnMyAccount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    totalArr=[[NSMutableArray alloc] init];
    // Do any additional setup after loading the view from its nib.
    isRecomded = NO;
   
    [self setContentView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addProduct:) name:kNotificationAddProduct object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshProductItem:) name:kNotificationRefreshProductItem object:nil];
    
    arrProductColorAndSize = [[NSMutableArray alloc]init];
    //[self getBottomGridData];
    arrContaintList = [[NSMutableArray alloc]init];
    arrContaintList = [[NSUserDefaults standardUserDefaults]objectForKey:@"searchProductsList"];
    isTablcontent = 0;
}

-(void)getBottomGridData
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    NSString *jsonString = [NSString stringWithFormat:@"http://inspirefnb.com/fashionworld/iosapi.php?methodName=getProductsByCatId&cat_id=%@",objAppDelegate.selectedCateId];
    
    NSString* encodedUrl = [jsonString stringByAddingPercentEscapesUsingEncoding:
                            NSASCIIStringEncoding];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:encodedUrl]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *connectionError)
     {
         // handle response
         objAppDelegate.dataArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
         NSLog(@"this is data --- %@",objAppDelegate.dataArray);
         [_CollectionMoreInfo reloadData];
         [hud hide:YES];
         
         [self RefreshCurrencydelegatemethod:objAppDelegate.currencySymbol withvalue:objAppDelegate.currencyValue];
         
         if ([objAppDelegate.dataArray count] != 0)
         {
             // [self.myCollectionView reloadData];
         }
         else
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"No product found for this category!!!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }
     }];
}

-(void)refreshProductItem:(NSNotification *)notification
{
    [self getYourBagItem];
}

-(void)addProduct:(NSNotification *)notification
{
    [self getYourBagItem];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)reportAuthStatus
{
    if ([GPPSignIn sharedInstance].authentication)
    {
       // signInAuthStatus_.text = @"Status: Authenticated";
        [self retrieveUserInfo];
        //[self enableSignInSettings:NO];
    }
    else
    {
        // To authenticate, use Google+ sign-in button.
       // signInAuthStatus_.text = @"Status: Not authenticated";
        //[self enableSignInSettings:YES];
    }
}

- (void)retrieveUserInfo
{
    
}//
-(void)setContentView
{
     NSDictionary *parameters = @{@"product_id":[mutDictProductDetail objectForKey:@"prd_id"]};
    [iShopServices PostMethodWithApiMethod:@"AvrageRating" Withparms:parameters WithSuccess:^(id response)
    {
        NSLog(@"Rating Response =%@",response);
        //Show Rating view
        NSString *starRating = response;
        float rating = [starRating floatValue] ;
        
        StarRatingView* starview = [[StarRatingView alloc]initWithFrame:CGRectMake(15, 420, 103, 18) andRating:rating withLabel:NO animated:YES withSetRating:NO];
        //  starview.delegate = self;
        starview.userInteractionEnabled = NO;
        
        [self.scrollViewProductDetail addSubview:starview];
        
    } failure:^(NSError *error)
    {
        NSLog(@"Error = %@",[error description]);
    }];

    popupView.hidden = YES;
    popupViewSave.hidden = YES;
    viewMoreInfo.hidden = YES;

    btnSizePicker.hidden = YES;
    lblProductSize.hidden = YES;
    btnColorPicker.hidden = YES;
    lblProductColor.hidden = YES;
    
    isWebserviceCount = 1;
    
    popupView.hidden = YES;
    popupViewSave.hidden = YES;
    viewMoreInfo.hidden = YES;
    self.pickerToolbar1.hidden = YES;
    self.pickerViewColorandSize.hidden = YES;
    
    lblProductColor.text = @"Color";
    
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableString *request_gallery = [NSMutableString stringWithFormat:@"%@", [NSString stringWithFormat:product_gallery, [mutDictProductDetail objectForKey:@"prd_id"]]];
    
    NSMutableString *request_options = [NSMutableString stringWithFormat:@"%@", [NSString stringWithFormat:product_option, [mutDictProductDetail objectForKey:@"prd_id"]]];
    
    [self getResponce:request_gallery :1];
    [self getResponce:request_options :2];
    
    NSLog(@"Dict -- %@",mutDictProductDetail);
    
    lblProductName.text = [mutDictProductDetail objectForKey:@"prd_name"];
    lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,[[mutDictProductDetail objectForKey:@"prd_price"] floatValue]];
    strDefaultPrice = [mutDictProductDetail objectForKey:@"prd_price"];
    
    lblProductProductCode.text = [NSString stringWithFormat:@"Product Code %@",[mutDictProductDetail objectForKey:@"prd_sku"]];
    NSString *str=[mutDictProductDetail objectForKey:@"prd_desc"];
    CGSize constraint = CGSizeMake(295 - (10 * 2), 20000.0f);
    CGSize size = [str sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 44.0);
    lblProductProductDiscription.text = [mutDictProductDetail objectForKey:@"prd_desc"];
    lblProductProductDiscription.frame=CGRectMake(12, 225, 295, height+10);
    btnMoreInfo.frame=CGRectMake(5,lblProductProductDiscription.bounds.size.height+225, 310, 30);
    productDetView.frame=CGRectMake(10,lblProductProductDiscription.bounds.size.height+265, 300, 236);
    lblProductProductDiscription.numberOfLines=0;
    imageViewBG.frame=CGRectMake(0, 101, 320, lblProductProductDiscription.bounds.size.height + 408);
    
    if (IS_IPHONE5)
    {
        self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, 3000);
    }
    else
    {
        self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, 1940+height);
    }
    self.scrollViewProductDetail.contentOffset = CGPointMake (self.scrollViewProductDetail.bounds.origin.x, 0);
    stringHt=height;
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideGuestView:)];
    [self.view addGestureRecognizer:gesture];
    
    // 30-05-14 collection registration
    UINib *cellNib = [UINib nibWithNibName:@"MoreInfoCollectionCell" bundle:nil];
    [_CollectionMoreInfo registerNib:cellNib forCellWithReuseIdentifier:@"MoreInfoCollectionCell"];
    
    // hide clear all button default
    _buttonClearAll.hidden=YES;
    _lblRecentItems.hidden=YES;
    
    // check and save Recent visited
    NSString *product_id = [mutDictProductDetail objectForKey:@"prd_id"];
    if (![self matchFoundForEventId:product_id WithArray:objAppDelegate.MoreInfoarray_Recent])
    {
        [objAppDelegate.MoreInfoarray_Recent addObject:mutDictProductDetail];
        [_CollectionMoreInfo reloadData];
    }
    _lblButTheItems.text=[NSString stringWithFormat:@"%d items",objAppDelegate.dataArray.count];
}

- (void)viewWillAppear:(BOOL)animated
{
    isTablcontent = 0;
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self setScrollContent];
    [self getYourBagItem];
}

-(void)setScrollContent
{
    popupView.hidden = YES;
    popupViewSave.hidden = YES;
    
    //topMenuSliderView.hidden = NO;
    CGRect frame = [topMenuSliderView frame];
    frame.origin.y = -323;
    [topMenuSliderView setFrame:frame];
    
    CGRect frameScroll = [self.scrollViewProductDetail frame];
    frameScroll.origin.y = 55;
    [self.scrollViewProductDetail setFrame:frameScroll];
    self.scrollViewProductDetail.userInteractionEnabled = YES;
    self.scrolViewPrductImages.userInteractionEnabled = YES;
    
    isVisibleTopView = NO;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        lblWelcomeuser.text = [NSString stringWithFormat:@"Hi %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"]];
        self.btnSignIn.hidden = YES;
        self.btnSignOut.hidden = NO;
        self.btnJoin.hidden = YES;
        self.btnMyAccount.hidden = NO;
    }
    else
    {
        lblWelcomeuser.text = @"Welcome to iShop";
        self.btnSignIn.hidden = NO;
        self.btnSignOut.hidden = YES;
        self.btnJoin.hidden = NO;
        self.btnMyAccount.hidden = YES;
    }
    
    [self getYourBagItem];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter a search keyword" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    objAppDelegate.isCheckSearchType = YES;
    
    [textField resignFirstResponder];
    
    objAppDelegate.strSearchName = textField.text;
    
    if(arrContaintList == nil)
    {
        arrContaintList = [[NSMutableArray alloc]init];
    }
    
    [arrContaintList addObject:textField.text];
    
    [[NSUserDefaults standardUserDefaults]setObject:arrContaintList forKey:@"searchProductsList"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Refined"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Desc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShortDesc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Sku"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MinPrice"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MaxPrice"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    ListGridViewController *listGridViewController = [[ListGridViewController alloc]initWithNibName:@"ListGridViewController" bundle:nil];
    listGridViewController.doUpdate = YES;
    [self.navigationController pushViewController:listGridViewController animated:YES];
    return YES;
}


#pragma mark --- Table View Delegate and Datasource
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(isTablcontent == 1)
    {
        return 44;
    }
    else
    {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(isTablcontent == 1)
    {
        if([arrContaintList count] > 0)
        {
            UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
            viewHeader.backgroundColor = [UIColor whiteColor];
            
            UILabel *lblHeaderTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 222, 21)];
            lblHeaderTitle.text = @"YOUR RECENT SEARCHES:";
            lblHeaderTitle.font = [UIFont systemFontOfSize:11];
            lblHeaderTitle.textColor = [UIColor blackColor];
            [viewHeader addSubview:lblHeaderTitle];
            
            UIButton *btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
            btnClear.frame = CGRectMake(235, 5, 60, 23);
            [btnClear setImage:[UIImage imageNamed:@"clear_r"] forState:UIControlStateNormal];
            [btnClear addTarget:self action:@selector(actionClearSearchList:) forControlEvents:UIControlEventTouchUpInside];
            [viewHeader addSubview:btnClear];
            return viewHeader;
        }
        return nil;
    }
    else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isTablcontent == 0)
    {
        return [arrProductColorAndSize count];
    }
    else if (isTablcontent == 1)
    {
        return [arrContaintList count];
    }
    else
    {
        return 1 + [objAppDelegate.mArrayCMSPages count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isTablcontent == 0)
    {
        static NSString *cellIdentifier = @"Prd_Attributes";
        CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            NSArray *cellView = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
            cell = (CustomCell *)[cellView objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
        
        if([[[arrProductColorAndSize objectAtIndex:indexPath.row]valueForKey:@"prd_Attribute_Title"] isEqualToString:@""])
        {
            cell.lblPrdAttibuteTitle.text = [[arrProductColorAndSize objectAtIndex:indexPath.row]valueForKey:@"custome_title"];
        }
        else
        {
            cell.lblPrdAttibuteTitle.text = [[arrProductColorAndSize objectAtIndex:indexPath.row]valueForKey:@"prd_Attribute_Title"];
        }
        cell.btnPicker.userInteractionEnabled = YES;
        
        [cell.btnPicker addTarget:self action:@selector(btnColorAndSizeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnPicker.tag = indexPath.row;
        return cell;
    }
    else if(isTablcontent == 1)
    {
        static NSString *CellIdentifier = nil;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
        cell.textLabel.text = [arrContaintList objectAtIndex:indexPath.row] ;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        return cell;
    }
    else
    {
        static NSString *cellIdentifier = @"CMSContent";
        CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
         NSArray *cellView = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
        cell = (CustomCell *)[cellView objectAtIndex:2];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.contentView.backgroundColor = [UIColor clearColor];
//        if (cell == nil)
//        {
//            NSArray *cellView = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
//            cell = (CustomCell *)[cellView objectAtIndex:2];
//            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//            cell.contentView.backgroundColor = [UIColor clearColor];
//        }
       
        if(indexPath.row == 0)
        {
            cell.lblPrdAttibuteTitle.text = [[NSString stringWithFormat:@"CURRENCY: %@",objAppDelegate.CurrentCurrency] uppercaseString];
            [cell.btnCell addTarget:self action:@selector(btnChangeCurrency:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.lblPrdAttibuteTitle.text = [[[objAppDelegate.mArrayCMSPages objectAtIndex:indexPath.row - 1]valueForKey:@"title"]uppercaseString];
            cell.btnCell.tag = indexPath.row - 1;
            [cell.btnCell addTarget:self action:@selector(actionGoToCMSPages:) forControlEvents:UIControlEventTouchUpInside];
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Refined"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Desc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShortDesc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Sku"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MinPrice"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MaxPrice"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if(isTablcontent == 1)
    {
        objAppDelegate.strSearchName = [arrContaintList objectAtIndex:indexPath.row];
        ListGridViewController *listGridViewController = [[ListGridViewController alloc]initWithNibName:@"ListGridViewController" bundle:nil];
        listGridViewController.doUpdate = YES;
        [self.navigationController pushViewController:listGridViewController animated:YES];
    }
    if(isTablcontent == 2)
    {
        if(indexPath.row == 0)
        {
            Currency_Size_VC *Currency_Size_view = [[Currency_Size_VC alloc] initWithNibName:@"Currency_Size_VC" bundle:nil];
            Currency_Size_view.delegateCurrency=self;
            Currency_Size_view.isSelectedValue=1001;
            [self.navigationController pushViewController:Currency_Size_view animated:YES];
        }
        else
        {
            CMSPageViewController *cMSPageViewController = [[CMSPageViewController alloc]initWithNibName:@"CMSPageViewController" bundle:nil];
            
            cMSPageViewController.strTitle = [[[objAppDelegate.mArrayCMSPages objectAtIndex:indexPath.row - 1]valueForKey:@"title"]uppercaseString];
            cMSPageViewController.strDescreption = [[objAppDelegate.mArrayCMSPages objectAtIndex:indexPath.row - 1]valueForKey:@"content"];
            [self.navigationController pushViewController:cMSPageViewController animated:YES];
        }
    }
}
-(IBAction)actionClearSearchList:(id)sender
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"searchProductsList"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    arrContaintList = [[NSMutableArray alloc]init];
    [searchView.tableViewSearchResult reloadData];
}

-(IBAction)actionGoToCMSPages:(id)sender
{
    CMSPageViewController *cMSPageViewController = [[CMSPageViewController alloc]initWithNibName:@"CMSPageViewController" bundle:nil];
    
    cMSPageViewController.strTitle = [[[objAppDelegate.mArrayCMSPages objectAtIndex:[sender tag]]valueForKey:@"title"]uppercaseString];
    cMSPageViewController.strDescreption = [[objAppDelegate.mArrayCMSPages objectAtIndex:[sender tag]]valueForKey:@"content"];
    [self.navigationController pushViewController:cMSPageViewController animated:YES];
}

#pragma mark - Custom Method

-(void) getYourBagItem
{
    int totalItem = 0;
    for(NSMutableDictionary *mDict in objAppDelegate.arrAddToBag)
    {
        totalItem = totalItem + [[ mDict valueForKey:@"prd_qty"]intValue];
    }
    lblDownBagCount.text = [NSString stringWithFormat:@"%d",totalItem];
    lblBagCount.text = [NSString stringWithFormat:@"%d",totalItem];
}

#pragma mark -- REVIEW AND RATING
-(IBAction)actionReviewAndRating:(id)sender
{
    NSString *strXIB = @"ReviewAndRatingViewController";
    if(!IS_IPHONE5)
    {
        strXIB = @"ReviewAndRatingViewController_iPhon3.5";
    }
    ReviewAndRatingViewController *reviewAndRatingViewController = [[ReviewAndRatingViewController alloc]initWithNibName:strXIB bundle:nil];
    reviewAndRatingViewController.strProductID = [mutDictProductDetail objectForKey:@"prd_id"];
    [self.navigationController pushViewController:reviewAndRatingViewController animated:YES];
}

- (void) getResponce:(NSString *)strURL :(int)service_count
{
    NSURL *url = [NSURL URLWithString:strURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60.0];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data,NSError *connectionError){
                               
                               if (service_count == 1)
                               {
                                   for (UIImageView *subImageView in self.scrolViewPrductImages.subviews)
                                   {
                                       [subImageView removeFromSuperview];
                                   }
                                   mutArrayProductImages = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                   NSLog(@"Responce-- %@",mutArrayProductImages);
                                   
                                   self.scrolViewPrductImages.contentSize = CGSizeMake(320 * [mutArrayProductImages count], self.scrolViewPrductImages.frame.size.height);
                                   self.scrolViewPrductImages.contentOffset = CGPointMake (self.scrolViewPrductImages.bounds.origin.x, self.scrollViewProductDetail.bounds.origin.y);
                                   _pagecontroller.numberOfPages = mutArrayProductImages.count;
                                   _pagecontroller.currentPage=0;
                                   
                                   for (int i = 0; i < [mutArrayProductImages count]; i++)
                                   {
                                       imgViewProducts = [[UIImageView alloc] initWithFrame:CGRectMake(i * 320, 0, 320,350)];
                                       imgViewProducts.backgroundColor = [UIColor clearColor];
                                       imgViewProducts.contentMode = UIViewContentModeScaleAspectFit;
                                       
                                       NSString *strURL = [[mutArrayProductImages objectAtIndex:i] objectForKey:@"prd_img"];
                                       
                                       __block UIActivityIndicatorView *activityIndicatorForImage = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                                       activityIndicatorForImage.center = imgViewProducts.center;
                                       activityIndicatorForImage.hidesWhenStopped = YES;
                                       
                                       // Here we use the new provided setImageWithURL: method to load the web image
                                       [imgViewProducts setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType){
                                           if(!error ){
                                               [activityIndicatorForImage removeFromSuperview];
                                           }
                                       }];
                                       [imgViewProducts addSubview:activityIndicatorForImage];
                                       
                                       UITapGestureRecognizer *singleTap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(productGalleryFullView:)];
                                       imgViewProducts.tag=i;
                                       singleTap.view.tag=imgViewProducts.tag;
                                       imgViewProducts.userInteractionEnabled=YES;
                                       [imgViewProducts addGestureRecognizer:singleTap];
                                       
                                       [self.scrolViewPrductImages addSubview:imgViewProducts];
                                   }
                               }
                               else
                               {
                                   NSMutableArray *mArrayResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                   arrProductColorAndSize = [[NSMutableArray alloc]init];
                                   for(NSMutableDictionary *mDict in mArrayResponse)
                                   {
                                       [mDict setValue:@"" forKey:@"prd_Attribute_Title"];
                                       
                                       [mDict setValue:@"" forKey:@"prd_option_id"];
                                       [mDict setValue:@"" forKey:@"prd_option_type_id"];
                                       
                                       [mDict setValue:@"0" forKey:@"prd_New_Price"];
                                       
                                       [arrProductColorAndSize addObject:mDict];
                                   }
                                   
                                   NSLog(@"Responce-- %@",arrProductColorAndSize);
                                   
                                   CGRect newTableFrame = self.tableViewPrdAttributes.frame;
                                   
                                   newTableFrame.size.height = [arrProductColorAndSize count] * 36;
                                   self.tableViewPrdAttributes.frame = newTableFrame;
                                   
                                   CGRect viewProductFrame = self.viewPrdAttributes.frame;
                                   
                                   viewProductFrame.origin.y = self.tableViewPrdAttributes.frame.origin.y + self.tableViewPrdAttributes.frame.size.height - 14 ;
                                   
                                   self.viewPrdAttributes.frame = viewProductFrame;
                                   
                                   float heightScroll = 1940 + ([arrProductColorAndSize count]-1) * 36;
                                   if (IS_IPHONE5)
                                   {
                                       self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, heightScroll+stringHt-275);
                                   }else{
                                        self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, heightScroll+stringHt-185);
                                   }
                                   [self.tableViewPrdAttributes reloadData];
                               }
                           }];
}

#pragma mark - UIButton Method

- (void) signIn
{
    SignInViewController *objSignInViewController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
    [self.navigationController pushViewController:objSignInViewController animated:YES];
}

- (IBAction)topNavigationSliderAction:(id)sender
{
    if (isVisibleTopView)
    {
        isTablcontent = 0;
        [_tableViewPrdAttributes reloadData];
        [UIView beginAnimations:@"animationOff" context:NULL];
        [UIView setAnimationDuration:0.4f];
        //topMenuSliderView.hidden = NO;
        CGRect frame = [topMenuSliderView frame];
        frame.origin.y = -323;
        [topMenuSliderView setFrame:frame];
        CGRect frameScroll = [self.scrollViewProductDetail frame];
        frameScroll.origin.y = 55;
        [self.scrollViewProductDetail setFrame:frameScroll];
        self.scrollViewProductDetail.userInteractionEnabled = YES;
        self.scrolViewPrductImages.userInteractionEnabled = YES;
        isVisibleTopView = NO;
        [UIView commitAnimations];
    }
    else
    {
        isTablcontent = 2;
        topMenuSliderView.userInteractionEnabled = YES;
        menuView.userInteractionEnabled = YES;

        [_tableViewTopContent reloadData];
        [UIView beginAnimations:@"animationOff" context:NULL];
        [UIView setAnimationDuration:0.4f];
        //topMenuSliderView.hidden = NO;
        CGRect frame = [topMenuSliderView frame];
        frame.origin.y = 0;
        frame.size.width = 320;//Some value
        frame.size.height = 378;//some value
        [topMenuSliderView setFrame:frame];
        CGRect frameScroll = [self.scrollViewProductDetail frame];
        frameScroll.origin.y = 378;
        [self.scrollViewProductDetail setFrame:frameScroll];
        self.scrollViewProductDetail.userInteractionEnabled = NO;
        self.scrolViewPrductImages.userInteractionEnabled = NO;
        _tableViewPrdAttributes.userInteractionEnabled =YES;

        isVisibleTopView = YES;
        [self.view  bringSubviewToFront:topMenuSliderView];
        [UIView commitAnimations];
        //menuView.hidden = NO;
    }
}

- (IBAction) btnJoinAction:(id)sender
{
    RegistrationViewController *objRegistrationViewController;
    if (IS_IPHONE5)
    {
        objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil];
    }
    else
    {
        objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController_iphone3.5" bundle:nil];
    }
    
    [self.navigationController pushViewController:objRegistrationViewController animated:YES];
}

- (IBAction) btnSignInAction:(id)sender
{
    [UIView beginAnimations:@"animationOff" context:NULL];
    [UIView setAnimationDuration:0.4f];
    //topMenuSliderView.hidden = NO;
    CGRect frame = [topMenuSliderView frame];
    frame.origin.y = -323;
    [topMenuSliderView setFrame:frame];
    
    CGRect frameScroll = [self.scrollViewProductDetail frame];
    frameScroll.origin.y = 55;
    [self.scrollViewProductDetail setFrame:frameScroll];
    self.scrollViewProductDetail.userInteractionEnabled = YES;
    self.scrolViewPrductImages.userInteractionEnabled = YES;
    isVisibleTopView = NO;
    [UIView commitAnimations];
    
    [self performSelector:@selector(signIn) withObject:nil afterDelay:0.5];
}

- (IBAction)btnSignOutAction:(id)sender
{
    objAppDelegate.isUserLogin = NO;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRemember"];
    
    [objAppDelegate.arrAddToBag removeAllObjects];
    
    [self getYourBagItem];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"customer_id"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"firstname"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"lastname"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"email"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Do you want to Sign Out?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
}

- (IBAction)SeacrhButtonPress:(id)sender
{
    isTablcontent = 1;
    isSearch = YES;
    searchView = (SearchView *)[[[NSBundle mainBundle]loadNibNamed:@"SearchView" owner:self options:nil]objectAtIndex:0 ];
    [searchView.btnHideView addTarget:self action:@selector(hideSearchView:) forControlEvents:UIControlEventTouchUpInside];
    searchView.txtSearch.delegate = self;
    searchView.tableViewSearchResult.delegate = self;
    searchView.tableViewSearchResult.dataSource = self;
    [self.view addSubview:searchView];
    [searchView.tableViewSearchResult reloadData];
}

-(IBAction)hideSearchView:(id)sender
{
    isTablcontent = 0;
    isSearch = NO;
    objAppDelegate.isCheckSearchType = NO;
    [searchView removeFromSuperview];
}

- (IBAction)btnSaveItemAction:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        FavouriteViewController *objFavouriteViewController = [[FavouriteViewController alloc]initWithNibName:@"FavouriteViewController" bundle:nil];
        [self presentViewController:objFavouriteViewController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign in to save" message:@"You'll need to sign in to view or add to your saved items. Don't worry, you only have to do it once." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 102;
        [alert show];
    }
}

- (IBAction) btnHelpAction:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://inspirefnb.com/fashionworld/help"]];
}

- (IBAction) btnReportAction:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://inspirefnb.com/fashionworld/report"]];
}

- (IBAction) btnFavouriteAction:(id)sender
{
    NSString *product_id = [[objAppDelegate.dataArray objectAtIndex:[sender tag]]objectForKey:@"prd_id"];
    
    if (![self matchFoundForEventId:product_id WithArray:objAppDelegate.arrFavourite])
    {
        [objAppDelegate.arrFavourite addObject:[objAppDelegate.dataArray objectAtIndex:[sender tag]]];
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:objAppDelegate.arrFavourite forKey:@"fav_array"];
    NSLog(@"--Bool--%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"fav_array"]);
}

- (IBAction)btnBackAction:(id)sender
{
    if (isVisibleTopView == YES)
    {
        [UIView beginAnimations:@"animationOff" context:NULL];
        [UIView setAnimationDuration:0.4f];
        //topMenuSliderView.hidden = NO;
        CGRect frame = [topMenuSliderView frame];
        frame.origin.y = -323;
        [topMenuSliderView setFrame:frame];
        CGRect frameScroll = [self.scrollViewProductDetail frame];
        frameScroll.origin.y = 55;
        [self.scrollViewProductDetail setFrame:frameScroll];
        self.scrollViewProductDetail.userInteractionEnabled = YES;
        self.scrolViewPrductImages.userInteractionEnabled = YES;


        isVisibleTopView = NO;
        [UIView commitAnimations];
        [self performSelector:@selector(backView) withObject:nil afterDelay:0.5];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) backView
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) btnAddToBagAction:(id)sender
{
//    NSLog(@"Arrrayt  add Bag %@",[[objAppDelegate.arrAddToBag valueForKey:@"option_json_value"] JSONValue]);
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        [objAppDelegate getCheckOutHistory];
        
        popupViewSave.hidden = YES;
        //        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cart_array"];
        popupView.frame = CGRectMake(10, self.viewPrdAttributes.frame.origin.y - 63, 300, 93);
        popupView.hidden = NO;
        [self.scrollViewProductDetail addSubview:popupView];
        [self.view bringSubviewToFront:popupView];
       
        NSString *product_id = [mutDictProductDetail objectForKey:@"prd_id"];
        
       // if (![self matchFoundForEventId:product_id WithArray:objAppDelegate.arrAddToBag])
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Please wait...";
            hud.dimBackground = YES;
            iRequest = 3;
            //[objAppDelegate.arrFavourite addObject:[objAppDelegate.dataArray objectAtIndex:[sender tag]]];
           
            NSMutableArray *jsonArray = [[NSMutableArray alloc]init];
            
            for(NSMutableDictionary *mDict in arrProductColorAndSize)
            {
                NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]init];
                NSLog(@"prd_Attribute_Title =%@",[mDict valueForKey:@"prd_Attribute_Title"]);
                
                [jsonDict setValue:[mDict valueForKey:@"prd_Attribute_Title"] forKey:@"option_value"];
                [jsonDict setValue:[mDict valueForKey:@"custome_title"] forKey:@"option_name"];
                
                [jsonDict setValue:[mDict valueForKey:@"prd_option_id"] forKey:@"option_id"];
                [jsonDict setValue:[mDict valueForKey:@"prd_option_type_id"] forKey:@"option_type_id"];
                if(![[mDict valueForKey:@"prd_Attribute_Title"] isEqualToString:@""])
                {
                   [jsonArray addObject:jsonDict]; 
                }
            }
            
            NSError *writeError = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonArray options:NSJSONWritingPrettyPrinted error:&writeError];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
          //  NSLog(@"JSON Output: %@", jsonString);
       
            int randomNumber = arc4random() % 999999;
            
            NSLog(@"Random No = %d",randomNumber);

            lblProductPrice.text = [lblProductPrice.text substringFromIndex:1];
            
            NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"prd_qty":[mutDictProductDetail objectForKey:@"prd_qty"],@"product_id":product_id,@"option_json_value":jsonString,@"uniq_id":[NSString stringWithFormat:@"%d",randomNumber],@"prd_default_price":[mutDictProductDetail valueForKey:@"prd_price"],@"prd_update_price":lblProductPrice.text,@"get":@"1"};
            
            [iShopServices PostMethodWithApiMethod:@"GetUserCheckoutHistory" Withparms:parameters WithSuccess:^(id response)
            {
               // NSLog(@"Response = %@",[response JSONValue]);
                
                NSLog(@"jsonString = %@",jsonString);
                
                [self addProductForPaymentWithOptions:jsonString];
                objAppDelegate.requestFor = ADDPRODUCT;
                [objAppDelegate getCheckOutHistory];
                //self.arrAddToBag = [response JSONValue];
            } failure:^(NSError *error)
             {
                 NSLog(@"Error =%@",[error description]);
             }];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Please sign in" message:@"To add item to your bag and make purchases, please sign in or join iShop now" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 101;
        [alert show];
    }
    
}

-(void)addProductForPaymentWithOptions:(NSString *)productOptions
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    NSString *strProductOptions = [NSString stringWithFormat:@""];
    NSMutableArray *optionArray = [productOptions JSONValue];
    for(int i = 0; i < [optionArray count]; i++)
    {
        NSMutableDictionary *dict = [optionArray objectAtIndex:i];
        
        if (i == 0)
        {
            strProductOptions = [strProductOptions stringByAppendingString:@"{"];
        }
        
        if(![[dict valueForKey:@"option_id"] isEqualToString:@""])
        {
            strProductOptions = [strProductOptions stringByAppendingFormat:@"\"%@\"",[dict valueForKey:@"option_id"]];
            
            strProductOptions = [strProductOptions stringByAppendingFormat:@":\"%@\"",[dict valueForKey:@"option_type_id"]];
            
            if(i != [optionArray count] - 1)
            {
                strProductOptions = [strProductOptions stringByAppendingString:@","];
            }
        }
        if(i == [optionArray count] - 1)
        {
            strProductOptions = [strProductOptions stringByAppendingString:@"}"];
        }
    }
    
    NSData *data = [strProductOptions dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    NSDictionary *parameters = @{@"prod_id":[mutDictProductDetail valueForKey:@"prd_id"],@"prod_qty":[mutDictProductDetail valueForKey:@"prd_qty"],@"prod_options":jsonString};
    
    [iShopServices PostMethodWithApiMethod:@"ShoppingCartProductsAdd" Withparms:parameters WithSuccess:^(id response)
     {
         NSLog(@"Add Product Response= %@",[response JSONValue]);
         NSMutableDictionary *mDict = [response JSONValue];
         objAppDelegate.strCartId = [mDict valueForKey:@"CartId"];
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     } failure:^(NSError *error)
     {
         NSLog(@"Error = %@",[error description]);
     }];
}

- (IBAction) btnViewBagAction:(id)sender
{
    AddCartViewController *addCart = [[AddCartViewController alloc]initWithNibName:@"AddCartViewController" bundle:nil];
    addCart.requestFor = ADDTOCARTPAYMENT;
    [self.navigationController pushViewController:addCart animated:YES];
}

- (IBAction) btnSaveLaterAction:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        NSString *product_id = [[objAppDelegate.dataArray objectAtIndex:[sender tag]]objectForKey:@"prd_id"];
        
        NSString *add_remove_Favorite = @"0";
        if (![self matchFoundForEventId:product_id WithArray:objAppDelegate.arrFavourite])
        {
            iRequest = 2;
            add_remove_Favorite = @"1";
            NSURL *url = [NSURL URLWithString:get_user_favorite];
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
            request.timeOutSeconds = 120;
            
            [request setPostValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"] forKey:@"customer_id"];
            [request setPostValue:product_id forKey:@"product_id"];
            [request setPostValue:add_remove_Favorite forKey:@"get"];
            [request setDidFinishSelector:@selector(requestFinishedForService:)];
            [request setDidFailSelector:@selector(requestFailed:)];
            [request setDelegate:self];
            [request startAsynchronous];
        }
        else{
            NSLog(@"comes in else SAVE Later %@ %@",mutArrayProductImages,[objAppDelegate.dataArray objectAtIndex:[sender tag]]);
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign in to save" message:@"To save this and other items, please sign in or join iShop now." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign in",@"Join",@"Cancel", nil];
        alert.tag = 102;
        [alert show];
    }
}

- (IBAction) btnViewSavedItemAction:(id)sender
{
    FavouriteViewController *objFavouriteViewController = [[FavouriteViewController alloc]initWithNibName:@"FavouriteViewController" bundle:nil];
    [self presentViewController:objFavouriteViewController animated:YES completion:nil];
}

- (BOOL)matchFoundForEventId:(NSString *)eventId WithArray:(NSMutableArray *)ConditionArray
{
    index = 0;
    for (NSDictionary *dataDict in ConditionArray)
    {
        NSString *createId = [dataDict objectForKey:@"prd_id"];
        if ([eventId isEqualToString:createId])
        {
            return YES;
        }
        index++;
    }
    return NO;
}

-(BOOL)matchFoundForProductId:(NSString *)productId
{
    index1 = 0;
    for (NSDictionary *dataDict in objAppDelegate.arrFavourite)
    {
        NSString *createId = [dataDict objectForKey:@"prd_id"];
        if ([productId isEqualToString:createId])
        {
            return YES;
        }
        index1++;
    }
    return NO;
}

#pragma mark --ALERT DELEGATE
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101 || alertView.tag == 102)
    {
        if (buttonIndex == 0)
        {
            SignInViewController *objSignInViewController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
            [self.navigationController pushViewController:objSignInViewController animated:YES];
        }
        else if(buttonIndex == 1)
        {
            RegistrationViewController *objRegistrationViewController;
            if (IS_IPHONE5)
            {
                objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil];
            }
            else
            {
                objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController_iphone3.5" bundle:nil];
            }
            [self.navigationController pushViewController:objRegistrationViewController animated:YES];
        }
    }
    else if (alertView.tag == 1000)
    {
        if(buttonIndex == 0)
        {
            BillingAddressViewController *objBillingAddressViewController;
            if (IS_IPHONE5)
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController" bundle:nil];
            }
            else
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController1" bundle:nil];
            }
            
            objBillingAddressViewController.requestFor = ADDTOCARTPAYMENT;
            
            objBillingAddressViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objBillingAddressViewController animated:YES];
        }
        if(buttonIndex == 1)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Please wait...";
            hud.dimBackground = YES;
            
             NSDictionary *parameters = @{@"CartId":objAppDelegate.strCartId,@"coupon":txtFieldCoupon.text};
            
            [iShopServices PostMethodWithApiMethod:@"ApplyCouponAdd" Withparms:parameters WithSuccess:^(id response)
            {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Coupon Applied" message:@"You can cancel coupon or continue." delegate:self cancelButtonTitle:@"Cancel Coupon" otherButtonTitles:@"Continue", nil];
                alert.tag = 8888;
                alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                txtFieldCoupon = [alert textFieldAtIndex:0];
                [alert show];
               
            } failure:^(NSError *error)
             {
                NSLog(@"Error = %@",[error description]);
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid coupon code." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 alert.tag = 9999;
                 [alert show];
                 
            }];
        }
    }
    else if (alertView.tag == 9999)
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Discount Codes" message:@"Enter your coupon code if you have one." delegate:self cancelButtonTitle:@"Skip" otherButtonTitles:@"Apply Coupon", nil];
        av.tag = 1000;
        av.alertViewStyle = UIAlertViewStylePlainTextInput;
        txtFieldCoupon = [av textFieldAtIndex:0];
        txtFieldCoupon.delegate = self;
        [av show];
    }
    else if (alertView.tag == 8888)
    {
        if(buttonIndex == 0)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Please wait...";
            hud.dimBackground = YES;
            
            NSDictionary *parameters = @{@"CartId":objAppDelegate.strCartId,@"coupon":txtFieldCoupon.text};

            [iShopServices PostMethodWithApiMethod:@"RemoveCouponAdd" Withparms:parameters WithSuccess:^(id response)
            {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                BillingAddressViewController *objBillingAddressViewController;
                if (IS_IPHONE5)
                {
                    objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController" bundle:nil];
                }
                else
                {
                    objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController1" bundle:nil];
                }
                objBillingAddressViewController.requestFor = ADDTOCARTPAYMENT;
                objBillingAddressViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:objBillingAddressViewController animated:YES];
            } failure:^(NSError *error)
            {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                NSLog(@"Error = %@",[error description]);
            }];
        }
        else if(buttonIndex == 1)
        {
            BillingAddressViewController *objBillingAddressViewController;
            if (IS_IPHONE5)
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController" bundle:nil];
            }
            else
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController1" bundle:nil];
            }
            objBillingAddressViewController.requestFor = ADDTOCARTPAYMENT;
            objBillingAddressViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objBillingAddressViewController animated:YES];
        }
    }
    else
    {
        if(buttonIndex == 1)
        {
            objAppDelegate.isUserLogin = NO;
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRemember"];
            self.btnSignIn.hidden = NO;
            self.btnSignOut.hidden = YES;
            self.btnJoin.hidden = NO;
            self.btnMyAccount.hidden = YES;
            lblWelcomeuser.text = @"Welcome to iShop";
        }
    }
}

- (IBAction) btnCheckOutAction:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        isCheckOut = YES;
        [self setCustomerToCart];
    }
    else
    {
        SignInViewController *objSignIn;
        objSignIn = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
        [self.navigationController pushViewController:objSignIn animated:YES];
    }
}

//NEW TASK START

- (void) setCustomerToCart
{
    //NSLog(@"Arrayyy -- %@",objAppDelegate.arrAddToBag);
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    objAppDelegate.strCustomerPassword = [[NSUserDefaults standardUserDefaults] objectForKey:@"passwordSave"];
    
    NSLog(@"strCustomerPassword -- %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"passwordSave"]);
    
    NSDictionary *parameters = @{@"customer_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"customer_id"],@"email":[[NSUserDefaults standardUserDefaults] valueForKey:@"email"],@"firstname":[[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"],@"lastname":[[NSUserDefaults standardUserDefaults] valueForKey:@"lastname"],@"password":objAppDelegate.strCustomerPassword,@"CartId":objAppDelegate.strCartId};
    
    [iShopServices PostMethodWithApiMethod:@"SetCustomerIntoCart" Withparms:parameters WithSuccess:^(id response)
     {
         NSLog(@"Add Product Response= %@",[response JSONValue]);
         NSMutableDictionary *mDict = [response JSONValue];
         // strCartId = [mDict valueForKey:@"CartId"];
         // [self setCustomerToCart];
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         objAppDelegate.strCartId = [mDict objectForKey:@"value"] ;
         
         UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Discount Codes" message:@"Enter your coupon code if you have one." delegate:self cancelButtonTitle:@"Skip" otherButtonTitles:@"Apply Coupon", nil];
         av.tag = 1000;
         av.alertViewStyle = UIAlertViewStylePlainTextInput;
         txtFieldCoupon = [av textFieldAtIndex:0];
         txtFieldCoupon.delegate = self;
         [av show];
         
     } failure:^(NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];

         NSLog(@"Error = %@",[error description]);
     }];
}


- (void) setProductToCart
{
    NSMutableArray *arrayProducts = [[NSMutableArray alloc] init];
    
    //for (NSDictionary *dictProduct in objAppDelegate.arrAddToBag)
    {
        NSString *strProducts = [[mutDictProductDetail objectForKey:@"prd_id"]  stringByAppendingFormat:@"~%@",[[mutDictProductDetail objectForKey:@"prd_qty"] stringByAppendingFormat:@"~%@", [mutDictProductDetail objectForKey:@"prd_sku"]]];
        [arrayProducts addObject:strProducts];
    }
    
    NSString *productOnCart  = [arrayProducts componentsJoinedByString:@"|"];
    
    objAppDelegate.strCartId = [dictCartToCustomerResponse objectForKey:@"value"];
    
    iRequest = 1;
    NSURL *url = [NSURL URLWithString:set_cart_product];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setPostValue:productOnCart forKey:@"products"];
    [request setPostValue:[dictCartToCustomerResponse objectForKey:@"value"] forKey:@"CartId"];
    [request setDidFinishSelector:@selector(requestFinishedForService:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDelegate:self];
    [request startAsynchronous];
}

#pragma mark - ASIHTTP Response

-(void)requestFinishedForService:(ASIHTTPRequest *)request
{
    if (iRequest == 0)
    {
        NSData *data = [request responseData];
        dictCartToCustomerResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        [self setProductToCart];
    }
    else if (iRequest == 1)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSData *data = [request responseData];
        
        NSDictionary *dictResponce = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        BOOL checkResponse = [[dictResponce objectForKey:@"value"] boolValue];
        
        if (checkResponse)
        {
            BillingAddressViewController *objBillingAddressViewController;
            if (IS_IPHONE5)
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController" bundle:nil];
            }
            else
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController1" bundle:nil];
            }
            
            objBillingAddressViewController.requestFor = FINALPAYMENT;
            objBillingAddressViewController.mDictProductDetail = mutDictProductDetail;
            objBillingAddressViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objBillingAddressViewController animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if (iRequest == 2)
    {
        //SAVE FOR LATER
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSData *data = [request responseData];
        NSLog(@"data -- %@",data);
        [objAppDelegate getFavoriteHistory];
    }
    else if (iRequest == 3)
    {
        objAppDelegate.requestFor = ADDPRODUCT;
        [objAppDelegate getCheckOutHistory];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Check your network connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

-(IBAction)btnActionMyAccount:(id)sender
{
    UserProfileViewController *objUserProfileViewController;
    if (IS_IPHONE5)
    {
        objUserProfileViewController = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
    }
    else
    {
        objUserProfileViewController = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController_iphone3.5" bundle:nil];
    }
    [self.navigationController pushViewController:objUserProfileViewController animated:YES];
}

- (IBAction)btnColorAndSizeAction:(id)sender
{
    isSelectedPicker  = [sender tag];
    
    self.tableViewPrdAttributes.userInteractionEnabled = NO;
    
    _mArrayProductAttributes = [[arrProductColorAndSize objectAtIndex:isSelectedPicker] objectForKey:@"custome_values"];
    
    // lblProductPrice.text = [NSString stringWithFormat:@"$ %@",[mutDictProductDetail objectForKey:@"prd_price"]];
  
    //lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,[[mutDictProductDetail objectForKey:@"prd_price"] floatValue]];
    [self.pickerViewColorandSize reloadAllComponents];
    
    self.pickerViewColorandSize.hidden = NO;
    self.pickerToolbar1.hidden = NO;
    
    self.btnPickerDone.hidden = NO;
    self.btnPickerCancel.hidden = NO;
}

- (IBAction) btnMoreInfoAction:(id)sender
{
    viewMoreInfo.hidden = NO;
    txtViewMoreInfo.text = [mutDictProductDetail objectForKey:@"prd_longdesc"];
    [self.view bringSubviewToFront:viewMoreInfo];
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = .50f;
    transition.type =  @"moveIn";
    transition.subtype = @"fromTop";
    [viewMoreInfo.layer removeAllAnimations];
    [self.view addSubview:viewMoreInfo];
    [viewMoreInfo.layer addAnimation:transition forKey:kCATransition];
}

- (IBAction) btnCloseMoreInfoAction:(id)sender
{
    [UIView beginAnimations:@"animationOff" context:NULL];
    [UIView setAnimationDuration:0.5f];
    viewMoreInfo.hidden = YES;
    [UIView commitAnimations];
}

#pragma mark - Image Sharing From Face Book

-(IBAction)btnActionFaceBookSharing:(id)sender
{
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [controller setInitialText:@"What do you think? Should i buy this?"];
    [controller addURL:[NSURL URLWithString:[mutDictProductDetail valueForKey:@"product_url"]]];
    
    [controller addImage:imgViewProducts.image];
    
    [self presentViewController:controller animated:YES completion:Nil];
}

-(IBAction)btnActionTwitterSharing:(id)sender
{
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    [controller setInitialText:@"What do you think? Should I buy this?"];
     [controller addURL:[NSURL URLWithString:[mutDictProductDetail valueForKey:@"product_url"]]];
    [controller addImage:imgViewProducts.image];
    
    [self presentViewController:controller animated:YES completion:Nil];
}

-(IBAction)btnActionMailSharing:(id)sender
{
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init]  ;
	mailComposer.mailComposeDelegate = self;
	
	if ([MFMailComposeViewController canSendMail])
    {
		[mailComposer setToRecipients:nil];
		[mailComposer setSubject:@"What do you think? Should I buy this?"];
        NSString *message = [@"Check it out at:\n\n\n" stringByAppendingString:[mutDictProductDetail valueForKey:@"product_url"]];
        
		[mailComposer setMessageBody:message isHTML:YES];
        
        NSData *imageData = UIImagePNGRepresentation(imgViewProducts.image);
        
		[mailComposer addAttachmentData:imageData mimeType:@"image/jpeg" fileName:@""];
		
		
        [self presentViewController:mailComposer animated:YES completion:nil];
	}//if
}

-(IBAction)btnActionGooglePlus:(id)sender
{
    static NSString * const kClientID =@"586890710364-pbld9i8gsg7ma3qu54ahn6jdpfc734rr.apps.googleusercontent.com";

    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.clientID = kClientID;
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    
    [GPPSignIn sharedInstance].actions = [NSArray arrayWithObjects:
                                          @"http://schemas.google.com/AddActivity",
                                          @"http://schemas.google.com/BuyActivity",
                                          @"http://schemas.google.com/CheckInActivity",
                                          @"http://schemas.google.com/CommentActivity",
                                          @"http://schemas.google.com/CreateActivity",
                                          @"http://schemas.google.com/ListenActivity",
                                          @"http://schemas.google.com/ReserveActivity",
                                          @"http://schemas.google.com/ReviewActivity",
                                          nil];
    signIn.delegate = self;
    [signIn authenticate];
    
    id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
    
    // Set any prefilled text that you might want to suggest
    
    [shareBuilder setURLToShare:[NSURL URLWithString:[mutDictProductDetail valueForKey:@"product_url"]]];
    [shareBuilder setPrefillText:@"What do you think? Should I buy this?"];
    
    //[shareBuilder attachImage:imgViewProducts.image];
     [shareBuilder open];
}

#pragma mark - GPPSignInDelegate

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error
{
    if (error)
    {
        return;
    }
    [self reportAuthStatus];
}

#pragma mark - GPPShareDelegate

- (void)finishedSharing:(BOOL)shared {
    NSString *text = shared ? @"Success" : @"Canceled";
    //shareStatus_.text = [NSString stringWithFormat:@"Status: %@", text];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:text delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - UIWebViewDelegate

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"DONE");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [activityIndicator stopAnimating];
    NSLog(@"error %@",error);
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	if (result == MFMailComposeResultSent)
    {
		NSLog(@"Mail Send");
	}// if
	[self dismissViewControllerAnimated:YES completion:nil];
}// mailComposeController

#pragma mark - UIPickerView Delegate methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_mArrayProductAttributes count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *strValue;
    strValue = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];
    return strValue;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    float Price = 0.0;

    strAttributeTitile = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];
    strPrdOptionId = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"option_id"];
    strPrdOptionTypeId = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"option_type_id"];
    
    lblProductColor.text = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];
    
   // Price = [strDefaultPrice floatValue] + [[[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"price"] floatValue];
    
    NSLog(@"Product Price= %@",lblProductPrice.text);
    
     Price = [strDefaultPrice floatValue] + [[[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"price"] floatValue];
    
   // lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,Price];
    
    strProductPrice = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"price"];
    
    //pickerViewColorandSize.hidden = YES;
}

-(NSString *)setProductPrice
{
    float newPrice = [strDefaultPrice floatValue];
    
    
    for (NSMutableDictionary *mdict in arrProductColorAndSize)
    {
        newPrice = [[mdict valueForKey:@"prd_New_Price"]floatValue] + newPrice;
    }
    
    return [NSString stringWithFormat:@"%.2f",newPrice];
}

- (IBAction)btnCancelToolbarAction:(id)sender
{
    self.pickerViewColorandSize.hidden = YES;
    self.pickerToolbar1.hidden = YES;
    
    self.btnPickerDone.hidden = YES;
    self.btnPickerCancel.hidden = YES;
    self.tableViewPrdAttributes.userInteractionEnabled = YES;
    
    NSMutableDictionary *mDict = [arrProductColorAndSize objectAtIndex:isSelectedPicker];
    
    if([[mDict valueForKey:@"prd_New_Price"] floatValue ] > 0)
    {
        [mDict setValue:[mDict valueForKey:@"prd_New_Price"] forKey:@"prd_New_Price"];
    }
    else
    {
        [mDict setValue:@"0" forKey:@"prd_New_Price"];
    }
    
    lblProductPrice.text = [NSString stringWithFormat:@"%@%@",objAppDelegate.currencySymbol,[self setProductPrice]];
    
    [self.tableViewPrdAttributes reloadData];
}

- (IBAction)btnDoneToolbarAction:(id)sender
{
    self.pickerViewColorandSize.hidden = YES;
    self.pickerToolbar1.hidden = YES;
    
    self.btnPickerDone.hidden = YES;
    self.btnPickerCancel.hidden = YES;
    
    self.tableViewPrdAttributes.userInteractionEnabled = YES;
    
    NSMutableDictionary *mDict = [arrProductColorAndSize objectAtIndex:isSelectedPicker];
    
    [mDict setValue:strProductPrice forKey:@"prd_New_Price"];
    
    lblProductPrice.text = [NSString stringWithFormat:@"%@%@",objAppDelegate.currencySymbol,[self setProductPrice]];
    
    strProductPrice = @"0";

   // NSString *strPrice = [lblProductPrice.text stringByReplacingOccurrencesOfString:objAppDelegate.currencySymbol withString:@""];
    //[mutDictProductDetail setValue:strPrice forKey:@"prd_price"];
    
    [[arrProductColorAndSize objectAtIndex:isSelectedPicker]setValue:strAttributeTitile forKey:@"prd_Attribute_Title"];
    
    [[arrProductColorAndSize objectAtIndex:isSelectedPicker]setValue:strPrdOptionId forKey:@"prd_option_id"];
    
    [[arrProductColorAndSize objectAtIndex:isSelectedPicker]setValue:strPrdOptionTypeId forKey:@"prd_option_type_id"];
    
   
    [self.tableViewPrdAttributes reloadData];
    
    NSLog(@"After Chnage dict -- %@",mutDictProductDetail);
}

#pragma mark - Response

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [webData setLength: 0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [webData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConenction");
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (isWebserviceCount == 1)
    {
        [mutArrayProductImages removeAllObjects];
        [imgViewProducts removeFromSuperview];
        mutArrayProductImages = [NSJSONSerialization JSONObjectWithData:webData options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"Responce-- %@",mutArrayProductImages);
        
        self.scrolViewPrductImages.contentSize = CGSizeMake(320 * [mutArrayProductImages count], self.scrolViewPrductImages.frame.size.height);
        self.scrolViewPrductImages.contentOffset = CGPointMake (self.scrolViewPrductImages.bounds.origin.x, self.scrollViewProductDetail.bounds.origin.y);
        _pagecontroller.numberOfPages=mutArrayProductImages.count;
        _pagecontroller.currentPage=0;
        
        for (int i = 0; i < [mutArrayProductImages count]; i++)
        {
            imgViewProducts = [[UIImageView alloc] initWithFrame:CGRectMake(i * 320, 0, 320,350)];
            imgViewProducts.backgroundColor = [UIColor clearColor];
            imgViewProducts.contentMode = UIViewContentModeScaleAspectFit;
            
            NSString *strURL = [[mutArrayProductImages objectAtIndex:i] objectForKey:@"prd_img"];
            // Here we use the new provided setImageWithURL: method to load the web image
            [imgViewProducts setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"]];
            
            UITapGestureRecognizer *singleTap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(productGalleryFullView:)];
            imgViewProducts.tag=i;
            singleTap.view.tag=imgViewProducts.tag;
            imgViewProducts.userInteractionEnabled=YES;
            [imgViewProducts addGestureRecognizer:singleTap];
            
            [self.scrolViewPrductImages addSubview:imgViewProducts];
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // Create Session Configuration
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        // Configure Session Configuration
        [sessionConfiguration setAllowsCellularAccess:YES];
        [sessionConfiguration setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json" }];
        
        // Create Session
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        // Send Request
        NSURL *url = [NSURL URLWithString:@""];
        [[session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
          {
              NSLog(@"%@", [NSJSONSerialization JSONObjectWithData:data options:0 error:nil]);
              arrProductColorAndSize = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
              NSLog(@"Responce-- %@",arrProductColorAndSize);
              
              [self.tableViewPrdAttributes reloadData];
              if ([arrProductColorAndSize count] == 1)
              {
                  if ([[[arrProductColorAndSize objectAtIndex:0] objectForKey:@"custome_title"] isEqualToString:@"Color"])
                  {
                      self.arrProductColor = [[arrProductColorAndSize objectAtIndex:0] objectForKey:@"custome_values"];
                      lblProductColor.text = [[arrProductColorAndSize objectAtIndex:0] objectForKey:@"custome_title"];
                  }
                  else
                  {
                      self.arrProductSize = [[arrProductColorAndSize objectAtIndex:0] objectForKey:@"custome_values"];
                      lblProductSize.text = [[arrProductColorAndSize objectAtIndex:1] objectForKey:@"custome_title"];
                  }
              }
              [self.pickerViewColorandSize reloadAllComponents];
          }] resume];
    }
    else
    {
        arrProductColorAndSize = [NSJSONSerialization JSONObjectWithData:webData options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"Responce-- %@",arrProductColorAndSize);
        
        [self.tableViewPrdAttributes reloadData];
        
        if ([arrProductColorAndSize count] == 1)
        {
            if ([[[arrProductColorAndSize objectAtIndex:0] objectForKey:@"custome_title"] isEqualToString:@"Color"])
            {
                self.arrProductColor = [[arrProductColorAndSize objectAtIndex:0] objectForKey:@"custome_values"];
                lblProductColor.text = [[arrProductColorAndSize objectAtIndex:0] objectForKey:@"custome_title"];
            }
            else
            {
                self.arrProductSize = [[arrProductColorAndSize objectAtIndex:0] objectForKey:@"custome_values"];
                lblProductSize.text = [[arrProductColorAndSize objectAtIndex:1] objectForKey:@"custome_title"];
            }
        }
        
        NSLog(@"self.arrProductColor-- %@",self.arrProductColor);
        NSLog(@" self.arrProductSize-- %@", self.arrProductSize);
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.pickerViewColorandSize reloadAllComponents];
    }
}

-(void)hideGuestView:(UITapGestureRecognizer *)gesture
{
    popupView.hidden = YES;
    popupViewSave.hidden = YES;
}

#pragma mark - ScrollView  Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrolViewPrductImages.frame.size.width;
    int pageNo = floor((self.scrolViewPrductImages.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pagecontroller.currentPage=pageNo;
    
}// scrollViewDidScroll

#pragma mark - Collection And methods

- (IBAction)BuyTheLookMethod:(id)sender
{
    _lblButTheItems.text=[NSString stringWithFormat:@"%d items",objAppDelegate.dataArray.count];
    recent_is = NO;
    isRecomded = NO;

    [_CollectionMoreInfo reloadData];
    [_buttonBuythelook setImage:[UIImage imageNamed:@"buythelook_w.png"] forState:UIControlStateNormal];
    [_buttonWeRecom setImage:[UIImage imageNamed:@"werecommend_g.png"] forState:UIControlStateNormal];
    [_buttonRecentView setImage:[UIImage imageNamed:@"recentlyviewed_g.png"] forState:UIControlStateNormal];
    
    _buttonClearAll.hidden=YES;
    _lblRecentItems.hidden=YES;
}

- (IBAction)WeRecommendMethod:(id)sender
{
    //_lblButTheItems.text =[NSString stringWithFormat:@"%d items",objAppDelegate.dataArray.count];
    recent_is=NO;
    isRecomded = YES;

    [_CollectionMoreInfo reloadData];
    
    [_buttonBuythelook setImage:[UIImage imageNamed:@"buythelook_g.png"] forState:UIControlStateNormal];
    [_buttonWeRecom setImage:[UIImage imageNamed:@"werecommend_w.png"] forState:UIControlStateNormal];
    [_buttonRecentView setImage:[UIImage imageNamed:@"recentlyviewed_g.png"] forState:UIControlStateNormal];
    
    _buttonClearAll.hidden=YES;
    _lblRecentItems.hidden=YES;
}

- (IBAction)RecentViewMethod:(id)sender
{
    _lblButTheItems.text=[NSString stringWithFormat:@"%d items",objAppDelegate.MoreInfoarray_Recent.count];
    recent_is=YES;
    [_CollectionMoreInfo reloadData];
    [_buttonBuythelook setImage:[UIImage imageNamed:@"buythelook_g.png"] forState:UIControlStateNormal];
    [_buttonWeRecom setImage:[UIImage imageNamed:@"werecommend_g.png"] forState:UIControlStateNormal];
    [_buttonRecentView setImage:[UIImage imageNamed:@"recentlyviewed_w.png"] forState:UIControlStateNormal];
    
    _buttonClearAll.hidden=NO;
    _lblRecentItems.hidden=NO;
}

- (IBAction)ClearAllMethod:(id)sender
{
    if (objAppDelegate.MoreInfoarray_Recent.count>0)
    {
        recent_is=YES;
        [_CollectionMoreInfo reloadData];
        [objAppDelegate.MoreInfoarray_Recent removeAllObjects];
        _lblButTheItems.text=[NSString stringWithFormat:@"%d items",objAppDelegate.MoreInfoarray_Recent.count];
    }
}

- (IBAction)change_image_via_page:(id)sender
{
    CGFloat x = _pagecontroller.currentPage * self.scrolViewPrductImages.frame.size.width;
    [self.scrolViewPrductImages setContentOffset:CGPointMake(x, 0) animated:YES];
}
//========================================== Photo collection  method==================================//

#pragma mark - UICollectionViewDataSource
#pragma mark Collection view Data source and delegate method

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    if (recent_is==NO)
    {
        int count = 0;
        if(isRecomded)
        {
            for( NSMutableDictionary *mDict in objAppDelegate.dataArray)
            {
                if([[mDict valueForKeyPath:@"product_recommended"] isEqualToString:@"1"])
                {
                    count = count +1;
                }
            }
            _lblButTheItems.text = [NSString stringWithFormat:@"%d items",count];
            return count;
        }
        else
        {
            return  objAppDelegate.dataArray.count;
        }
    }
    else
    {
        return objAppDelegate.MoreInfoarray_Recent.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MoreInfoCollectionCell *cell = (MoreInfoCollectionCell*)[cv dequeueReusableCellWithReuseIdentifier:@"MoreInfoCollectionCell" forIndexPath:indexPath];
    
    if (recent_is==NO)
    {
        cell.lblName.text=[[objAppDelegate.dataArray objectAtIndex:indexPath.row] objectForKey:@"prd_name"];
        cell.lblPrice.text=[NSString stringWithFormat:@"$ %@",[[objAppDelegate.dataArray objectAtIndex:indexPath.row] objectForKey:@"prd_price"]];
        
        [cell.imageThumb setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[objAppDelegate.dataArray objectAtIndex:indexPath.row] objectForKey:@"prd_thumb"]]] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"]];
        cell.imageThumb.contentMode = UIViewContentModeScaleAspectFill;
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
        singleTap.numberOfTapsRequired = 1;
        cell.imageThumb.tag = indexPath.row;
        [cell.imageThumb addGestureRecognizer:singleTap];
    }
    else
    {
        cell.lblName.text=[[objAppDelegate.MoreInfoarray_Recent objectAtIndex:indexPath.row] objectForKey:@"prd_name"];
        cell.lblPrice.text=[NSString stringWithFormat:@"$ %@",[[objAppDelegate.MoreInfoarray_Recent objectAtIndex:indexPath.row] objectForKey:@"prd_price"]];
        
        [cell.imageThumb setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[objAppDelegate.MoreInfoarray_Recent objectAtIndex:indexPath.row] objectForKey:@"prd_thumb"]]] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"]];
        cell.imageThumb.contentMode = UIViewContentModeScaleAspectFill;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
        singleTap.numberOfTapsRequired = 1;
        cell.imageThumb.tag = indexPath.row;
        [cell.imageThumb addGestureRecognizer:singleTap];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Adjust cell size for orientation
    return CGSizeMake(90.0f, 177.0f);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2; // This is the minimum inter item spacing, can be more
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"objAppDelegate.dataArray -- %@",objAppDelegate.dataArray);
}

- (void) oneTap:(UIGestureRecognizer *)gesture
{
    UIImageView *selectedImageView = (UIImageView*)[gesture view];
    self.mutDictProductDetail = [objAppDelegate.dataArray objectAtIndex:selectedImageView.tag];
    [mutArrayProductImages removeAllObjects];
    [self setContentView];
    [self setScrollContent];
}

-(void)productGalleryFullView:(UITapGestureRecognizer *)gesture
{
    //gesture.view.tag
    NSString *strXIB = @"ViewFullSizeImageVC_iPhone3.5";
    if(IS_IPHONE5)
    {
        strXIB = @"ViewFullSizeImageVC";
    }
    
    ViewFullSizeImageVC *viewfullsizeimageview=[[ViewFullSizeImageVC alloc]initWithNibName:strXIB bundle:nil];
    viewfullsizeimageview.image_tag = gesture.view.tag;
    viewfullsizeimageview.imageArray = mutArrayProductImages;
    [self presentViewController:viewfullsizeimageview animated:YES completion:nil];
}

- (IBAction)btnChangeCurrency:(id)sender
{
    Currency_Size_VC *Currency_Size_view = [[Currency_Size_VC alloc] initWithNibName:@"Currency_Size_VC" bundle:nil];
    Currency_Size_view.delegateCurrency=self;
    Currency_Size_view.isSelectedValue = 1001;
    [self.navigationController pushViewController:Currency_Size_view animated:YES];
}

- (IBAction)btnChangeSize:(id)sender
{
    Currency_Size_VC *Currency_Size_view = [[Currency_Size_VC alloc] initWithNibName:@"Currency_Size_VC" bundle:nil];
    Currency_Size_view.delegateCurrency=self;
    Currency_Size_view.isSelectedValue = [sender tag];
    [self.navigationController pushViewController:Currency_Size_view animated:YES];
}

-(void)RefreshCurrencydelegatemethod:(NSString *)symbol withvalue:(NSString *)value
{
    NSString *prod_price=[NSString stringWithFormat:@"%@",[mutDictProductDetail objectForKey:@"prd_price"]];
    
    CGFloat final_price = [prod_price floatValue] * [value floatValue];
    
    lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,final_price];
    
    //[mutDictProductDetail setValue:[NSNumber numberWithFloat:final_price] forKey:@"prd_price"];
}


@end
