//
//  ViewFullSizeImageVC.h
//  IShop
//
//  Created by Vivek  on 30/05/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewFullSizeImageVC : UIViewController

@property (nonatomic,retain) NSMutableArray *imageArray;
@property NSInteger image_tag;
@end
