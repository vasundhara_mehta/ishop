//
//  ProductDetailView.h
//  IShop
//
//  Created by Hashim on 5/3/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import "SearchView.h"


@interface ProductDetailView : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate,UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIWebViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    
    UIScrollView *scrollViewProductDetail;
    UIScrollView *scrolViewPrductImages;
    
    IBOutlet UIImageView *imgViewProducts;
    
    //Mutable Data iVar
    NSMutableData *webData;
    NSMutableArray *mutArrayProductImages;
    
    IBOutlet UILabel *lblProductName;
    IBOutlet UILabel *lblProductPrice;
    IBOutlet UILabel *lblProductProductCode;
    IBOutlet UILabel *lblProductProductDiscription;
    IBOutlet UILabel *lblProductSize;
    IBOutlet UILabel *lblProductColor;
    IBOutlet UILabel *lblBagCount;
    IBOutlet UILabel *lblDownBagCount;
    
    BOOL isRecomded;
    IBOutlet UIButton *btnDone;
    
    IBOutlet UIView *viewMoreInfo;
    IBOutlet UITextView *txtViewMoreInfo;
    IBOutlet UIButton *btnCloseMorInfo;
    
    IBOutlet UIButton *btnColorPicker;
    IBOutlet UIButton *btnSizePicker;
    
    int isWebserviceCount;
    int isGetButton;
    
    AppDelegate *objAppDelegate;
    int index;
    int index1;
    
    int isSelectedPicker;
    
    IBOutlet UIView *topMenuSliderView;
    IBOutlet UIView *menuView;
    BOOL isVisibleTopView;
    NSString *strDefaultPrice;
    IBOutlet UILabel *lblWelcomeuser;
    
    IBOutlet UIButton *btnCurrency;
    IBOutlet UIButton *btnSize;
    
    int iRequest;
    
    NSMutableDictionary *dictCartToCustomerResponse;
    
    
    NSString *strAttributeTitile;
    NSString *strPrdOptionId;
    NSString *strPrdOptionTypeId;
    
    NSString *strProductPrice;
    
    BOOL isCheckOut;
    IBOutlet UIButton *btnMoreInfo;
    IBOutlet UIView *productDetView;
    IBOutlet UIImageView *imageViewBG;
    CGFloat stringHt;
    NSMutableArray *totalArr;
    UITextField *txtFieldCoupon;
    BOOL isSearch;
    SearchView *searchView;
    NSMutableArray *arrContaintList;

    int isTablcontent;
}
@property(nonatomic ,retain)IBOutlet UITableView *tableViewTopContent;

@property(nonatomic ,retain)IBOutlet UIButton *btnAddToBag;
@property(nonatomic ,retain)IBOutlet UITableView *tableViewPrdAttributes;
@property(nonatomic ,retain)IBOutlet UIView *viewPrdAttributes;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollViewProductDetail;
@property (nonatomic, retain) IBOutlet UIScrollView *scrolViewPrductImages;
@property (nonatomic, retain) NSMutableDictionary *mutDictProductDetail;

//Property Mutable Array iVar
@property (nonatomic, retain) NSMutableArray *arrProductColor,*mArrayProductAttributes;
@property (nonatomic, retain) NSMutableArray *arrProductSize;
@property (nonatomic, retain) NSMutableArray *arrProductColorAndSize;

@property (retain, nonatomic) IBOutlet UIView *popupView;
@property (retain, nonatomic) IBOutlet UIView *popupViewSave;

@property (nonatomic, retain) IBOutlet UIToolbar *pickerToolbar1;
@property (nonatomic,retain) IBOutlet UIPickerView *pickerViewColorandSize;
//Property Uibutton iVar
@property (nonatomic, retain) IBOutlet UIButton *btnSignOut;
@property (nonatomic, retain) IBOutlet UIButton *btnSignIn, *btnPickerCancel, *btnPickerDone, *btnJoin,*btnMyAccount;

#pragma mark - UIButton Method

- (IBAction) btnBackAction:(id)sender;
- (IBAction) btnColorAndSizeAction:(id)sender;
- (IBAction) btnAddToBagAction:(id)sender;

- (IBAction) btnViewBagAction:(id)sender;
- (IBAction) btnCheckOutAction:(id)sender;
- (IBAction) btnMoreInfoAction:(id)sender;
- (IBAction) btnCloseMoreInfoAction:(id)sender;

- (IBAction) btnSaveLaterAction:(id)sender;
- (IBAction) btnViewSavedItemAction:(id)sender;

- (IBAction)btnCancelToolbarAction:(id)sender;
- (IBAction)btnDoneToolbarAction:(id)sender;

- (IBAction) topNavigationSliderAction:(id)sender;

- (IBAction) btnJoinAction:(id)sender;
- (IBAction) btnSignInAction:(id)sender;
- (IBAction) btnSignOutAction:(id)sender;
- (IBAction) btnHelpAction:(id)sender;
- (IBAction) btnReportAction:(id)sender;
- (IBAction) btnFavouriteAction:(id)sender;
- (IBAction)btnActionMyAccount:(id)sender;


@end
