//
//  SplashViewController.h
//  IShop
//
//  Created by Hashim on 4/30/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "SWRevealViewController.h"

@interface SplashViewController : UIViewController<MHTabBarControllerDelegate,SWRevealViewControllerDelegate>
{
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

@property (strong, nonatomic) SWRevealViewController *revealView;
@property(nonatomic,retain)UINavigationController *navigationController;

@end
