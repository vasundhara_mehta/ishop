//
//  AddCartViewController.h
//  IShop
//
//  Created by Hashim on 5/1/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>


@interface AddCartViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIAlertViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate>
{
    AppDelegate *objAppDelegate;
    IBOutlet UILabel *lblNavTitle;
    
    float totalPrice;
    int iRequest;
    
    int btnTag;
    
    int selectedItemIndex,selectedColorIndex,selectedSizeIndex,selectedQuantityIndex;
    
    NSString *strHoldCurrentPrice;
    
    NSMutableDictionary *dictProductAddToCart;
    NSMutableDictionary *dictCartToCustomerResponse;
    NSMutableArray *mArrayQuantity;
    
    IBOutlet UIView *viewMoreInfo;
    IBOutlet UITextView *txtViewMoreInfo;
    IBOutlet UIButton *btnCloseMorInfo;
    
    int isWebserviceCount;
    IBOutlet UIImageView *imgViewProducts;

    NSString *strDefaultPrice;
    NSMutableArray *mutArrayProductImages;

    IBOutlet UILabel *lblProductName;
    IBOutlet UILabel *lblProductPrice;
    IBOutlet UILabel *lblProductProductCode;
    IBOutlet UILabel *lblProductProductDiscription;
   
    int index;
    int isSelectedPicker;

    BOOL isRecomded;

    BOOL recent_is;
    BOOL isProductEdit;
    
    NSString *strAttributeTitile;
    
    int selectedIndexViewMore;
    
    NSMutableArray *optionJsonValues;
    
    NSString *strCartId;
    
    UITextField *txtFieldCoupon;
}

@property (weak, nonatomic) IBOutlet UILabel *lblRecentItems;
@property (weak, nonatomic) IBOutlet UIButton *buttonClearAll;

@property (weak, nonatomic) IBOutlet UICollectionView *CollectionMoreInfo;


@property (weak, nonatomic) IBOutlet UIButton *buttonBuythelook;
@property (weak, nonatomic) IBOutlet UIButton *buttonWeRecom;
@property (weak, nonatomic) IBOutlet UIButton *buttonRecentView;
@property (weak, nonatomic) IBOutlet UILabel *lblButTheItems;
@property (weak, nonatomic) IBOutlet UIPageControl *pagecontroller;

@property (nonatomic, retain) IBOutlet UIToolbar *pickerToolbar1;
@property (nonatomic,retain) IBOutlet UIPickerView *pickerViewColorandSize;

@property (nonatomic, retain) NSMutableDictionary *mutDictProductDetail;

@property(nonatomic ,readwrite)int requestFor;

@property (nonatomic, retain) NSString *strQty, *strProductSize, *strProductColour, *strPrices;

//Property Mutable Array iVar
@property (nonatomic, retain) NSMutableArray *arrProductColor;
@property (nonatomic, retain) NSMutableArray *arrProductSize;
@property (nonatomic, retain) NSMutableArray *arrProductColorAndSize;

@property (nonatomic, retain) NSMutableDictionary *mDictEditDetail;

@property (nonatomic, retain) NSMutableData *dataSize;

@property (nonatomic, retain) IBOutlet UITableView *tblViewProductBag;
@property (nonatomic, retain)  NSMutableArray *arrProducts;
@property (nonatomic,retain) IBOutlet UIToolbar *pickerToolbar;
@property (nonatomic,retain) IBOutlet UIPickerView *pickerViewDetail;

@property(nonatomic ,retain)IBOutlet UIView *viewProductDetail;
@property(nonatomic ,retain)IBOutlet UITableView *tableViewPrdAttributes;
@property(nonatomic ,retain)IBOutlet UIView *viewPrdAttributes;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollViewProductDetail;
@property (nonatomic, retain) IBOutlet UIScrollView *scrolViewPrductImages;

@property (nonatomic, retain) NSMutableArray *mArrayProductAttributes;



- (IBAction) btnBackAction:(id)sender;
- (void)btnContinueAction;
- (IBAction)btnDeleteAction:(id)sender;
- (IBAction)btnCancelToolbarAction:(id)sender;
- (IBAction)btnDoneToolbarAction:(id)sender;

@end
