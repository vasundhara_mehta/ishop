//
//  AddCartViewController.m
//  IShop
//
//  Created by Hashim on 5/1/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "AddCartViewController.h"
#import "AddToBagCell.h"
#import "UIImageView+WebCache.h"
#import "BillingAddressViewController.h"
#import "ASIFormDataRequest.h"
#import "MBProgressHUD.h"
#import "SignInViewController.h"
#import "iShopServices.h"
#import "JSON.h"
#import "Constants.h"
#import "MoreInfoCollectionCell.h"
#import "CustomCell.h"
#import "ViewFullSizeImageVC.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <Social/Social.h>
#import "ProductDetailView.h"


#define kNotificationDeleteProduct @"DeleteProduct"
#define kNotificationEditProduct @"EditProduct"
#define kNotificationAddProduct @"AddProduct"


@interface AddCartViewController ()<GPPSignInDelegate>

@end

@implementation AddCartViewController

@synthesize arrProducts,tblViewProductBag, pickerViewDetail, dataSize, arrProductColor, arrProductColorAndSize, arrProductSize, strProductSize, strQty, mDictEditDetail, strProductColour,strPrices;

@synthesize requestFor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    isProductEdit = NO;
    self.tblViewProductBag.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tblViewProductBag.bounds.size.width, 0.01f)];
    
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    selectedColorIndex = selectedQuantityIndex = selectedSizeIndex = -1;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deleteProduct:) name:kNotificationDeleteProduct object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(editProduct:) name:kNotificationEditProduct object:nil];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        if([objAppDelegate.strCartId isEqualToString:@"<null>"] || objAppDelegate.strCartId == nil)
        {
            [self getCartId];
        }
    }
}

-(void)getCartId
{
    //getCartId
    [iShopServices GetMethodWithApiMethod:@"getCartId" WithSuccess:^(id response)
    {
        NSLog(@"Response Cart id = %@",[response JSONValue]);
    } failure:^(NSError *error) {
        NSLog(@"Error = %@",[error description]);
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSLog(@"--Bool--%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"cart_array"]);
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    NSLog(@"Cart arrary -- %@",objAppDelegate.arrAddToBag);
    
    [self.tblViewProductBag setBackgroundColor:[UIColor clearColor]];
    
    [self getYourBagItem];
    [self setTotalValue];
    
    selectedIndexViewMore = -1;
}

-(void)setContentView
{
    viewMoreInfo.hidden = YES;
    
    isWebserviceCount = 1;
    
    viewMoreInfo.hidden = YES;
    //self.pickerToolbar1.hidden = YES;
    self.pickerViewColorandSize.hidden = YES;
    
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    NSMutableString *request_gallery = [NSMutableString stringWithFormat:@"%@", [NSString stringWithFormat:product_gallery, [_mutDictProductDetail objectForKey:@"prd_id"]]];
    
    NSMutableString *request_options = [NSMutableString stringWithFormat:@"%@", [NSString stringWithFormat:product_option, [_mutDictProductDetail objectForKey:@"prd_id"]]];
    
    [self getResponce:request_gallery :1];
    [self getResponce:request_options :2];
    
    NSLog(@"Dict -- %@",_mutDictProductDetail);
    
    if (IS_IPHONE5)
    {
        self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, 891);
    }
    else
    {
        self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, 891);
    }
    self.scrollViewProductDetail.contentOffset = CGPointMake (self.scrollViewProductDetail.bounds.origin.x, 0);
    
    lblProductName.text = [_mutDictProductDetail objectForKey:@"prd_name"];
    lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,[[_mutDictProductDetail objectForKey:@"prd_price"] floatValue]];
    
    strDefaultPrice = [_mutDictProductDetail objectForKey:@"prd_price"];
    
    lblProductProductCode.text = [NSString stringWithFormat:@"Product Code %@",[_mutDictProductDetail objectForKey:@"prd_sku"]];
    lblProductProductDiscription.text = [_mutDictProductDetail objectForKey:@"prd_desc"];
}

- (void) getResponce:(NSString *)strURL :(int)service_count
{
    NSURL *url = [NSURL URLWithString:strURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60.0];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data,NSError *connectionError){
                               
                               if (service_count == 1)
                               {
                                   for (UIImageView *subImageView in self.scrolViewPrductImages.subviews)
                                   {
                                       [subImageView removeFromSuperview];
                                   }
                                   mutArrayProductImages = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                   NSLog(@"Responce-- %@",mutArrayProductImages);
                                   
                                   self.scrolViewPrductImages.contentSize = CGSizeMake(320 * [mutArrayProductImages count], self.scrolViewPrductImages.frame.size.height);
                                   self.scrolViewPrductImages.contentOffset = CGPointMake (self.scrolViewPrductImages.bounds.origin.x, self.scrollViewProductDetail.bounds.origin.y);
                                   _pagecontroller.numberOfPages = mutArrayProductImages.count;
                                   _pagecontroller.currentPage=0;
                                   
                                   for (int i = 0; i < [mutArrayProductImages count]; i++)
                                   {
                                       imgViewProducts = [[UIImageView alloc] initWithFrame:CGRectMake(i * 320, 0, 320,350)];
                                       imgViewProducts.backgroundColor = [UIColor clearColor];
                                       imgViewProducts.contentMode = UIViewContentModeScaleAspectFit;
                                       
                                       NSString *strURL = [[mutArrayProductImages objectAtIndex:i] objectForKey:@"prd_img"];
                                       
                                       __block UIActivityIndicatorView *activityIndicatorForImage = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                                       activityIndicatorForImage.center = imgViewProducts.center;
                                       activityIndicatorForImage.hidesWhenStopped = YES;
                                       
                                       // Here we use the new provided setImageWithURL: method to load the web image
                                       [imgViewProducts setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType){
                                           if(!error ){
                                               [activityIndicatorForImage removeFromSuperview];
                                           }
                                       }];
                                       [imgViewProducts addSubview:activityIndicatorForImage];
                                       
                                       UITapGestureRecognizer *singleTap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(productGalleryFullView:)];
                                       imgViewProducts.tag=i;
                                       singleTap.view.tag=imgViewProducts.tag;
                                       imgViewProducts.userInteractionEnabled=YES;
                                       [imgViewProducts addGestureRecognizer:singleTap];
                                       
                                       [self.scrolViewPrductImages addSubview:imgViewProducts];
                                   }
                               }
                               else
                               {
                                   NSMutableArray *mArrayResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                   arrProductColorAndSize = [[NSMutableArray alloc]init];
                                   for(NSMutableDictionary *mDict in mArrayResponse)
                                   {
                                       
                                       NSString *jsonString = [_mutDictProductDetail valueForKey:@"option_json_value"];
                                       
                                       BOOL isOption = NO;
                                       NSMutableArray *optionArray = [jsonString JSONValue];
                                       
                                       for (int i = 0; i < [optionArray count]; i++)
                                       {
                                           NSString *strText = @"";
                                           id obj = [[optionArray objectAtIndex:i]valueForKey:@"option_value"];
                                           if (obj != nil)
                                           {
                                               strText = [strText stringByAppendingString:[[optionArray objectAtIndex:i]valueForKey:@"option_value"]];
                                           }
                                           
                                           for (NSMutableDictionary *dict in [mDict valueForKey:@"custome_values"])
                                           {
                                               if([[dict valueForKey:@"title"] isEqualToString:strText])
                                               {
                                                   isOption = YES;
                                                   [mDict setValue:strText forKey:@"prd_Attribute_Title"];
                                                   break;
                                               }
                                           }
                                       }
                                       if(!isOption)
                                       {
                                            [mDict setValue:[mDict valueForKey:@"custome_title"] forKey:@"prd_Attribute_Title"];
                                       }
                                       [arrProductColorAndSize addObject:mDict];
                                   }
                                   
                                   NSLog(@"Responce-- %@",arrProductColorAndSize);
                                   
                                   CGRect newTableFrame = self.tableViewPrdAttributes.frame;
                                   
                                   newTableFrame.size.height = [arrProductColorAndSize count] * 36;
                                   self.tableViewPrdAttributes.frame = newTableFrame;
                                   
                                   CGRect viewProductFrame = self.viewPrdAttributes.frame;
                                   
                                   viewProductFrame.origin.y = self.tableViewPrdAttributes.frame.origin.y + self.tableViewPrdAttributes.frame.size.height ;
                                   
                                   self.viewPrdAttributes.frame = viewProductFrame;
                                   
                                   float heightScroll = 891 + ([arrProductColorAndSize count]-1) * 36;
                                   self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, heightScroll);
                                   
                                   [self.tableViewPrdAttributes reloadData];
                               }
                           }];
}

- (BOOL)matchFoundForEventId:(NSString *)eventId WithArray:(NSMutableArray *)ConditionArray
{
    index = 0;
    for (NSDictionary *dataDict in ConditionArray)
    {
        NSString *createId = [dataDict objectForKey:@"prd_id"];
        if ([eventId isEqualToString:createId])
        {
            return YES;
        }
        index++;
    }
    return NO;
}

- (IBAction)change_image_via_page:(id)sender
{
    CGFloat x = _pagecontroller.currentPage * self.scrolViewPrductImages.frame.size.width;
    [self.scrolViewPrductImages setContentOffset:CGPointMake(x, 0) animated:YES];
}


#pragma mark - ScrollView  Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrolViewPrductImages.frame.size.width;
    int pageNo = floor((self.scrolViewPrductImages.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pagecontroller.currentPage=pageNo;
    
}
//end new dev

-(IBAction)actionHideProductDetail:(id)sender
{
    isProductEdit = NO;
    _viewProductDetail.hidden = YES;
    pickerViewDetail.hidden = YES;
    [UIView transitionFromView:_viewProductDetail toView:self.tblViewProductBag duration:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft
                    completion: ^(BOOL inFinished) {
                        //do any post animation actions here
                    }];
    [self.tblViewProductBag reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-Action method

- (IBAction) btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)btnContinueAction
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"])
    {
        [self setCustomerToCart];
    }
    else
    {
        SignInViewController *objSignIn;
        objSignIn = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
        [self.navigationController pushViewController:objSignIn animated:YES];
    }
}

-(IBAction)btnEditAction:(id)sender
{
    _mutDictProductDetail = [objAppDelegate.arrAddToBag objectAtIndex:[sender tag]];
    
    _viewProductDetail.hidden = NO;
    if(IS_IPHONE5)
    {
        _viewProductDetail.frame = CGRectMake(_viewProductDetail.frame.origin.x, _viewProductDetail.frame.origin.y, 320, 568);
    }
    else
    {
        _viewProductDetail.frame = CGRectMake(_viewProductDetail.frame.origin.x, _viewProductDetail.frame.origin.y, 320, 480);
    }
    
    [UIView transitionFromView:self.tblViewProductBag toView:_viewProductDetail duration:1.0 options:UIViewAnimationOptionTransitionFlipFromRight
                    completion: ^(BOOL inFinished) {
                        //do any post animation actions here
                        NSLog(@"test");
                        isProductEdit = YES;
                        [self setContentView];
                    }];
}

- (IBAction)btnEditApplyAction:(id)sender
{
    NSMutableDictionary *mDict1 = [objAppDelegate.arrAddToBag objectAtIndex:[sender tag]];
    
    NSDictionary *parameters1 = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"product_id":[mDict1 valueForKeyPath:@"prd_id"],@"uniq_id":[mDict1 valueForKey:@"uniq_id"],@"get":@"0"};
    
    [iShopServices PostMethodWithApiMethod:@"GetUserCheckoutHistory" Withparms:parameters1 WithSuccess:^(id response) {
        //self.arrAddToBag = [response JSONValue];
        objAppDelegate.requestFor = DELETEPRODUCT;
        [objAppDelegate getCheckOutHistory];
        
    } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
     }];

    self.pickerViewColorandSize.hidden = YES;
    [self.pickerToolbar1 removeFromSuperview];
    
    NSString *product_id = [_mutDictProductDetail objectForKey:@"prd_id"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    iRequest = 3;
    
    NSMutableArray *jsonArray = [[NSMutableArray alloc]init];
    NSLog(@"ggg %@",[arrProductColorAndSize objectAtIndex:[sender tag]]);

    for(NSMutableDictionary *mDict in arrProductColorAndSize)
    {
        NSLog(@"mdictttt %@",mDict);
        NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]init];
        [jsonDict setValue:[mDict valueForKey:@"prd_Attribute_Title"] forKey:@"option_value"];
        [jsonDict setValue:[mDict valueForKey:@"custome_title"] forKey:@"option_name"];
        //[arrProductColorAndSize replaceObjectAtIndex:[arrProductColorAndSize indexOfObject:mDict] withObject:jsonDict];
        NSLog(@"indexx %@",arrProductColorAndSize);

        [jsonArray addObject:jsonDict];
    }
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonArray options:NSJSONWritingPrettyPrinted error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //  NSLog(@"JSON Output: %@", jsonString);
    
    int randomNumber = arc4random() % 999999;
    NSLog(@"Random No = %d",randomNumber);
    
    lblProductPrice.text = [lblProductPrice.text substringFromIndex:1];
    NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"prd_qty":[_mutDictProductDetail objectForKey:@"prd_qty"],@"product_id":product_id,@"option_json_value":jsonString,@"uniq_id":[NSString stringWithFormat:@"%d",randomNumber],@"prd_default_price":[_mutDictProductDetail valueForKey:@"prd_price"],@"prd_update_price":lblProductPrice.text,@"get":@"1"};
    [iShopServices PostMethodWithApiMethod:@"GetUserCheckoutHistory" Withparms:parameters WithSuccess:^(id response)
     {
         // NSLog(@"Response = %@",[response JSONValue]);
         objAppDelegate.requestFor = EDITPRODUCT;
         [objAppDelegate getCheckOutHistory];
         //self.arrAddToBag = [response JSONValue];
         self.viewProductDetail.hidden=YES;
     } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
     }];
    [tblViewProductBag reloadData];
}

-(void) getYourBagItem
{
    int bagCount = 0;
    for(int i = 0; i < [objAppDelegate.arrAddToBag count]; i++)
    {
        NSString *strQutanty = [[objAppDelegate.arrAddToBag objectAtIndex:i] valueForKey:@"prd_qty"];
        bagCount = bagCount+[strQutanty intValue];
    }
    lblNavTitle.text = [NSString stringWithFormat:@"YOUR BAG : %i ITEMS" ,bagCount];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:bagCount forKey:@"bag_count"];
}

-(IBAction)btnEditQtyAction:(id)sender
{
    if(IS_IPHONE5)
    {
        self.pickerToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 362, 320, 44)];
        self.pickerViewDetail = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 406, 320, 162)];
        self.pickerViewDetail.delegate = self;
        self.pickerViewDetail.showsSelectionIndicator = YES;
        self.pickerViewDetail.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.pickerViewDetail];
    }
    else
    {
        self.pickerToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 274, 320, 44)];
        self.pickerViewDetail = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 318, 320, 162)];
        self.pickerViewDetail.delegate = self;
        self.pickerViewDetail.showsSelectionIndicator = YES;
        self.pickerViewDetail.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.pickerViewDetail];
    }
    
    UIButton *done_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    done_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [done_Button setTitle:@"Done" forState:UIControlStateNormal];
    [done_Button addTarget:self action:@selector(btnDoneToolbarAction:) forControlEvents:UIControlEventTouchUpInside];
    
    done_Button.tag = [sender tag];
    
    UIButton *cancel_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    cancel_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel_Button addTarget:self action:@selector(btnCancelToolbarAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *myButton = [[UIBarButtonItem alloc] initWithCustomView:done_Button];
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithCustomView:cancel_Button];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [self.pickerToolbar1 insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Bar.png"]] atIndex:0];
    
    CALayer *l=self.pickerToolbar1.layer;
    [l setCornerRadius:0.5];
    [l setBorderColor:[[UIColor grayColor] CGColor]];
    [l setBorderWidth:0.5];
    [self.pickerToolbar1 setItems:[NSArray arrayWithObjects:btnCancel, flex, myButton, nil]];
    [self.view addSubview:self.pickerToolbar1];
    
    selectedItemIndex = 2;
    
    mArrayQuantity = [[NSMutableArray alloc] initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", nil];
    
    [self.pickerViewDetail reloadAllComponents];
}

- (IBAction)btnCancelToolbarAction:(id)sender
{
    [self.pickerViewDetail removeFromSuperview];
    [self.pickerToolbar1 removeFromSuperview];
}

- (IBAction)CancelButtonPressed:(id)sender
{
    lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,[[_mutDictProductDetail objectForKey:@"prd_price"] floatValue]];
    self.pickerViewColorandSize.hidden = YES;
    [self.pickerToolbar1 removeFromSuperview];
}

- (IBAction)doneBtnPressToGetValue:(id)sender
{
    self.pickerViewColorandSize.hidden = YES;
    [self.pickerToolbar1 removeFromSuperview];
    [[arrProductColorAndSize objectAtIndex:isSelectedPicker]setValue:strAttributeTitile forKey:@"prd_Attribute_Title"];
    [self.tableViewPrdAttributes reloadData];
}

- (IBAction)btnDoneToolbarAction:(id)sender
{
    pickerViewDetail.hidden = YES;
    self.pickerToolbar.hidden = YES;
    
    if(selectedItemIndex == 1)
    {
        [self.mDictEditDetail setValue:strProductSize forKey:@"pro_size"];
    }
    else if(selectedItemIndex == 2)
    {
        [self.pickerViewDetail removeFromSuperview];
        [self.pickerToolbar1 removeFromSuperview];
        
        NSMutableDictionary *mDictProduct = [objAppDelegate.arrAddToBag objectAtIndex:[sender tag]];
        
        NSString *product_id = [mDictProduct objectForKey:@"prd_id"];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Please wait...";
        hud.dimBackground = YES;
        
        lblProductPrice.text = [lblProductPrice.text substringFromIndex:1];
        
        NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"prd_qty":strQty,@"product_id":product_id,@"option_json_value":[mDictProduct valueForKey:@"option_json_value"],@"uniq_id":[mDictProduct valueForKey:@"uniq_id"],@"prd_default_price":[mDictProduct valueForKey:@"prd_price"],@"prd_update_price":lblProductPrice.text,@"get":@"1"};
        
        [iShopServices PostMethodWithApiMethod:@"GetUserCheckoutHistory" Withparms:parameters WithSuccess:^(id response)
         {
             // NSLog(@"Response = %@",[response JSONValue]);
             objAppDelegate.requestFor = EDITPRODUCT;
             [objAppDelegate getCheckOutHistory];
             //self.arrAddToBag = [response JSONValue];
         } failure:^(NSError *error)
         {
             NSLog(@"Error =%@",[error description]);
         }];
        
    }
    else if(selectedItemIndex == 3)
    {
        float temp = [[self.mDictEditDetail valueForKey:@"prd_price"] floatValue];
        temp = temp + [strPrices floatValue];
        
        [self.mDictEditDetail setValue:strProductColour forKey:@"pro_color"];
        [self.mDictEditDetail setValue:[NSString stringWithFormat:@"%.02f",temp] forKey:@"prd_price"];
    }
}

- (IBAction)btnDeleteAction:(id)sender
{
    NSLog(@"index -- %i",[sender tag]);
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    NSMutableDictionary *mDict = [objAppDelegate.arrAddToBag objectAtIndex:[sender tag]];
    
    NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"product_id":[mDict valueForKeyPath:@"prd_id"],@"uniq_id":[mDict valueForKey:@"uniq_id"],@"get":@"0"};
    
    [iShopServices PostMethodWithApiMethod:@"GetUserCheckoutHistory" Withparms:parameters WithSuccess:^(id response) {
        //self.arrAddToBag = [response JSONValue];
        objAppDelegate.requestFor = DELETEPRODUCT;
        [objAppDelegate getCheckOutHistory];
        
    } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
     }];
}

-(void)deleteProduct:(NSNotification *)notification
{
    [self setTotalValue];
    [self getYourBagItem];
    [self.tblViewProductBag reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)editProduct:(NSNotification *)notification
{
    [self getYourBagItem];
    [self setTotalValue];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if(isProductEdit)
    {
        isProductEdit = NO;
        _viewProductDetail.hidden = YES;
        pickerViewDetail.hidden = YES;
        
        [UIView transitionFromView:_viewProductDetail toView:self.tblViewProductBag duration:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft
                        completion: ^(BOOL inFinished) {
                            //do any post animation actions here
                        }];
    }
    
    [self.tblViewProductBag reloadData];
}

#pragma mark - UIPicker Delegate Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView.tag==1000){
        return [_mArrayProductAttributes count];

    }else{
    if(selectedItemIndex == 1)
    {
        return [self.arrProductSize count];
    }
    else if(selectedItemIndex == 2)
    {
        return [mArrayQuantity count];
    }
    else if(selectedItemIndex == 3)
    {
        return [arrProductColor count];
    }
    else
    {
        return [_mArrayProductAttributes count];
    }
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40.0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *strValue;
    if (pickerView.tag==1000) {
        strValue = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];

    }else{
    if (selectedItemIndex == 1)
    {
        strValue = [[self.arrProductSize objectAtIndex:row] objectForKey:@"title"];
    }
    else if(selectedItemIndex == 2)
    {
        return [mArrayQuantity objectAtIndex:row];
    }
    else if(selectedItemIndex == 3)
    {
        strValue = [[self.arrProductColor objectAtIndex:row] objectForKey:@"title"];
    }
    else
    {
        strValue = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];
    }
    }
    return strValue;
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag==1000) {
        
    }else{
    if(selectedItemIndex == 1)
    {
        strProductSize = [[self.arrProductSize objectAtIndex:row] objectForKey:@"title"];
        selectedSizeIndex = row;
    }
    else if(selectedItemIndex == 2)
    {
        strQty = [mArrayQuantity objectAtIndex:row];
        selectedQuantityIndex = row;
    }
    else
    {
        strProductColour = [[self.arrProductColor objectAtIndex:row] objectForKey:@"title"];
        strPrices = [[self.arrProductColor objectAtIndex:row] objectForKey:@"price"];
        selectedColorIndex = row;
        //NSLog(@" price = %@",[[objAppDelegate.arrAddToBag objectAtIndex:row] valueForKey:@"prd_price"]);
    }
    }
    
    
    strAttributeTitile = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];
    
    float Price = 0.0;
    
    Price = [strDefaultPrice floatValue] + [[[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"price"] floatValue];
    
    lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,Price];
}

#pragma mark - UIAlert Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1000)
    {
        if(buttonIndex == 0)
        {
            BillingAddressViewController *objBillingAddressViewController;
            if (IS_IPHONE5)
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController" bundle:nil];
            }
            else
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController1" bundle:nil];
            }
            
            objBillingAddressViewController.requestFor = ADDTOCARTPAYMENT;
            
            objBillingAddressViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objBillingAddressViewController animated:YES];
        }
        if(buttonIndex == 1)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Please wait...";
            hud.dimBackground = YES;
            
            NSDictionary *parameters = @{@"CartId":objAppDelegate.strCartId,@"coupon":txtFieldCoupon.text};
            
            [iShopServices PostMethodWithApiMethod:@"ApplyCouponAdd" Withparms:parameters WithSuccess:^(id response)
             {
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Coupon Applied" message:@"You can cancel coupon or continue." delegate:self cancelButtonTitle:@"Cancel Coupon" otherButtonTitles:@"Continue", nil];
                 alert.tag = 8888;
                 alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                 txtFieldCoupon = [alert textFieldAtIndex:0];
                 [alert show];
                 
             } failure:^(NSError *error)
             {
                 NSLog(@"Error = %@",[error description]);
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid coupon code." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 alert.tag = 9999;
                 [alert show];
             }];
        }
    }
    else if (alertView.tag == 9999)
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Discount Codes" message:@"Enter your coupon code if you have one." delegate:self cancelButtonTitle:@"Skip" otherButtonTitles:@"Apply Coupon", nil];
        av.tag = 1000;
        av.alertViewStyle = UIAlertViewStylePlainTextInput;
        txtFieldCoupon = [av textFieldAtIndex:0];
        txtFieldCoupon.delegate = self;
        [av show];
    }
    else if (alertView.tag == 8888)
    {
        if(buttonIndex == 0)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Please wait...";
            hud.dimBackground = YES;
            
            NSDictionary *parameters = @{@"CartId":objAppDelegate.strCartId,@"coupon":txtFieldCoupon.text};
            
            [iShopServices PostMethodWithApiMethod:@"RemoveCouponAdd" Withparms:parameters WithSuccess:^(id response)
             {
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 BillingAddressViewController *objBillingAddressViewController;
                 if (IS_IPHONE5)
                 {
                     objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController" bundle:nil];
                 }
                 else
                 {
                     objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController1" bundle:nil];
                 }
                 objBillingAddressViewController.requestFor = ADDTOCARTPAYMENT;
                 objBillingAddressViewController.hidesBottomBarWhenPushed = YES;
                 [self.navigationController pushViewController:objBillingAddressViewController animated:YES];
             } failure:^(NSError *error)
             {
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 NSLog(@"Error = %@",[error description]);
             }];
        }
        else if(buttonIndex == 1)
        {
            BillingAddressViewController *objBillingAddressViewController;
            if (IS_IPHONE5)
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController" bundle:nil];
            }
            else
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController1" bundle:nil];
            }
            objBillingAddressViewController.requestFor = ADDTOCARTPAYMENT;
            objBillingAddressViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objBillingAddressViewController animated:YES];
        }
    }
}

#pragma mark - Custom Methods

+ (UIImage *) resizeImage:(UIImage *)orginalImage resizeSize:(CGSize)size
{
    CGFloat actualHeight = orginalImage.size.height;
    CGFloat actualWidth = orginalImage.size.width;
    
    float oldRatio = actualWidth/actualHeight;
    float newRatio = size.width/size.height;
    if(oldRatio < newRatio)
    {
        oldRatio = size.height/actualHeight;
        actualWidth = oldRatio * actualWidth;
        actualHeight = size.height;
    }
    else
    {
        oldRatio = size.width/actualWidth;
        actualHeight = oldRatio * actualHeight;
        actualWidth = size.width;
    }
    
    CGRect rect = CGRectMake(0.0,0.0,actualWidth,actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [orginalImage drawInRect:rect];
    orginalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return orginalImage;
}

-(void) setTotalValue
{
    if([objAppDelegate.arrAddToBag count] != 0)
    {
        NSMutableDictionary *mDictRecord = [[NSMutableDictionary alloc] init];
        float totalPriceQty = 0;
        int qtyTotal = 0;
        for (int i = 0; i<[objAppDelegate.arrAddToBag count]; i++)
        {
            mDictRecord = [objAppDelegate.arrAddToBag objectAtIndex:i];
            NSString *strQtyy = [mDictRecord valueForKey:@"prd_qty"];
            NSString *strPrice = [mDictRecord valueForKey:@"prd_price"];
            float price = [strPrice floatValue];
            int qty = [strQtyy intValue];
            
            qtyTotal = qtyTotal+qty;
            
            float prceQty = price*qty;
            totalPriceQty = totalPriceQty+prceQty;
        }
        NSLog(@"%f", totalPriceQty);
        totalPrice = totalPriceQty;
    }
    if([objAppDelegate.arrAddToBag count] == 0)
    {
        totalPrice = 0.0;
    }
}

- (void) setCustomerToCart
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    objAppDelegate.strCustomerPassword = [[NSUserDefaults standardUserDefaults] objectForKey:@"passwordSave"];
    
    
    NSLog(@"customer_id = %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"customer_id"]);
    NSLog(@"email = %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"email"]);
    NSLog(@"firstname = %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"]);
    NSLog(@"lastname = %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"lastname"]);
    NSLog(@"objAppDelegate.strCartId = %@",objAppDelegate.strCartId);
    NSLog(@"passwordSave = %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"passwordSave"]);

    NSDictionary *parameters = @{@"customer_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"customer_id"],@"email":[[NSUserDefaults standardUserDefaults] valueForKey:@"email"],@"firstname":[[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"],@"lastname":[[NSUserDefaults standardUserDefaults] valueForKey:@"lastname"],@"password":objAppDelegate.strCustomerPassword,@"CartId":objAppDelegate.strCartId};
    
    [iShopServices PostMethodWithApiMethod:@"SetCustomerIntoCart" Withparms:parameters WithSuccess:^(id response)
     {
         NSLog(@"Add Product Response= %@",[response JSONValue]);
         NSMutableDictionary *mDict = [response JSONValue];
        // strCartId = [mDict valueForKey:@"CartId"];
        // [self setCustomerToCart];
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         objAppDelegate.strCartId = [mDict objectForKey:@"value"] ;
         UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Discount Codes" message:@"Enter your coupon code if you have one." delegate:self cancelButtonTitle:@"Skip" otherButtonTitles:@"Apply Coupon", nil];
         av.tag = 1000;
         av.alertViewStyle = UIAlertViewStylePlainTextInput;
         txtFieldCoupon = [av textFieldAtIndex:0];
         txtFieldCoupon.delegate = self;
         [av show];
         
     } failure:^(NSError *error)
    {
         NSLog(@"Error = %@",[error description]);
     }];
}

- (void) setProductToCart
{
    NSMutableArray *arrayProducts = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dictProduct in objAppDelegate.arrAddToBag)
    {
        NSString *strProducts = [[dictProduct objectForKey:@"prd_id"]  stringByAppendingFormat:@"~%@",[[dictProduct objectForKey:@"prd_qty"] stringByAppendingFormat:@"~%@", [dictProduct objectForKey:@"prd_sku"]]];
        [arrayProducts addObject:strProducts];
    }
    
    NSString *productOnCart  = [arrayProducts componentsJoinedByString:@"|"];
    
    objAppDelegate.strCartId = [dictCartToCustomerResponse objectForKey:@"value"];
    
    iRequest = 1;
    NSURL *url = [NSURL URLWithString:set_cart_product];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setPostValue:productOnCart forKey:@"products"];
    [request setPostValue:[dictCartToCustomerResponse objectForKey:@"value"] forKey:@"CartId"];
    [request setDidFinishSelector:@selector(requestFinishedForService:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDelegate:self];
    [request startAsynchronous];
}

#pragma mark - ASIHTTP Response

-(void)requestFinishedForService:(ASIHTTPRequest *)request
{
    if (iRequest == 0)
    {
        // NSString *string =[request responseString];
        NSData *data = [request responseData];
        
        NSLog(@"%@", [request responseString]);
        
        dictCartToCustomerResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        [self setProductToCart];
    }
    else if (iRequest == 1)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //NSString *string =[request responseString];
        NSData *data = [request responseData];
        
        NSLog(@"%@", [request responseString]);
        
        NSDictionary *dictResponce = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        BOOL checkResponse = [[dictResponce objectForKey:@"value"] boolValue];
        
        if (checkResponse)
        {
            BillingAddressViewController *objBillingAddressViewController;
            if (IS_IPHONE5)
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController" bundle:nil];
            }
            else
            {
                objBillingAddressViewController = [[BillingAddressViewController alloc] initWithNibName:@"BillingAddressViewController1" bundle:nil];
            }
            
            objBillingAddressViewController.requestFor = ADDTOCARTPAYMENT;
            
            objBillingAddressViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objBillingAddressViewController animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Check your network connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isProductEdit)
    {
        return [arrProductColorAndSize count];
    }
    return [objAppDelegate.arrAddToBag count];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(isProductEdit)
    {
        return nil;
    }
    else
    {
        UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
        viewFooter.backgroundColor = [UIColor whiteColor];
        
        UILabel *lblTotal = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 35, 20)];
        lblTotal.text = @"Total";
        lblTotal.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
        lblTotal.textColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1.0];
        lblTotal.backgroundColor = [UIColor clearColor];
        [viewFooter addSubview:lblTotal];
        
        UILabel *lblWithoutDelivery = [[UILabel alloc] initWithFrame:CGRectMake(42, 10, 130, 20)];
        lblWithoutDelivery.text = @"(without delivery):";
        lblWithoutDelivery.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
        lblWithoutDelivery.textColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0];
        lblWithoutDelivery.backgroundColor = [UIColor clearColor];
        [viewFooter addSubview:lblWithoutDelivery];
        
        UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(200, 10, 105, 20)];
        lblPrice.text = @"Price";
        lblPrice.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
        lblPrice.font = [UIFont boldSystemFontOfSize:14.0f];
        lblPrice.textColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0];
        lblPrice.textAlignment = NSTextAlignmentRight;
        //NSString *str = [NSString stringWithFormat:@""]
        lblPrice.text = [NSString stringWithFormat:@"%@ %.02f",objAppDelegate.currencySymbol,totalPrice];
        lblPrice.backgroundColor = [UIColor clearColor];
        [viewFooter addSubview:lblPrice];
        
        UIButton *btnPayment = [[UIButton alloc] initWithFrame:CGRectMake(10, 50, 300, 30)];
        btnPayment.backgroundColor = [UIColor clearColor];
        [btnPayment setImage:[UIImage imageNamed:@"btn_Pay_Securely_Now.png"] forState:UIControlStateNormal];
        [btnPayment setImage:[UIImage imageNamed:@"btn_Pay_Securely Now_hover.png"] forState:UIControlStateHighlighted];
        [btnPayment addTarget:self action:@selector(btnContinueAction) forControlEvents:UIControlEventTouchUpInside];
        [viewFooter addSubview:btnPayment];
        NSLog(@"__FUCTION__");
        return viewFooter;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isProductEdit)
    {
        static NSString *cellIdentifier = @"Prd_Attributes";
        CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            NSArray *cellView = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
            cell = (CustomCell *)[cellView objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
        
        if([[[arrProductColorAndSize objectAtIndex:indexPath.row]valueForKey:@"prd_Attribute_Title"] isEqualToString:@""])
        {
            cell.lblPrdAttibuteTitle.text = [[arrProductColorAndSize objectAtIndex:indexPath.row]valueForKey:@"custome_title"];
        }
        else
        {
            cell.lblPrdAttibuteTitle.text = [[arrProductColorAndSize objectAtIndex:indexPath.row]valueForKey:@"prd_Attribute_Title"];
        }
        
        [cell.btnPicker addTarget:self action:@selector(btnColorAndSizeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnPicker.tag = indexPath.row;
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier;
        CellIdentifier = @"AddToBagCell";
        AddToBagCell *cell = (AddToBagCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        //if (cell == nil)
        //  {
        cell = [[AddToBagCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddToBagCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        // }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.viewDetail.hidden = NO;
        cell.viewEdit.hidden = YES;
        
        NSMutableDictionary *dictCartProduct = [objAppDelegate.arrAddToBag objectAtIndex:indexPath.row];
        NSString *str=[dictCartProduct objectForKey:@"prd_name"];

        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        
        NSDictionary * attributes = @{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16],
                                      NSParagraphStyleAttributeName : paragraphStyle};
        
        CGSize size = [str boundingRectWithSize:(CGSize){138 - (10 * 2), 20000.0f}
                                        options:NSStringDrawingUsesFontLeading
                       |NSStringDrawingUsesLineFragmentOrigin
                                     attributes:attributes
                                        context:nil].size;
        
        CGFloat height = MAX(size.height, 10.0);
        cell.lblProductName.frame=CGRectMake(141, 0, 138, height+20);
        cell.lblProductName.numberOfLines=0;
        cell.lblProductName.lineBreakMode=NSLineBreakByWordWrapping;
        cell.lblProductName.text = [dictCartProduct objectForKey:@"prd_name"];
        cell.lblProductName.lineBreakMode = NSLineBreakByWordWrapping;
        cell.lblProductName.numberOfLines = 0;

        cell.lblProductName.text = [dictCartProduct objectForKey:@"prd_name"];
        
        [cell.imgViewProduct setImageWithURL:[NSURL URLWithString:[dictCartProduct valueForKey:@"prd_thumb"]] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"]];
        
        cell.imgViewProduct.contentMode = UIViewContentModeScaleAspectFit;
        
        NSString *jsonString = [dictCartProduct valueForKey:@"option_json_value"];
        
        NSMutableArray *optionArray = [jsonString JSONValue];
        
        float yPosition = 30+height-20;
        
        cell.lblViewMore.text = @"View More";
        
        for (int i = 0; i < [optionArray count]; i++)
        {
            if(selectedIndexViewMore == indexPath.row)
            {
                cell.lblViewMore.text = @"View Less";
                UILabel *lblMessage = [[UILabel alloc]initWithFrame:CGRectMake(141, yPosition, 160, 18)];
                NSString *strText = [[optionArray objectAtIndex:i]valueForKey:@"option_name"];
                strText = [strText stringByAppendingString:@": "];
                
                id obj = [[optionArray objectAtIndex:i]valueForKey:@"option_value"];
                if (obj != nil)
                {
                    strText = [strText stringByAppendingString:[[optionArray objectAtIndex:i]valueForKey:@"option_value"]];
                    lblMessage.text = strText;
                    yPosition = yPosition + 16;
                    [lblMessage setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
                    [cell.contentView addSubview:lblMessage];
                }
            }
            else if (i < 3)
            {
                UILabel *lblMessage = [[UILabel alloc]initWithFrame:CGRectMake(141, yPosition, 160, 18)];
                NSString *strText = [[optionArray objectAtIndex:i]valueForKey:@"option_name"];
                strText = [strText stringByAppendingString:@": "];
                
                id obj = [[optionArray objectAtIndex:i]valueForKey:@"option_value"];
                if (obj != nil)
                {
                    strText = [strText stringByAppendingString:[[optionArray objectAtIndex:i]valueForKey:@"option_value"]];
                    lblMessage.text = strText;
                    yPosition = yPosition + 16;
                    [lblMessage setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
                    [cell.contentView addSubview:lblMessage];
                }
            }
        }
        if(selectedIndexViewMore == indexPath.row){
            if (yPosition>32) {
                cell.btnEditQty.frame=CGRectMake(141, cell.bounds.size.height-70, 70, 23);
                cell.lblQty.frame=CGRectMake(163, cell.bounds.size.height-70, 25, 21);
                cell.btnViewMore.frame=CGRectMake(232, cell.bounds.size.height-70, 75, 23);
                cell.lblViewMore.frame=CGRectMake(239, cell.bounds.size.height-67, 61, 18);
                //                cell.lblPrice.frame=CGRectMake(141, yPosition-120, 54, 21);
                //                cell.btnMoveToBag.frame=CGRectMake(194, yPosition-120, 84, 25);
                //                cell.btn_Delete.frame=CGRectMake(285, yPosition-120, 32, 22);
            }else{
                NSLog(@"comes in elkse");
            }
        }

        cell.lblQty.text = [dictCartProduct objectForKey:@"prd_qty"];
        
        float productPrice = [[dictCartProduct valueForKey:@"prd_price"] floatValue];
        float updatedProductPrice = [[dictCartProduct valueForKey:@"prd_update_price"] floatValue];
        
        if(updatedProductPrice == 0)
        {
            productPrice = productPrice* [[dictCartProduct objectForKey:@"prd_qty"] floatValue];
            cell.lblPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,productPrice];
        }
        else
        {
            updatedProductPrice = updatedProductPrice * [[dictCartProduct objectForKey:@"prd_qty"] floatValue];
            cell.lblPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,updatedProductPrice];
        }
        
        //cell.lblPrice.text = [NSString stringWithFormat:@"$%.02f", productPrice];
        
        cell.btn_Edit.tag = indexPath.row;
        [cell.btn_Edit addTarget:self action:@selector(btnEditAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btn_Delete.tag = indexPath.row;
        [cell.btn_Delete addTarget:self action:@selector(btnDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnProductDetail.tag = indexPath.row;
        [cell.btnProductDetail addTarget:self action:@selector(actionProductDetail:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnEditQty.tag = indexPath.row;
        [cell.btnEditQty addTarget:self action:@selector(btnEditQtyAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnViewMore.hidden = NO;
        cell.lblViewMore.hidden = NO;
        
        if([optionArray count] < 3)
        {
            cell.btnViewMore.hidden = YES;
            cell.lblViewMore.hidden = YES;
        }
        cell.btnViewMore.tag = indexPath.row;
        [cell.btnViewMore addTarget:self action:@selector(actionViewMore:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

#pragma mark - UITableView Delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dictCartProduct = [objAppDelegate.arrAddToBag objectAtIndex:indexPath.row];
    NSString *str=[dictCartProduct objectForKey:@"prd_name"];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary * attributes = @{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    CGSize size = [str boundingRectWithSize:(CGSize){138 - (10 * 2), 20000.0f}
                                    options:NSStringDrawingUsesFontLeading
                   |NSStringDrawingUsesLineFragmentOrigin
                                 attributes:attributes
                                    context:nil].size;
    
    CGFloat height1 = MAX(size.height, 5);

    if(isProductEdit)
    {
        return 36;
    }
    
    if(selectedIndexViewMore != indexPath.row)
    {
        return  125+height1;
    }
    else
    {

        NSString *jsonString = [dictCartProduct valueForKey:@"option_json_value"];
        NSMutableArray *optionArray = [jsonString JSONValue];
        CGFloat ypos=28;
        for (int i=0; i<optionArray.count; i++) {
            ypos=ypos+16;
        }

        int cunter = [optionArray count];
        
        if(cunter < 3)
        {
            return 150;
        }
        return   ypos+height1+54;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(isProductEdit)
    {
        return 0;
    }
    return 100.0;
}

-(IBAction)actionViewMore:(id)sender
{
    if(selectedIndexViewMore == -1 || selectedIndexViewMore != [sender tag])
    {
        selectedIndexViewMore = [sender tag];
    }
    else
    {
        selectedIndexViewMore = -1;
    }
    
    [tblViewProductBag reloadData];
}

- (IBAction)btnColorAndSizeAction:(id)sender
{
    if(IS_IPHONE5)
    {
        self.pickerToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 362, 320, 44)];
    }
    else
    {
        self.pickerToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 274, 320, 44)];
    }
    
    UIButton *done_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    done_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [done_Button setTitle:@"Done" forState:UIControlStateNormal];
    [done_Button addTarget:self action:@selector(doneBtnPressToGetValue:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *cancel_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    cancel_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel_Button addTarget:self action:@selector(CancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *myButton = [[UIBarButtonItem alloc] initWithCustomView:done_Button];
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithCustomView:cancel_Button];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [self.pickerToolbar1 insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Bar.png"]] atIndex:0];
    
    CALayer *l=self.pickerToolbar1.layer;
    [l setCornerRadius:0.5];
    [l setBorderColor:[[UIColor grayColor] CGColor]];
    [l setBorderWidth:0.5];
    [self.pickerToolbar1 setItems:[NSArray arrayWithObjects:btnCancel, flex, myButton, nil]];
    [self.viewProductDetail addSubview:self.pickerToolbar1];
    
    
    isSelectedPicker  = [sender tag];
    
    _mArrayProductAttributes = [[arrProductColorAndSize objectAtIndex:isSelectedPicker] objectForKey:@"custome_values"];
    
    self.pickerViewColorandSize.hidden = NO;
    
    [self.pickerViewColorandSize reloadAllComponents];
}

-(IBAction)actionProductDetail:(id)sender
{
        ProductDetailView *objProductDetailView;
        if (IS_IPHONE5)
        {
            objProductDetailView = [[ProductDetailView alloc] initWithNibName:@"ProductDetailView" bundle:nil];
        }
        else
        {
            objProductDetailView = [[ProductDetailView alloc] initWithNibName:@"ProductDetailView_iphone3.5" bundle:nil];
        }
    
        objProductDetailView.mutDictProductDetail = [objAppDelegate.arrAddToBag objectAtIndex:[sender tag]];
        [self.navigationController pushViewController:objProductDetailView animated:YES];
}


-(void)productGalleryFullView:(UITapGestureRecognizer *)gesture
{
    //gesture.view.tag
    
    NSString *strXIB = @"ViewFullSizeImageVC_iPhone3.5";
    if(IS_IPHONE5)
    {
        strXIB = @"ViewFullSizeImageVC";
    }
    ViewFullSizeImageVC *viewfullsizeimageview=[[ViewFullSizeImageVC alloc]initWithNibName:strXIB bundle:nil];
    viewfullsizeimageview.image_tag = gesture.view.tag;
    viewfullsizeimageview.imageArray = mutArrayProductImages;
    [self presentViewController:viewfullsizeimageview animated:YES completion:nil];
}

#pragma mark - Image Sharing From Face Book

-(IBAction)btnActionFaceBookSharing:(id)sender
{
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [controller setInitialText:@"What do you think? Should i buy this?"];
    [controller addURL:[NSURL URLWithString:[_mutDictProductDetail valueForKey:@"product_url"]]];
    
    [controller addImage:imgViewProducts.image];
    [self presentViewController:controller animated:YES completion:Nil];
}

-(IBAction)btnActionTwitterSharing:(id)sender
{
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [controller setInitialText:@"What do you think? Should I buy this?"];
    [controller addURL:[NSURL URLWithString:[_mutDictProductDetail valueForKey:@"product_url"]]];
    [controller addImage:imgViewProducts.image];
    
    [self presentViewController:controller animated:YES completion:Nil];
}

-(IBAction)btnActionMailSharing:(id)sender
{
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init]  ;
	mailComposer.mailComposeDelegate = self;
	
	if ([MFMailComposeViewController canSendMail])
    {
		[mailComposer setToRecipients:nil];
		[mailComposer setSubject:@"What do you think? Should I buy this?"];
        NSString *message = [@"Check it out at:\n\n\n" stringByAppendingString:[_mutDictProductDetail valueForKey:@"product_url"]];
		[mailComposer setMessageBody:message isHTML:YES];
        NSData *imageData = UIImagePNGRepresentation(imgViewProducts.image);
		[mailComposer addAttachmentData:imageData mimeType:@"image/jpeg" fileName:@""];
		
        [self presentViewController:mailComposer animated:YES completion:nil];
	}
}

-(IBAction)btnActionGooglePlus:(id)sender
{
    static NSString * const kClientID =@"586890710364-pbld9i8gsg7ma3qu54ahn6jdpfc734rr.apps.googleusercontent.com";
    
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.clientID = kClientID;
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    
    [GPPSignIn sharedInstance].actions = [NSArray arrayWithObjects:
                                          @"http://schemas.google.com/AddActivity",
                                          @"http://schemas.google.com/BuyActivity",
                                          @"http://schemas.google.com/CheckInActivity",
                                          @"http://schemas.google.com/CommentActivity",
                                          @"http://schemas.google.com/CreateActivity",
                                          @"http://schemas.google.com/ListenActivity",
                                          @"http://schemas.google.com/ReserveActivity",
                                          @"http://schemas.google.com/ReviewActivity",
                                          nil];
    signIn.delegate = self;
    [signIn authenticate];
    
    id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
    
    // Set any prefilled text that you might want to suggest
    
    [shareBuilder setURLToShare:[NSURL URLWithString:[_mutDictProductDetail valueForKey:@"product_url"]]];
    [shareBuilder setPrefillText:@"What do you think? Should I buy this?"];
    
    //[shareBuilder attachImage:imgViewProducts.image];
    [shareBuilder open];
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	if (result == MFMailComposeResultSent)
    {
		NSLog(@"Mail Send");
	}
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - GPPSignInDelegate

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error
{
    if (error)
    {
        return;
    }
    [self reportAuthStatus];
}


#pragma mark - GPPShareDelegate

- (void)finishedSharing:(BOOL)shared {
    NSString *text = shared ? @"Success" : @"Canceled";
    //shareStatus_.text = [NSString stringWithFormat:@"Status: %@", text];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:text delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)reportAuthStatus
{
    if ([GPPSignIn sharedInstance].authentication)
    {
        // signInAuthStatus_.text = @"Status: Authenticated";
        //[self retrieveUserInfo];
        //[self enableSignInSettings:NO];
    }
    else
    {
        // To authenticate, use Google+ sign-in button.
        // signInAuthStatus_.text = @"Status: Not authenticated";
        //[self enableSignInSettings:YES];
    }
}

- (IBAction) btnMoreInfoAction:(id)sender
{
    viewMoreInfo.hidden = NO;
    txtViewMoreInfo.text = [_mutDictProductDetail objectForKey:@"prd_longdesc"];
    [self.view bringSubviewToFront:viewMoreInfo];
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = .50f;
    transition.type =  @"moveIn";
    transition.subtype = @"fromTop";
    [viewMoreInfo.layer removeAllAnimations];
    [self.view addSubview:viewMoreInfo];
    [viewMoreInfo.layer addAnimation:transition forKey:kCATransition];
}

- (IBAction) btnCloseMoreInfoAction:(id)sender
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = .50f;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromBottom;
    //[viewMoreInfo.layer removeAllAnimations];
    viewMoreInfo.hidden = YES;
    [viewMoreInfo.layer addAnimation:transition forKey:kCATransition];
    
}

@end
