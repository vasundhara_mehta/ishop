//
//  DashboardDetail.m
//  IShop
//
//  Created by Admin on 16/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "DashboardDetail.h"
#import "iShopServices.h"
#import "JSON.h"
#import "DashboardDetailCell.h"
@interface DashboardDetail ()

@end

@implementation DashboardDetail
@synthesize orderId,statusStr,window,progressHud;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.window=[UIApplication sharedApplication].keyWindow;
    self.navigationController.navigationBarHidden=YES;
    self.progressHud=[[MBProgressHUD alloc]init];
    self.progressHud.delegate=self;
    self.progressHud.labelText = @"Please wait...";
    [self.window addSubview:self.progressHud];
    [self.progressHud show:YES];

    [self getDashboardDetails];
    shippingAdd.userInteractionEnabled=NO;
    billingAdd.userInteractionEnabled=NO;
    PaymentMethod.userInteractionEnabled=NO;
    scroll.contentSize=CGSizeMake(320, 1500);
    headerLbl.text=[NSString stringWithFormat:@"Order #%@ - %@",self.orderId,self.statusStr];
    itemArray=[[NSMutableArray alloc] init];
    titleArray=[[NSMutableArray alloc] initWithObjects:@"Product Name",@"SKU",@"Price",@"Qty",@"Subtotal",@"Shipping & Handling",@"Discount",@"Grand Total", nil];
    paymentDetail=[[NSMutableArray alloc] init];
    // Do any additional setup after loading the view from its nib.
}
-(void)getDashboardDetails{
    NSDictionary *parameters = @{@"order_id":self.orderId};
    
    [iShopServices PostMethodWithApiMethod:@"GetCustomerOrdersByOrderId" Withparms:parameters WithSuccess:^(id response)
     {
         NSArray *responseArray=[response JSONValue];
         itemArray=[responseArray valueForKey:@"Item Order"];
         //NSLog(@"Response = %@",[response JSONValue]);
         //for Shipping add
         NSArray *shiparray=[[[[[responseArray valueForKeyPath:@"Address"] valueForKey:@"shipping address"] objectAtIndex:0] objectAtIndex:0] objectAtIndex:0];
         
         sh_namelbl.text=[NSString stringWithFormat:@"%@ %@",[shiparray valueForKey:@"firstname"],[shiparray valueForKey:@"lastname"]];
         sh_addLbl.text=[NSString stringWithFormat:@"%@",[shiparray valueForKey:@"street"]];
         sh_cityLbl.text=[NSString stringWithFormat:@"%@,%@,%@",[shiparray valueForKey:@"city"],[shiparray valueForKey:@"region"],[shiparray valueForKey:@"postcode"]];
         sh_countryLbl.text=[NSString stringWithFormat:@"%@",[shiparray valueForKey:@"country"]];
         sh_telLbl.text=@"";
         
         //for billing add
         NSArray *billArr=[[[[[responseArray valueForKeyPath:@"Address"] valueForKey:@"billing address"] objectAtIndex:0] objectAtIndex:0] objectAtIndex:0];
         
         bi_namelbl.text=[NSString stringWithFormat:@"%@ %@",[billArr valueForKey:@"firstname"],[billArr valueForKey:@"lastname"]];
         bi_addLbl.text=[NSString stringWithFormat:@"%@",[billArr valueForKey:@"street"]];
         bi_cityLbl.text=[NSString stringWithFormat:@"%@,%@,%@",[billArr valueForKey:@"city"],[billArr valueForKey:@"region"],[billArr valueForKey:@"postcode"]];
         bi_countryLbl.text=[NSString stringWithFormat:@"%@",[billArr valueForKey:@"country"]];
         bi_telLbl.text=@"";

         // for Payment
         NSLog(@"Response = %@",[[[responseArray valueForKey:@"Payment"] objectAtIndex:0] objectAtIndex:0]);
         paymentDetail=[[[responseArray valueForKey:@"Payment"] objectAtIndex:0] objectAtIndex:0];
         
//         shippingAdd.text=[NSString stringWithFormat:@"%@",[[[[responseArray valueForKey:@"Address"] valueForKey:@"shipping address"] objectAtIndex:0] objectAtIndex:0]];
         [tab reloadData];
         [self.progressHud hide:YES];
     } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
     }];
}

#pragma mark UITableViewDatasource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0)
    {
        NSString *str=[NSString stringWithFormat:@"%@",[[[itemArray objectAtIndex:0] valueForKey:@"prod_name"] objectAtIndex:indexPath.section]];
        
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        
        NSDictionary * attributes = @{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16],
                                      NSParagraphStyleAttributeName : paragraphStyle};
        
        CGSize size = [str boundingRectWithSize:(CGSize){133 - (10 * 2), 20000.0f}
                                        options:NSStringDrawingUsesFontLeading
                       |NSStringDrawingUsesLineFragmentOrigin
                                     attributes:attributes
                                        context:nil].size;
        
        CGFloat height = MAX(size.height, 44.0);
        return height+20;
    }
    else
        return 44;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return itemArray.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return titleArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId=@"cellID";
    DashboardDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell==nil) {
        NSArray *nibArray=[[NSBundle mainBundle] loadNibNamed:@"DashboardDetailCell" owner:self options:nil];
        cell=[nibArray lastObject];
    }
    
    if (indexPath.row==0)
    {
        NSString *str=[NSString stringWithFormat:@"%@",[[[itemArray objectAtIndex:0] valueForKey:@"prod_name"] objectAtIndex:indexPath.section]];
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        
        NSDictionary * attributes = @{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16],
                                      NSParagraphStyleAttributeName : paragraphStyle};
        
        CGSize size = [str boundingRectWithSize:(CGSize){133 - (10 * 2), 20000.0f}
                                             options:NSStringDrawingUsesFontLeading
                        |NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attributes
                                             context:nil].size;
        
        CGFloat height = MAX(size.height, 44.0);
        cell.valueLbl.frame=CGRectMake(178, 10, 133, height);
    }
    
    NSString *str=[NSString stringWithFormat:@"%@",[[[itemArray objectAtIndex:0] valueForKey:@"prod_subtotal"] objectAtIndex:indexPath.section]];
    float val=[str floatValue];
    NSString *disStr=[NSString stringWithFormat:@"%@",[paymentDetail valueForKey:@"Discount"]];
    float disVal=[disStr floatValue];
    NSString *totalStr=[NSString stringWithFormat:@"%@",[paymentDetail valueForKey:@"Grand Total"]];
    float totalVal=[totalStr floatValue];
    NSString *shipStr=[NSString stringWithFormat:@"%@",[paymentDetail valueForKey:@"Shipping & Handling"]];
    float shipVal=[shipStr floatValue];
    
    cell.titleLbl.text=[NSString stringWithFormat:@"%@",[titleArray objectAtIndex:indexPath.row]];
    switch (indexPath.row) {
        case 0:
            cell.valueLbl.text=[NSString stringWithFormat:@"%@",[[[itemArray objectAtIndex:0] valueForKey:@"prod_name"] objectAtIndex:indexPath.section]];
            break;
        case 1:
            cell.valueLbl.text=[NSString stringWithFormat:@"%@",[[[itemArray objectAtIndex:0] valueForKey:@"prod_sku"] objectAtIndex:indexPath.section]];
            break;
        case 2:
            cell.valueLbl.text=[NSString stringWithFormat:@"$%@",[[[itemArray objectAtIndex:0] valueForKey:@"prod_price"] objectAtIndex:indexPath.section]];
            break;
        case 3:
            cell.valueLbl.text=[NSString stringWithFormat:@"%@",[[[itemArray objectAtIndex:0] valueForKey:@"prod_qty"] objectAtIndex:indexPath.section]];
            break;
        case 4:
            cell.valueLbl.text=[NSString stringWithFormat:@"$%.2f",val];
            break;
        case 5:
            cell.valueLbl.text=[NSString stringWithFormat:@"$%.2f",shipVal];
            break;
        case 6:
            cell.valueLbl.text=[NSString stringWithFormat:@"$%.2f",disVal];
            break;
        case 7:
            cell.valueLbl.text=[NSString stringWithFormat:@"$%.2f",totalVal];
        default:
            break;
    }
    //cell.detailTextLabel.text=@"hello";
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)backAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
