//
//  DashboardView.m
//  IShop
//
//  Created by Admin on 15/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "DashboardView.h"
#import "DashboardCell.h"
#import "iShopServices.h"
#import "JSON.h"
#import "DashboardDetail.h"
#import "UserProfileViewController.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface DashboardView ()

@end

@implementation DashboardView
@synthesize progressHud,window;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dashArray=[[NSMutableArray alloc] init];
    self.window=[UIApplication sharedApplication].keyWindow;
    self.navigationController.navigationBarHidden=YES;
    self.progressHud=[[MBProgressHUD alloc]init];
    self.progressHud.delegate=self;
    self.progressHud.labelText = @"Please wait...";
    [self.window addSubview:self.progressHud];
    [self.progressHud show:YES];

    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    txtFName.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"];
    txtLName.text =[[NSUserDefaults standardUserDefaults] valueForKey:@"lastname"];
    txtEmail.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"email"];
    txtPassword.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"passwordSave"];
    txtCnfmPassword.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"passwordSave"];
    txtEmail.userInteractionEnabled = NO;

    myAcc_view.hidden=YES;
    [self getDashboardDetails];
}
-(void)getDashboardDetails
{
    NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]};

    [iShopServices PostMethodWithApiMethod:@"getOrderList" Withparms:parameters WithSuccess:^(id response)
     {
          NSLog(@"Response = %@",[response JSONValue]);
         dashArray=[response JSONValue];
         [tab reloadData];
         [self.progressHud hide:YES];
     } failure:^(NSError *error)
     {
         [self.progressHud hide:YES];
         NSLog(@"Error =%@",[error description]);
     }];

}
#pragma mar - UITableViewDatasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dashArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellid =@"CellId";
    DashboardCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        NSArray *nibArr=[[NSBundle mainBundle] loadNibNamed:@"DashboardCell" owner:self options:nil];
        cell=[nibArr lastObject];
    }
  //  cell.shipto.text=[NSString stringWithFormat:@"%@",[[dashArray valueForKey:@"Ship To"] objectAtIndex:indexPath.row]];
    cell.orderNum.text=[NSString stringWithFormat:@"#%@",[[dashArray valueForKey:@"Order"] objectAtIndex:indexPath.row]];
    //cell.date.text=[NSString stringWithFormat:@"%@",[[dashArray valueForKey:@"Date"] objectAtIndex:indexPath.row]];
   //[[dashArray valueForKey:@"Order Total"] objectAtIndex:indexPath.row]];
    //float val=[str floatValue];
    //cell.orderTotal.text=[NSString stringWithFormat:@"$%.2f",val];
    //cell.statuslbl.text=[NSString stringWithFormat:@"%@",[[dashArray valueForKey:@"Status"] objectAtIndex:indexPath.row]];

    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
#pragma mark - UITablViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DashboardDetail *detail=[[DashboardDetail alloc] initWithNibName:@"DashboardDetail" bundle:nil];
    detail.orderId=[NSString stringWithFormat:@"%@",[[dashArray valueForKey:@"Order"] objectAtIndex:indexPath.row]];
    detail.statusStr=[NSString stringWithFormat:@"%@",[[dashArray valueForKey:@"Status"] objectAtIndex:indexPath.row]];
    [self presentViewController:detail animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBActions
-(IBAction)backAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)segmentAction:(id)sender{
    if (seg.selectedSegmentIndex==0) {
        myAcc_view.hidden=YES;
    }else{
        myAcc_view.hidden=NO;
    }
}
- (IBAction)btnNextStepAction:(id)sender
{
    NSString *password = txtPassword.text;
    NSString *confirmPassword = txtCnfmPassword.text;
    
    if([txtFName.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter first name." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtLName.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter last name." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if ([txtEmail.text length] > 0 )
    {
        if (![self emailValidation:txtEmail.text])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please enter Correct email." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please enter e-mail." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if([txtPassword.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtCnfmPassword.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter confirm password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if([password isEqualToString:confirmPassword])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Please Wait ...";
        hud.dimBackground = YES;
        
        NSURL *url = [NSURL URLWithString:@"http://inspirefnb.com/fashionworld/iosapi.php?methodName=UpdatCustomer"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue:txtEmail.text forKey:@"email"];
        [request setPostValue:txtPassword.text forKey:@"password"];
        [request setPostValue:txtFName.text forKey:@"fname"];
        [request setPostValue:txtLName.text forKey:@"lname"];
        [request setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"customer_id"] forKey:@"CusloginId"];
        [request setDidFinishSelector:@selector(requestFinishedForService:)];
        [request setDidFailSelector:@selector(requestFailed:)];
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Password Do not match." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
}

#pragma mark - ASIHTTP Response

-(void)requestFinishedForService:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSString *string =[request responseString];
    NSData *data = [request responseData];
    
    NSLog(@"string -- %@",string);
    NSLog(@"data -- %@",data);
    
    NSMutableDictionary *mutableDictResponce = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    NSString *strAlert = [mutableDictResponce valueForKey:@"error"];
    
    if ([[mutableDictResponce objectForKey:@"error"] isEqualToString:@""] || [mutableDictResponce objectForKey:@"error"] == nil)
    {
        objAppDelegate.dictUserInfo = [mutableDictResponce copy];
        
        if ([[mutableDictResponce objectForKey:@"message"] isEqualToString:@"Invalid login or password."])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Invalid Login Id or password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            objAppDelegate.isUserLogin = YES;
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:txtEmail.text forKey:@"emailSave"];
            [userDefaults setValue:txtPassword.text forKey:@"passwordSave"];
            [userDefaults setValue:txtFName.text forKey:@"firstname"];
            [userDefaults setValue:txtLName.text forKey:@"lastname"];
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRemember"];
            
            //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:[mutableDictResponce objectForKey:@"success"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Profile updated succesfully!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if(![[mutableDictResponce objectForKey:@"error"] isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:strAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Check your network connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Uitextfield delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(!IS_IPHONE5)
    {
        if(textField == txtPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -120, 320 , 460)];
            [UIView commitAnimations];
        }
        else if(textField == txtCnfmPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -150, 320 , 460)];
            [UIView commitAnimations];
        }
    }
    else
    {
        if(textField == txtPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -120, 320 , 548)];
            [UIView commitAnimations];
        }
        else if(textField == txtCnfmPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -150, 320 , 548)];
            [UIView commitAnimations];
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(!IS_IPHONE5)
    {
        if(textField == txtPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 480)];
            [UIView commitAnimations];
        }
        else if(textField == txtCnfmPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 480)];
            [UIView commitAnimations];
        }
    }
    else
    {
        if(textField == txtPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 568)];
            [UIView commitAnimations];
        }
        else if(textField == txtCnfmPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 568)];
            [UIView commitAnimations];
        }
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - validations

-(BOOL)emailValidation:(NSString *)string
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:string];
}

@end
