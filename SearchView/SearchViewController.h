//
//  SearchViewController.h
//  IShop
//
//  Created by Hashim on 4/30/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    NSMutableData *nsDataResult;
    IBOutlet UISearchBar *searchBarResult;
    
}

@property (nonatomic,retain)  NSMutableArray *arrContaintList;
@property (nonatomic,retain) IBOutlet UITableView *tblViewSearchResult;
@property (nonatomic,retain)  NSArray *searchResults;
@property (nonatomic, retain) NSString *strSearchResult, *strView;

-(IBAction)backButtonPress:(id)sender;

@end
