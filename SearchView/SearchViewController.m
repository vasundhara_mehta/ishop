//
//  SearchViewController.m
//  IShop
//
//  Created by Hashim on 4/30/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "SearchViewController.h"
#import "MBProgressHUD.h"
#import "ListGridViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"

@interface SearchViewController ()

@end

@implementation SearchViewController
@synthesize arrContaintList,tblViewSearchResult,strSearchResult, strView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-ActionMethod

-(IBAction)backButtonPress:(id)sender
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrContaintList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    cell.textLabel.text = [[self.arrContaintList objectAtIndex:indexPath.row] objectForKey:@"CategoryName"];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    
    return cell;
}

#pragma mark - UITableView Delegates

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([strView isEqualToString:@"1"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Refined"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Name"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Desc"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShortDesc"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Sku"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MinPrice"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MaxPrice"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        ListGridViewController *listView = [[ListGridViewController alloc] initWithNibName:@"ListGridViewController" bundle:nil];
        listView.categoryId = [[self.arrContaintList valueForKey:@"CategoryId"] objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:listView animated:YES];
    }
    else
    {
        AppDelegate *objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        objAppDelegate.selectedCateId = [[self.arrContaintList valueForKey:@"CategoryId"] objectAtIndex:indexPath.row];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isListGrid"];
        //[self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
} 

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  30;
}

#pragma mark - Custom Methods

- (void) getSearchResult
{
    
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.labelText = @"Please wait...";
//    hud.dimBackground = YES;
    
    NSMutableString *path = [NSMutableString stringWithFormat:@"%@", [NSString stringWithFormat:search_result, self.strSearchResult]];

    NSURL *onwURL = [[NSURL alloc] initWithString:path];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:onwURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0];
    [request addValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if( theConnection )
    {
        nsDataResult = [NSMutableData data];
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
}

#pragma mark UISearchbar delegates

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    AppDelegate *objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    objAppDelegate.isCheckSearchType = YES;
    objAppDelegate.strSearchName = searchBar.text;
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isListGrid"];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    NSLog(@"Testing");
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Refined"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Desc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ShortDesc"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Sku"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MinPrice"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MaxPrice"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    ListGridViewController *listView = [[ListGridViewController alloc] initWithNibName:@"ListGridViewController" bundle:nil];
    [self.navigationController pushViewController:listView animated:YES];

    
    NSLog(@"Testing111");
    return YES;
}


- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    self.strSearchResult = searchText;
    
    if ([self.strSearchResult length] == 0)
    {
        self.tblViewSearchResult.hidden = YES;
    }
    else
    {
        self.tblViewSearchResult.hidden = NO;
    }
    
    [self getSearchResult];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
	[searchBar resignFirstResponder];
	[searchBar setShowsCancelButton:YES animated:YES];
}

#pragma mark - NSURLConnection Delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [nsDataResult setLength: 0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [nsDataResult appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConenction");
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"DONE. Received Bytes: %lu", (unsigned long)[nsDataResult length]);
    self.arrContaintList = [NSJSONSerialization JSONObjectWithData:nsDataResult options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"Array -- %@",self.arrContaintList);
    
    [self.tblViewSearchResult reloadData];
  }

@end
