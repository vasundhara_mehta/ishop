//
//  FavouriteViewController.m
//  IShop
//
//  Created by Avnish Sharma on 5/26/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "FavouriteViewController.h"
#import "FavouriteCell.h"
#import "UIImageView+WebCache.h"
#import "BillingAddressViewController.h"
#import "ASIFormDataRequest.h"
#import "MBProgressHUD.h"
#import "SignInViewController.h"
#import "ViewFullSizeImageVC.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <Social/Social.h>

#import "JSON.h"
#import "CustomCell.h"
#import "iShopServices.h"
#import "Constants.h"
#import "ProductDetailView.h"

#define kNotificationDeleteFavoriteProduct @"DeleteFavoriteProduct"
#define kNotificationEditFavoriteProduct @"EditFavoriteProduct"
#define kNotificationMoveToBagProduct @"MoveToBagProduct"


@interface FavouriteViewController ()<GPPSignInDelegate>

@end

@implementation FavouriteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tblViewProductBag.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tblViewProductBag.bounds.size.width, 0.01f)];
    
    isEditProduct = NO;
    
    _mDictEditDetail = [[NSMutableDictionary alloc]init];
    [_mDictEditDetail setValue:@"" forKey:@"pro_size"];
    [_mDictEditDetail setValue:@"" forKey:@"pro_color"];
    
    selectedCellIndex = -1;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(editFavoriteProduct:) name:kNotificationEditFavoriteProduct object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deleteProduct:) name:kNotificationDeleteFavoriteProduct object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moveToBagProduct:) name:kNotificationMoveToBagProduct object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    selectedIndexViewMore = -1;

    NSLog(@"--Bool--%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"fav_array"]);
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    NSLog(@"Cart arrary -- %@",objAppDelegate.arrFavourite);
    
    if ([objAppDelegate.arrFavourite count] == 0)
    {
        objAppDelegate.arrFavourite = [[[NSUserDefaults standardUserDefaults] objectForKey:@"fav_array"] mutableCopy];
    }
    else
    {
        //[objAppDelegate.arrFavourite removeAllObjects];
        //objAppDelegate.arrFavourite = [[[NSUserDefaults standardUserDefaults] objectForKey:@"fav_array"] mutableCopy];
    }
    [self.tblViewProductBag setBackgroundColor:[UIColor clearColor]];
    
    lblNavTitle.text = [NSString stringWithFormat:@"YOUR SAVED ITEMS : %i ITEMS" ,[objAppDelegate.arrFavourite count]];
    [self setTotalValue];
}

-(void)moveToBagProduct:(NSNotification *)notification
{
    NSMutableDictionary *mDict = [objAppDelegate.arrFavourite objectAtIndex:btnTag];
    
    NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"product_id":[mDict valueForKeyPath:@"prd_id"],@"uniq_id":[mDict valueForKey:@"uniq_id"],@"get":@"0"};
    
    [iShopServices PostMethodWithApiMethod:@"GetUserFavoriteHistory" Withparms:parameters WithSuccess:^(id response) {
        // self.arrAddToBag = [response JSONValue];
        objAppDelegate.requestFor = DELETEFAVORITEPRODUCT;
        [objAppDelegate getFavoriteHistory];
        lblNavTitle.text = [NSString stringWithFormat:@"YOUR SAVED ITEMS : %i ITEMS" ,[objAppDelegate.arrFavourite count]];
        [self setTotalValue];
        [self.tableViewPrdAttributes reloadData];
    } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
     }];
}

-(void)editFavoriteProduct:(NSNotification *)notification
{
    [self setTotalValue];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if(isProductEdit)
    {
        isProductEdit = NO;
        _viewProductDetail.hidden = YES;
        [UIView transitionFromView:_viewProductDetail toView:self.tblViewProductBag duration:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft
                        completion: ^(BOOL inFinished) {
                            //do any post animation actions here
                        }];
    }
    
    [self.tblViewProductBag reloadData];
}

-(void)deleteProduct:(NSNotification *)notification
{
    //[[NSNotificationCenter defaultCenter]removeObserver:self name:kNotificationDeleteProduct object:nil];
    [self setTotalValue];
    [self.tblViewProductBag reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

#pragma mark-Action method

- (IBAction) btnBackAction:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)btnContinueAction
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Do you want to continue?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
}
-(void)DeleteAllProductsFromFav
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    for (int i=0; i<objAppDelegate.arrFavourite.count; i++)
    {
        NSMutableDictionary *mDict = [objAppDelegate.arrFavourite objectAtIndex:i];
        
        NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"product_id":[mDict valueForKeyPath:@"prd_id"],@"uniq_id":[mDict valueForKey:@"uniq_id"],@"get":@"0"};
        
        [iShopServices PostMethodWithApiMethod:@"GetUserFavoriteHistory" Withparms:parameters WithSuccess:^(id response) {
            // self.arrAddToBag = [response JSONValue];
            objAppDelegate.requestFor = DELETEFAVORITEPRODUCT;
            [objAppDelegate getFavoriteHistory];
            lblNavTitle.text = [NSString stringWithFormat:@"YOUR SAVED ITEMS : 0 ITEMS" ];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            [self.tableViewPrdAttributes reloadData];
            
        } failure:^(NSError *error)
         {
             NSLog(@"Error =%@",[error description]);
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         }];
    }
}

- (IBAction)btnDeleteAction:(id)sender
{
    NSLog(@"index -- %i",[sender tag]);
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    NSMutableDictionary *mDict = [objAppDelegate.arrFavourite objectAtIndex:[sender tag]];
    
    NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"product_id":[mDict valueForKeyPath:@"prd_id"],@"uniq_id":[mDict valueForKey:@"uniq_id"],@"get":@"0"};
    
    [iShopServices PostMethodWithApiMethod:@"GetUserFavoriteHistory" Withparms:parameters WithSuccess:^(id response) {
       // self.arrAddToBag = [response JSONValue];
        objAppDelegate.requestFor = DELETEFAVORITEPRODUCT;
        [objAppDelegate getFavoriteHistory];
        
    } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
     }];
}

- (IBAction) btnMoveAllToBagAction:(id)sender
{
    //NSLog(@"move all %d",objAppDelegate.arrFavourite.count);
    for (int i=0; i<objAppDelegate.arrFavourite.count; i++) {
        NSLog(@"post");
        NSMutableDictionary *mDictProduct = [objAppDelegate.arrFavourite objectAtIndex:i];
        NSString *product_id = [mDictProduct objectForKey:@"prd_id"];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Please wait...";
        hud.dimBackground = YES;
        //[objAppDelegate.arrFavourite addObject:[objAppDelegate.dataArray objectAtIndex:[sender tag]]];
        
        NSMutableArray *jsonArray = [[NSMutableArray alloc]init];
        
        for(NSMutableDictionary *mDict in _arrProductColorAndSize)
        {
            NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]init];
            [jsonDict setValue:[mDict valueForKey:@"prd_Attribute_Title"] forKey:@"option_value"];
            [jsonDict setValue:[mDict valueForKey:@"custome_title"] forKey:@"option_name"];
            
            [jsonDict setValue:[mDict valueForKey:@"prd_option_id"] forKey:@"option_id"];
            [jsonDict setValue:[mDict valueForKey:@"prd_option_type_id"] forKey:@"option_type_id"];
            [jsonArray addObject:jsonDict];
        }
        
        NSError *writeError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonArray options:NSJSONWritingPrettyPrinted error:&writeError];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //  NSLog(@"JSON Output: %@", jsonString);
        
        lblProductPrice.text = [lblProductPrice.text substringFromIndex:1];
        
        NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"prd_qty":[mDictProduct objectForKey:@"prd_qty"],@"product_id":product_id,@"option_json_value":jsonString,@"uniq_id":[mDictProduct valueForKey:@"uniq_id"],@"prd_default_price":[mDictProduct valueForKey:@"prd_price"],@"prd_update_price":lblProductPrice.text,@"get":@"1"};
        
        [iShopServices PostMethodWithApiMethod:@"GetUserCheckoutHistory" Withparms:parameters WithSuccess:^(id response)
         {
             // NSLog(@"Response = %@",[response JSONValue]);
             
             NSLog(@"jsonString = %@",jsonString);
             objAppDelegate.requestFor = MOVETOBAGPRODUCT;
             [objAppDelegate getCheckOutHistory];
             
             //self.arrAddToBag = [response JSONValue];
         } failure:^(NSError *error)
         {
             NSLog(@"Error =%@",[error description]);
         }];
    }
    [self DeleteAllProductsFromFav];
}

- (BOOL)matchFoundForEventId:(NSString *)eventId WithArray:(NSMutableArray *)ConditionArray
{
    index = 0;
    for (NSDictionary *dataDict in ConditionArray)
    {
        NSString *createId = [dataDict objectForKey:@"prd_id"];
        if ([eventId isEqualToString:createId])
        {
            return YES;
        }
        index++;
    }
    return NO;
}

#pragma mark - UIAlert Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
}

#pragma mark - Custom Methods

+ (UIImage *) resizeImage:(UIImage *)orginalImage resizeSize:(CGSize)size
{
    CGFloat actualHeight = orginalImage.size.height;
    CGFloat actualWidth = orginalImage.size.width;
    
    float oldRatio = actualWidth/actualHeight;
    float newRatio = size.width/size.height;
    if(oldRatio < newRatio)
    {
        oldRatio = size.height/actualHeight;
        actualWidth = oldRatio * actualWidth;
        actualHeight = size.height;
    }
    else
    {
        oldRatio = size.width/actualWidth;
        actualHeight = oldRatio * actualHeight;
        actualWidth = size.width;
    }
    
    CGRect rect = CGRectMake(0.0,0.0,actualWidth,actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [orginalImage drawInRect:rect];
    orginalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return orginalImage;
}

-(void) setTotalValue
{
    if([objAppDelegate.arrFavourite count] != 0)
    {
        NSMutableDictionary *mDictRecord = [[NSMutableDictionary alloc] init];
        float totalPriceQty = 0;
        int qtyTotal = 0;
        for (int i = 0; i<[objAppDelegate.arrFavourite count]; i++)
        {
            mDictRecord = [objAppDelegate.arrFavourite objectAtIndex:i];
            NSString *strQty = [mDictRecord valueForKey:@"prd_qty"];
            NSString *strPrice = [mDictRecord valueForKey:@"prd_price"];
            float price = [strPrice floatValue];
            int qty = [strQty intValue];
            
            qtyTotal = qtyTotal+qty;
            
            float prceQty = price*qty;
            totalPriceQty = totalPriceQty+prceQty;
        }
        
        NSLog(@"%f", totalPriceQty);
        totalPrice = totalPriceQty;
    }
}

-(IBAction)btnActionMoveToBag:(id)sender
{
    btnTag = [sender tag];
    
    NSMutableDictionary *mDictProduct = [objAppDelegate.arrFavourite objectAtIndex:[sender tag]];
    NSString *product_id = [mDictProduct objectForKey:@"prd_id"];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    //[objAppDelegate.arrFavourite addObject:[objAppDelegate.dataArray objectAtIndex:[sender tag]]];
    
    NSMutableArray *jsonArray = [[NSMutableArray alloc]init];
    
    for(NSMutableDictionary *mDict in _arrProductColorAndSize)
    {
        NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]init];
        [jsonDict setValue:[mDict valueForKey:@"prd_Attribute_Title"] forKey:@"option_value"];
        [jsonDict setValue:[mDict valueForKey:@"custome_title"] forKey:@"option_name"];
        
        [jsonDict setValue:[mDict valueForKey:@"prd_option_id"] forKey:@"option_id"];
        [jsonDict setValue:[mDict valueForKey:@"prd_option_type_id"] forKey:@"option_type_id"];
        [jsonArray addObject:jsonDict];
    }
    
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonArray options:NSJSONWritingPrettyPrinted error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //  NSLog(@"JSON Output: %@", jsonString);
    
    lblProductPrice.text = [lblProductPrice.text substringFromIndex:1];
    
    NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"prd_qty":[mDictProduct objectForKey:@"prd_qty"],@"product_id":product_id,@"option_json_value":jsonString,@"uniq_id":[mDictProduct valueForKey:@"uniq_id"],@"prd_default_price":[mDictProduct valueForKey:@"prd_price"],@"prd_update_price":lblProductPrice.text,@"get":@"1"};
    
    [iShopServices PostMethodWithApiMethod:@"GetUserCheckoutHistory" Withparms:parameters WithSuccess:^(id response)
     {
         // NSLog(@"Response = %@",[response JSONValue]);
         
         NSLog(@"jsonString = %@",jsonString);
         objAppDelegate.requestFor = MOVETOBAGPRODUCT;
         [objAppDelegate getCheckOutHistory];
         //self.arrAddToBag = [response JSONValue];
     } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
     }];
}

-(BOOL)matchFoundForProductId:(NSString *)productId
{
    index1 = 0;
    for (NSDictionary *dataDict in objAppDelegate.arrAddToBag)
    {
        NSString *createId = [dataDict objectForKey:@"prd_id"];
        if ([productId isEqualToString:createId])
        {
            return YES;
        }
        index1++;
    }
    return NO;
}

#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isProductEdit)
    {
        return [_arrProductColorAndSize count];
    }
    return [objAppDelegate.arrFavourite count];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(isProductEdit)
    {
        return nil;
    }
    else
    {
        UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        viewFooter.backgroundColor = [UIColor whiteColor];
        
        UIButton *btnPayment = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
        btnPayment.backgroundColor = [UIColor clearColor];
        [btnPayment setImage:[UIImage imageNamed:@"movetoallbag.png"] forState:UIControlStateNormal];
        [btnPayment setImage:[UIImage imageNamed:@"movetoallbag.png"] forState:UIControlStateHighlighted];
        [btnPayment addTarget:self action:@selector(btnMoveAllToBagAction:) forControlEvents:UIControlEventTouchUpInside];
        [viewFooter addSubview:btnPayment];
        NSLog(@"__FUCTION__");
        
        return viewFooter;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isProductEdit)
    {
        static NSString *cellIdentifier = @"Prd_Attributes";
        CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            NSArray *cellView = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
            cell = (CustomCell *)[cellView objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
        
        if([[[_arrProductColorAndSize objectAtIndex:indexPath.row]valueForKey:@"prd_Attribute_Title"] isEqualToString:@""])
        {
            cell.lblPrdAttibuteTitle.text = [[_arrProductColorAndSize objectAtIndex:indexPath.row]valueForKey:@"custome_title"];
        }
        else
        {
            cell.lblPrdAttibuteTitle.text = [[_arrProductColorAndSize objectAtIndex:indexPath.row]valueForKey:@"prd_Attribute_Title"];
        }
        
        [cell.btnPicker addTarget:self action:@selector(btnColorAndSizeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnPicker.tag = indexPath.row;
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier;
        CellIdentifier = @"FavouriteCell";
        FavouriteCell *cell = (FavouriteCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[FavouriteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FavouriteCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell.viewDetail.hidden = NO;
        
        NSMutableDictionary *dictCartProduct = [objAppDelegate.arrFavourite objectAtIndex:indexPath.row];
        
        NSString *str=[dictCartProduct objectForKey:@"prd_name"];
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        
        NSDictionary * attributes = @{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16],
                                      NSParagraphStyleAttributeName : paragraphStyle};
        
        CGSize size = [str boundingRectWithSize:(CGSize){138 - (10 * 2), 20000.0f}
                                        options:NSStringDrawingUsesFontLeading
                       |NSStringDrawingUsesLineFragmentOrigin
                                     attributes:attributes
                                        context:nil].size;
        
        CGFloat height = MAX(size.height, 10.0);
        NSLog(@"height %f",size.height);
        cell.lblProductName.frame=CGRectMake(141, 0, 138, height+20);
        cell.lblProductName.numberOfLines=0;
        cell.lblProductName.lineBreakMode=NSLineBreakByWordWrapping;
        cell.lblProductName.text = [dictCartProduct objectForKey:@"prd_name"];
        cell.lblProductName.lineBreakMode = NSLineBreakByWordWrapping;
        cell.lblProductName.numberOfLines = 0;
        
        cell.lblQty.text = [dictCartProduct objectForKey:@"prd_qty"];
        
        cell.lblPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,[[dictCartProduct objectForKey:@"prd_price"] floatValue]];
        
        [cell.imgViewProduct setImageWithURL:[NSURL URLWithString:[dictCartProduct valueForKey:@"prd_thumb"]] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"]];
        
        cell.btnMoveToBag.tag = indexPath.row;
        [cell.btnMoveToBag addTarget:self action:@selector(btnActionMoveToBag:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btn_Delete.tag = indexPath.row;
        [cell.btn_Delete addTarget:self action:@selector(btnDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btn_Edit.enabled = YES;
        
        NSString *strProductOption  =[NSString stringWithFormat:@"%@", [dictCartProduct valueForKey:@"product_options"]] ;
        
        if([strProductOption isEqualToString:@"<null>"])
        {
            cell.btn_Edit.enabled = NO;
        }
        
        cell.btn_Edit.tag = indexPath.row ;
        [cell.btn_Edit addTarget:self action:@selector(btnEditAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnEditQty.tag = indexPath.row;
        [cell.btnEditQty addTarget:self action:@selector(btnEditQtyAction:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *jsonString = [dictCartProduct valueForKey:@"option_json_value"];
        
        NSMutableArray *optionArray = [jsonString JSONValue];
        
        float yPosition = 28+height-20;
        
        cell.lblViewMore.text = @"View More";
        
        for (int i = 0; i < [optionArray count]; i++)
        {
            if(selectedIndexViewMore == indexPath.row)
            {
                cell.lblViewMore.text = @"View Less";
                
                UILabel *lblMessage = [[UILabel alloc]initWithFrame:CGRectMake(141, yPosition, 160, 18)];
                NSString *strText = [[optionArray objectAtIndex:i]valueForKey:@"option_name"];
                strText = [strText stringByAppendingString:@": "];
                
                id obj = [[optionArray objectAtIndex:i]valueForKey:@"option_value"];
                if (obj != nil)
                {
                    strText = [strText stringByAppendingString:[[optionArray objectAtIndex:i]valueForKey:@"option_value"]];
//                    NSString *str=strText;
//                    CGSize constraint = CGSizeMake(160 - (10 * 2), 20000.0f);
//                    CGSize size = [str sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
//                    CGFloat height = MAX(size.height, 5.0);
//                    lblMessage.numberOfLines=0;
//                    lblMessage.lineBreakMode=NSLineBreakByWordWrapping;
                    lblMessage.text = strText;
                    yPosition = yPosition + 16;
                    [lblMessage setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
                   
                    [cell.contentView addSubview:lblMessage];
                }
            }
            else if (i < 3)
            {
                UILabel *lblMessage = [[UILabel alloc]initWithFrame:CGRectMake(141, yPosition, 160, 18)];
                NSString *strText = [[optionArray objectAtIndex:i]valueForKey:@"option_name"];
                strText = [strText stringByAppendingString:@": "];
                
                id obj = [[optionArray objectAtIndex:i]valueForKey:@"option_value"];
                if (obj != nil)
                {
                    strText = [strText stringByAppendingString:[[optionArray objectAtIndex:i]valueForKey:@"option_value"]];
                    lblMessage.text = strText;
                    yPosition = yPosition + 16;
                    [lblMessage setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
                    [cell.contentView addSubview:lblMessage];
                }
            }
            
        }
        NSLog(@"selectedindex val %d",selectedIndexViewMore);
        
        if(selectedIndexViewMore == indexPath.row){
            if (yPosition>32) {
                cell.btnEditQty.frame=CGRectMake(141, cell.bounds.size.height-70, 70, 23);
                cell.lblQty.frame=CGRectMake(163, cell.bounds.size.height-70, 25, 21);
                cell.btnViewMore.frame=CGRectMake(232, cell.bounds.size.height-70, 75, 23);
                cell.lblViewMore.frame=CGRectMake(239, cell.bounds.size.height-67, 61, 18);
//                cell.lblPrice.frame=CGRectMake(141, yPosition-120, 54, 21);
//                cell.btnMoveToBag.frame=CGRectMake(194, yPosition-120, 84, 25);
//                cell.btn_Delete.frame=CGRectMake(285, yPosition-120, 32, 22);
            }
        }
        
        cell.btnViewMore.hidden = NO;
        cell.lblViewMore.hidden = NO;
        
        if([optionArray count] < 3)
        {
            cell.btnViewMore.hidden = YES;
            cell.lblViewMore.hidden = YES;
        }
        cell.btnViewMore.tag = indexPath.row;
        [cell.btnViewMore addTarget:self action:@selector(actionViewMore:) forControlEvents:UIControlEventTouchUpInside];
        [cell.imgBtn addTarget:self action:@selector(detailAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.imgBtn.tag=indexPath.row;
        return cell;
    }
}
-(IBAction)detailAction:(id)sender{
    NSMutableDictionary *mDictProduct = [objAppDelegate.arrFavourite objectAtIndex:[sender tag]];
    NSLog(@"detail action %@",mDictProduct);
    ProductDetailView *objProductDetailView;
    if (IS_IPHONE5)
    {
        objProductDetailView = [[ProductDetailView alloc] initWithNibName:@"ProductDetailView" bundle:nil];
    }
    else
    {
        objProductDetailView = [[ProductDetailView alloc] initWithNibName:@"ProductDetailView_iphone3.5" bundle:nil];
    }
    
    objProductDetailView.mutDictProductDetail =mDictProduct;
    [self.navigationController pushViewController:objProductDetailView animated
                                                 :YES];

}
#pragma mark - UITableView Delegates

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dictCartProduct = [objAppDelegate.arrFavourite objectAtIndex:indexPath.row];
    NSString *str=[dictCartProduct objectForKey:@"prd_name"];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary * attributes = @{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    CGSize size = [str boundingRectWithSize:(CGSize){138 - (10 * 2), 20000.0f}
                                    options:NSStringDrawingUsesFontLeading
                   |NSStringDrawingUsesLineFragmentOrigin
                                 attributes:attributes
                                    context:nil].size;
    
    CGFloat height1 = MAX(size.height, 5);

    NSLog(@"selected index %d",selectedIndexViewMore);
    if(isProductEdit)
    {
        return 36;
    }
    if(selectedIndexViewMore != indexPath.row)
    {
        return  130+height1;
    }
    else
    {

        NSString *jsonString = [dictCartProduct valueForKey:@"option_json_value"];
        NSMutableArray *optionArray = [jsonString JSONValue];
        CGFloat ypos=28;
        for (int i=0; i<optionArray.count; i++) {
            ypos=ypos+16;
        }
        int cunter = [optionArray count];
        if(cunter < 3)
        {
            return 163;
        }
        return   ypos+height1+54;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(isProductEdit)
    {
        return 0;
    }
    return 50.0;
}

-(IBAction)btnEditQtyAction:(id)sender
{
    if(IS_IPHONE5)
    {
        self.pickerToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 362, 320, 44)];
        
        pickerViewQty = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 406, 320, 162)];
        pickerViewQty.delegate = self;
        pickerViewQty.showsSelectionIndicator = YES;
        pickerViewQty.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:pickerViewQty];
    }
    else
    {
        self.pickerToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 274, 320, 44)];
        
        pickerViewQty = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 318, 320, 162)];
        pickerViewQty.delegate = self;
        pickerViewQty.showsSelectionIndicator = YES;
        pickerViewQty.backgroundColor = [UIColor whiteColor];
        
        [self.view addSubview:pickerViewQty];
    }
    
    UIButton *done_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    done_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [done_Button setTitle:@"Done" forState:UIControlStateNormal];
    [done_Button addTarget:self action:@selector(btnDoneToolbarAction:) forControlEvents:UIControlEventTouchUpInside];
    
    done_Button.tag = [sender tag];
    
    UIButton *cancel_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    cancel_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel_Button addTarget:self action:@selector(btnCancelToolbarAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *myButton = [[UIBarButtonItem alloc] initWithCustomView:done_Button];
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithCustomView:cancel_Button];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [self.pickerToolbar1 insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Bar.png"]] atIndex:0];
    
    CALayer *l=self.pickerToolbar1.layer;
    [l setCornerRadius:0.5];
    [l setBorderColor:[[UIColor grayColor] CGColor]];
    [l setBorderWidth:0.5];
    [self.pickerToolbar1 setItems:[NSArray arrayWithObjects:btnCancel, flex, myButton, nil]];
    [self.view addSubview:self.pickerToolbar1];
    
    selectedItemIndex = 2;
    
    mArrayQuantity = [[NSMutableArray alloc] initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", nil];
    
    [pickerViewQty reloadAllComponents];
}

-(IBAction)actionViewMore:(id)sender
{
    if(selectedIndexViewMore == -1 || selectedIndexViewMore != [sender tag])
    {
        selectedIndexViewMore = [sender tag];
    }
    else
    {
        selectedIndexViewMore = -1;
    }
    
    [_tblViewProductBag reloadData];
}
-(IBAction)btnDoneToolbarAction:(id)sender
{
    [pickerViewQty removeFromSuperview];
    [self.pickerToolbar1 removeFromSuperview];
    
    NSMutableDictionary *mDictProduct = [objAppDelegate.arrFavourite objectAtIndex:[sender tag]];
    
    NSString *product_id = [mDictProduct objectForKey:@"prd_id"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    //int randomNumber = arc4random() % 999999;
    
    // NSLog(@"Random No = %d",randomNumber);
    
    
   
    lblProductPrice.text = [lblProductPrice.text substringFromIndex:1];
    
    NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"prd_qty":_strQty,@"product_id":product_id,@"uniq_id":[mDictProduct valueForKey:@"uniq_id"],@"get":@"1"};
    
    [iShopServices PostMethodWithApiMethod:@"GetUserFavoriteHistory" Withparms:parameters WithSuccess:^(id response)
     {
         // NSLog(@"Response = %@",[response JSONValue]);
         objAppDelegate.requestFor = EDITFAVORITEPRODUCT;
         [objAppDelegate getFavoriteHistory];
         //self.arrAddToBag = [response JSONValue];
     } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
     }];
}

- (IBAction)btnCancelToolbarAction:(id)sender
{
    [pickerViewQty removeFromSuperview];
    [self.pickerToolbar1 removeFromSuperview];
}

- (IBAction)btnColorAndSizeAction:(id)sender
{
    if(IS_IPHONE5)
    {
        self.pickerToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 362, 320, 44)];
    }
    else
    {
        self.pickerToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 274, 320, 44)];
    }
    
    UIButton *done_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    done_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [done_Button setTitle:@"Done" forState:UIControlStateNormal];
    [done_Button addTarget:self action:@selector(doneBtnPressToGetValue:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *cancel_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    cancel_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel_Button addTarget:self action:@selector(CancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *myButton = [[UIBarButtonItem alloc] initWithCustomView:done_Button];
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithCustomView:cancel_Button];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [self.pickerToolbar1 insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Bar.png"]] atIndex:0];
    
    CALayer *l=self.pickerToolbar1.layer;
    [l setCornerRadius:0.5];
    [l setBorderColor:[[UIColor grayColor] CGColor]];
    [l setBorderWidth:0.5];
    [self.pickerToolbar1 setItems:[NSArray arrayWithObjects:btnCancel, flex, myButton, nil]];
    [self.viewProductDetail addSubview:self.pickerToolbar1];
    
    isSelectedPicker  = [sender tag];
    
    _mArrayProductAttributes = [[_arrProductColorAndSize objectAtIndex:isSelectedPicker] objectForKey:@"custome_values"];
    
    pickerViewColor.hidden = NO;
    
    self.tableViewPrdAttributes.userInteractionEnabled = NO;
    
    [pickerViewColor reloadAllComponents];
}

-(IBAction)btnEditAction:(id)sender
{
    _mutDictProductDetail = [objAppDelegate.arrFavourite objectAtIndex:[sender tag]];
    
    _viewProductDetail.hidden = NO;
    if(IS_IPHONE5)
    {
        _viewProductDetail.frame = CGRectMake(_viewProductDetail.frame.origin.x, _viewProductDetail.frame.origin.y, 320, 568);
    }
    else
    {
        _viewProductDetail.frame = CGRectMake(_viewProductDetail.frame.origin.x, _viewProductDetail.frame.origin.y, 320, 480);
    }
    
    [UIView transitionFromView:self.tblViewProductBag toView:_viewProductDetail duration:1.0 options:UIViewAnimationOptionTransitionFlipFromRight
                    completion: ^(BOOL inFinished) {
                        //do any post animation actions here
                        NSLog(@"test");
                        isProductEdit = YES;
                        [self setContentView];
                    }];
    
}



-(IBAction)btnEditColorAction:(id)sender
{
    selectedItemIndex = 3;
    
    [self showPickerView];
}

-(void)showPickerView
{
    if(IS_IPHONE5)
    {
        _keyboardToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 308, 320, 44)];
    }
    else
    {
        _keyboardToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 220, 320, 44)];
    }
    
    UIButton *done_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    done_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [done_Button setTitle:@"Done" forState:UIControlStateNormal];
    [done_Button addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *cancel_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    cancel_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel_Button addTarget:self action:@selector(CancelQtyButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *myButton = [[UIBarButtonItem alloc] initWithCustomView:done_Button];
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithCustomView:cancel_Button];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [_keyboardToolbar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Bar.png"]] atIndex:0];
    
    CALayer *l=_keyboardToolbar.layer;
    [l setCornerRadius:0.5];
    [l setBorderColor:[[UIColor grayColor] CGColor]];
    [l setBorderWidth:0.5];
    [_keyboardToolbar setItems:[NSArray arrayWithObjects:btnCancel, flex, myButton, nil]];
    [self.view addSubview:_keyboardToolbar];
    
    [pickerViewQty setHidden:NO];
    [self.view bringSubviewToFront:pickerViewQty];
    [pickerViewQty reloadAllComponents];
}


-(IBAction)CancelQtyButtonPressed:(id)sender
{
    [pickerViewQty setHidden:YES];
    [_keyboardToolbar removeFromSuperview];
}

-(IBAction)doneButtonPressed:(id)sender
{
    [pickerViewColor setHidden:YES];
    [_keyboardToolbar removeFromSuperview];
    
    
    if(selectedItemIndex == 1)
    {
        //Size
    }
    else if(selectedItemIndex == 2)
    {
        //quantity
    }
    else if(selectedItemIndex == 3)
    {
        //Color
    }
    
}

- (IBAction)btnEditCancelAction:(id)sender
{
    selectedCellIndex = -1 ;
    isEditProduct = NO;
    pickerViewColor.hidden = YES;
    _keyboardToolbar.hidden = YES;
    
    [self.tblViewProductBag reloadData];
}
-(IBAction)btnEditSizeAction:(id)sender
{
    // int tag = [sender tag];
    
    selectedItemIndex = 1;
    
    [self showPickerView];
}

- (IBAction)btnEditApplyAction:(id)sender
{
    pickerViewColor.hidden = YES;
    _keyboardToolbar.hidden = YES;
    
    NSString *product_id = [_mutDictProductDetail objectForKey:@"prd_id"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    iRequest = 3;
    
    NSMutableArray *jsonArray = [[NSMutableArray alloc]init];
    
    for(NSMutableDictionary *mDict in _arrProductColorAndSize)
    {
        NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]init];
        [jsonDict setValue:[mDict valueForKey:@"prd_Attribute_Title"] forKey:@"option_value"];
        [jsonDict setValue:[mDict valueForKey:@"custome_title"] forKey:@"option_name"];
        [jsonArray addObject:jsonDict];
    }
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonArray options:NSJSONWritingPrettyPrinted error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //  NSLog(@"JSON Output: %@", jsonString);
    
    int randomNumber = arc4random() % 999999;
    
    NSLog(@"Random No = %d",randomNumber);
    
    lblProductPrice.text = [lblProductPrice.text substringFromIndex:1];
    
    NSDictionary *parameters = @{@"customer_id": [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"],@"prd_qty":[_mutDictProductDetail objectForKey:@"prd_qty"],@"product_id":product_id,@"option_json_value":jsonString,@"uniq_id":[_mutDictProductDetail valueForKey:@"uniq_id"],@"prd_default_price":[_mutDictProductDetail valueForKey:@"prd_price"],@"prd_update_price":lblProductPrice.text,@"get":@"1"};
    
    [iShopServices PostMethodWithApiMethod:@"GetUserFavoriteHistory" Withparms:parameters WithSuccess:^(id response)
     {
         // NSLog(@"Response = %@",[response JSONValue]);
         objAppDelegate.requestFor = EDITFAVORITEPRODUCT;
         [objAppDelegate getFavoriteHistory];
         //self.arrAddToBag = [response JSONValue];
     } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
     }];
    
}

#pragma mark - ASIHTTP Response

-(void)requestFinishedForService:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSString *string =[request responseString];
    NSData *data = [request responseData];
    
    NSLog(@"string -- %@",string);
    NSLog(@"data -- %@",data);
    
    [objAppDelegate getFavoriteHistory];
    selectedCellIndex = -1;
    isEditProduct = NO;
    [self.tblViewProductBag reloadData];
    
    [self performSelector:@selector(reloadAfterDelay) withObject:nil afterDelay:1.0];
    
}

-(void)reloadAfterDelay
{
    [self.tblViewProductBag reloadData];
}


-(void)requestFailed:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Check your network connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}


#pragma mark - UIPickerView Delegate methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView.tag==1000){
        return [_mArrayProductAttributes count];

    }else{
    if(selectedItemIndex == 1)
    {
        return [_arrProductSize count];
    }
    else if(selectedItemIndex == 2)
    {
        return [mArrayQuantity count];
    }
    
    if(selectedItemIndex == 3)
    {
        return [_arrProductColor count];
    }
    else
    {
        return [_mArrayProductAttributes count];
    }
    }
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30.0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, pickerViewColor.frame.size.width, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:16];
    
    NSString *strValue;
    if (pickerView.tag==1000) {
        strValue = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];

    }else{
    if(selectedItemIndex == 1)
    {
        strValue = [[_arrProductSize objectAtIndex:row] objectForKey:@"title"];
    }
    else if(selectedItemIndex == 2)
    {
        strValue = [mArrayQuantity objectAtIndex:row];
    }
    else if(selectedItemIndex == 3)
    {
        strValue = [[_arrProductColor objectAtIndex:row]objectForKey:@"title"];
    }
    else
    {
        strValue = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];
    }
    }
    label.text = [NSString stringWithFormat:@"%@", strValue];
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag==1000) {
        strAttributeTitile = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];

    }else{
    if(selectedItemIndex == 1)
    {
        [_mDictEditDetail setValue:[[_arrProductSize objectAtIndex:row] objectForKey:@"title"] forKey:@"pro_size"];
    }
    else if(selectedItemIndex == 2)
    {
        _strQty = [mArrayQuantity objectAtIndex:row];
    }
    
    if(selectedItemIndex == 3)
    {
        [_mDictEditDetail setValue:[[_arrProductColor objectAtIndex:row] objectForKey:@"title"] forKey:@"pro_color"];
    }
    }
    strAttributeTitile = [[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"title"];
    
    float Price = 0.0;
    
    Price = [strDefaultPrice floatValue] + [[[_mArrayProductAttributes objectAtIndex:row] objectForKey:@"price"] floatValue];
    
    lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,Price];
    
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	return 300;
}

-(void)setContentView
{
    viewMoreInfo.hidden = YES;
    isWebserviceCount = 1;
    
    viewMoreInfo.hidden = YES;
    //self.pickerViewColorandSize.hidden = YES;
    
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = @"Please wait...";
    //    hud.dimBackground = YES;
    
    NSMutableString *request_gallery = [NSMutableString stringWithFormat:@"%@", [NSString stringWithFormat:product_gallery, [_mutDictProductDetail objectForKey:@"prd_id"]]];
    
    NSMutableString *request_options = [NSMutableString stringWithFormat:@"%@", [NSString stringWithFormat:product_option, [_mutDictProductDetail objectForKey:@"prd_id"]]];
    
    [self getResponce:request_gallery :1];
    [self getResponce:request_options :2];
    
    NSLog(@"Dict -- %@",_mutDictProductDetail);
    
    if (IS_IPHONE5)
    {
        self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, 891);
    }
    else
    {
        self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, 891);
    }
    self.scrollViewProductDetail.contentOffset = CGPointMake (self.scrollViewProductDetail.bounds.origin.x, 0);
    
    lblProductName.text = [_mutDictProductDetail objectForKey:@"prd_name"];
    lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,[[_mutDictProductDetail objectForKey:@"prd_price"] floatValue]];
    
    strDefaultPrice = [_mutDictProductDetail objectForKey:@"prd_price"];
    
    lblProductProductCode.text = [NSString stringWithFormat:@"Product Code %@",[_mutDictProductDetail objectForKey:@"prd_sku"]];
    lblProductProductDiscription.text = [_mutDictProductDetail objectForKey:@"prd_desc"];
}

- (void) getResponce:(NSString *)strURL :(int)service_count
{
    NSURL *url = [NSURL URLWithString:strURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60.0];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data,NSError *connectionError){
                               
                               if (service_count == 1)
                               {
                                   for (UIImageView *subImageView in self.scrolViewPrductImages.subviews)
                                   {
                                       [subImageView removeFromSuperview];
                                   }
                                   mutArrayProductImages = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                   NSLog(@"Responce-- %@",mutArrayProductImages);
                                   
                                   self.scrolViewPrductImages.contentSize = CGSizeMake(320 * [mutArrayProductImages count], self.scrolViewPrductImages.frame.size.height);
                                   self.scrolViewPrductImages.contentOffset = CGPointMake (self.scrolViewPrductImages.bounds.origin.x, self.scrollViewProductDetail.bounds.origin.y);
                                   _pagecontroller.numberOfPages = mutArrayProductImages.count;
                                   _pagecontroller.currentPage=0;
                                   
                                   for (int i = 0; i < [mutArrayProductImages count]; i++)
                                   {
                                       imgViewProducts = [[UIImageView alloc] initWithFrame:CGRectMake(i * 320, 0, 320,350)];
                                       imgViewProducts.backgroundColor = [UIColor clearColor];
                                       imgViewProducts.contentMode = UIViewContentModeScaleAspectFit;
                                       
                                       NSString *strURL = [[mutArrayProductImages objectAtIndex:i] objectForKey:@"prd_img"];
                                       
                                       __block UIActivityIndicatorView *activityIndicatorForImage = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                                       activityIndicatorForImage.center = imgViewProducts.center;
                                       activityIndicatorForImage.hidesWhenStopped = YES;
                                       
                                       // Here we use the new provided setImageWithURL: method to load the web image
                                       [imgViewProducts setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:[UIImage imageNamed:@"Place_holder.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType){
                                           if(!error ){
                                               [activityIndicatorForImage removeFromSuperview];
                                           }
                                       }];
                                       [imgViewProducts addSubview:activityIndicatorForImage];
                                       
                                       UITapGestureRecognizer *singleTap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(productGalleryFullView:)];
                                       imgViewProducts.tag=i;
                                       singleTap.view.tag=imgViewProducts.tag;
                                       imgViewProducts.userInteractionEnabled=YES;
                                       [imgViewProducts addGestureRecognizer:singleTap];
                                       
                                       [self.scrolViewPrductImages addSubview:imgViewProducts];
                                   }
                               }
                               else
                               {
                                   NSMutableArray *mArrayResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                   _arrProductColorAndSize = [[NSMutableArray alloc]init];
                                   for(NSMutableDictionary *mDict in mArrayResponse)
                                   {
                                       
                                       NSString *jsonString = [_mutDictProductDetail valueForKey:@"option_json_value"];
                                       
                                       BOOL isOption = NO;
                                       NSMutableArray *optionArray = [jsonString JSONValue];
                                       
                                       for (int i = 0; i < [optionArray count]; i++)
                                       {
                                           NSString *strText = @"";
                                           id obj = [[optionArray objectAtIndex:i]valueForKey:@"option_value"];
                                           if (obj != nil)
                                           {
                                               strText = [strText stringByAppendingString:[[optionArray objectAtIndex:i]valueForKey:@"option_value"]];
                                           }
                                           
                                           for (NSMutableDictionary *dict in [mDict valueForKey:@"custome_values"])
                                           {
                                               if([[dict valueForKey:@"title"] isEqualToString:strText])
                                               {
                                                   isOption = YES;
                                                   [mDict setValue:strText forKey:@"prd_Attribute_Title"];
                                                   break;
                                               }
                                           }
                                       }
                                       if(!isOption)
                                       {
                                           [mDict setValue:[mDict valueForKey:@"custome_title"] forKey:@"prd_Attribute_Title"];
                                       }
                                       [_arrProductColorAndSize addObject:mDict];
                                   }
                                   
                                   NSLog(@"Responce-- %@",_arrProductColorAndSize);
                                   
                                   CGRect newTableFrame = self.tableViewPrdAttributes.frame;
                                   
                                   newTableFrame.size.height = [_arrProductColorAndSize count] * 36;
                                   self.tableViewPrdAttributes.frame = newTableFrame;
                                   
                                   CGRect viewProductFrame = self.viewPrdAttributes.frame;
                                   
                                   viewProductFrame.origin.y = self.tableViewPrdAttributes.frame.origin.y + self.tableViewPrdAttributes.frame.size.height ;
                                   
                                   self.viewPrdAttributes.frame = viewProductFrame;
                                   
                                   float heightScroll = 891 + ([_arrProductColorAndSize count]-1) * 36;
                                   self.scrollViewProductDetail.contentSize = CGSizeMake(self.scrollViewProductDetail.frame.size.width, heightScroll);
                                   
                                   [self.tableViewPrdAttributes reloadData];
                               }
                           }];
}

-(void)productGalleryFullView:(UITapGestureRecognizer *)gesture
{
    //gesture.view.tag
    
    NSString *strXIB = @"ViewFullSizeImageVC_iPhone3.5";
    if(IS_IPHONE5)
    {
        strXIB = @"ViewFullSizeImageVC";
    }
    
    ViewFullSizeImageVC *viewfullsizeimageview=[[ViewFullSizeImageVC alloc]initWithNibName:strXIB bundle:nil];
    viewfullsizeimageview.image_tag = gesture.view.tag;
    viewfullsizeimageview.imageArray = mutArrayProductImages;
    [self presentViewController:viewfullsizeimageview animated:YES completion:nil];
}

- (IBAction)change_image_via_page:(id)sender
{
    CGFloat x = _pagecontroller.currentPage * self.scrolViewPrductImages.frame.size.width;
    [self.scrolViewPrductImages setContentOffset:CGPointMake(x, 0) animated:YES];
}


#pragma mark - ScrollView  Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrolViewPrductImages.frame.size.width;
    int pageNo = floor((self.scrolViewPrductImages.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pagecontroller.currentPage=pageNo;
    
}
//end new dev

-(IBAction)actionHideProductDetail:(id)sender
{
    isProductEdit = NO;
    _viewProductDetail.hidden = YES;
    //pickerViewDetail.hidden = YES;
    
    [UIView transitionFromView:_viewProductDetail toView:self.tblViewProductBag duration:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft
                    completion: ^(BOOL inFinished) {
                        //do any post animation actions here
                    }];
    [self.tblViewProductBag reloadData];
}

- (IBAction)CancelButtonPressed:(id)sender
{
    self.tableViewPrdAttributes.userInteractionEnabled = YES;
    lblProductPrice.text = [NSString stringWithFormat:@"%@%.02f",objAppDelegate.currencySymbol,[[_mutDictProductDetail objectForKey:@"prd_price"] floatValue]];
    pickerViewColor.hidden = YES;
    [self.pickerToolbar1 removeFromSuperview];
}

- (IBAction)doneBtnPressToGetValue:(id)sender
{
    self.tableViewPrdAttributes.userInteractionEnabled = YES;
    
    pickerViewColor.hidden = YES;
    
    [self.pickerToolbar1 removeFromSuperview];
    
    [[_arrProductColorAndSize objectAtIndex:isSelectedPicker]setValue:strAttributeTitile forKey:@"prd_Attribute_Title"];
    [self.tableViewPrdAttributes reloadData];
}

#pragma mark - Image Sharing From Face Book

-(IBAction)btnActionFaceBookSharing:(id)sender
{
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [controller setInitialText:@"What do you think? Should i buy this?"];
    [controller addURL:[NSURL URLWithString:[_mutDictProductDetail valueForKey:@"product_url"]]];
    
    [controller addImage:imgViewProducts.image];
    
    [self presentViewController:controller animated:YES completion:Nil];
}

-(IBAction)btnActionTwitterSharing:(id)sender
{
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    [controller setInitialText:@"What do you think? Should I buy this?"];
    [controller addURL:[NSURL URLWithString:[_mutDictProductDetail valueForKey:@"product_url"]]];
    [controller addImage:imgViewProducts.image];
    [self presentViewController:controller animated:YES completion:Nil];
}

-(IBAction)btnActionMailSharing:(id)sender
{
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init]  ;
	mailComposer.mailComposeDelegate = self;
	
	if ([MFMailComposeViewController canSendMail])
    {
		[mailComposer setToRecipients:nil];
		[mailComposer setSubject:@"What do you think? Should I buy this?"];
        NSString *message = [@"Check it out at:\n\n\n" stringByAppendingString:[_mutDictProductDetail valueForKey:@"product_url"]];
        
		[mailComposer setMessageBody:message isHTML:YES];
        
        NSData *imageData = UIImagePNGRepresentation(imgViewProducts.image);
        
		[mailComposer addAttachmentData:imageData mimeType:@"image/jpeg" fileName:@""];
		
		
        [self presentViewController:mailComposer animated:YES completion:nil];
	}//if
}

-(IBAction)btnActionGooglePlus:(id)sender
{
    static NSString * const kClientID =@"586890710364-pbld9i8gsg7ma3qu54ahn6jdpfc734rr.apps.googleusercontent.com";
    
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.clientID = kClientID;
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    
    [GPPSignIn sharedInstance].actions = [NSArray arrayWithObjects:
                                          @"http://schemas.google.com/AddActivity",
                                          @"http://schemas.google.com/BuyActivity",
                                          @"http://schemas.google.com/CheckInActivity",
                                          @"http://schemas.google.com/CommentActivity",
                                          @"http://schemas.google.com/CreateActivity",
                                          @"http://schemas.google.com/ListenActivity",
                                          @"http://schemas.google.com/ReserveActivity",
                                          @"http://schemas.google.com/ReviewActivity",
                                          nil];
    signIn.delegate = self;
    [signIn authenticate];
    
    id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
    
    // Set any prefilled text that you might want to suggest
    
    [shareBuilder setURLToShare:[NSURL URLWithString:[_mutDictProductDetail valueForKey:@"product_url"]]];
    [shareBuilder setPrefillText:@"What do you think? Should I buy this?"];
    
    //[shareBuilder attachImage:imgViewProducts.image];
    
    [shareBuilder open];
}



#pragma mark - GPPSignInDelegate

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error
{
    if (error)
    {
        return;
    }
    [self reportAuthStatus];
}


#pragma mark - GPPShareDelegate

- (void)finishedSharing:(BOOL)shared {
    NSString *text = shared ? @"Success" : @"Canceled";
    //shareStatus_.text = [NSString stringWithFormat:@"Status: %@", text];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:text delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)reportAuthStatus
{
    if ([GPPSignIn sharedInstance].authentication)
    {
        // signInAuthStatus_.text = @"Status: Authenticated";
        //[self retrieveUserInfo];
        //[self enableSignInSettings:NO];
    }
    else
    {
        // To authenticate, use Google+ sign-in button.
        // signInAuthStatus_.text = @"Status: Not authenticated";
        //[self enableSignInSettings:YES];
    }
}

- (IBAction) btnMoreInfoAction:(id)sender
{
    viewMoreInfo.hidden = NO;
    txtViewMoreInfo.text = [_mutDictProductDetail objectForKey:@"prd_longdesc"];
    [self.view bringSubviewToFront:viewMoreInfo];
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = .50f;
    transition.type =  @"moveIn";
    transition.subtype = @"fromTop";
    [viewMoreInfo.layer removeAllAnimations];
    [self.view addSubview:viewMoreInfo];
    [viewMoreInfo.layer addAnimation:transition forKey:kCATransition];
}

- (IBAction) btnCloseMoreInfoAction:(id)sender
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = .50f;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromBottom;
    //[viewMoreInfo.layer removeAllAnimations];
    viewMoreInfo.hidden = YES;
    [viewMoreInfo.layer addAnimation:transition forKey:kCATransition];
    
}




@end
