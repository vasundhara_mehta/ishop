//
//  PaymentDetailViewController.m
//  French Bakery
//
//  Created by Avnish Sharma on 10/11/13.
//  Copyright (c) 2013 Mayank. All rights reserved.
//

#import "PaymentDetailViewController.h"
#import "MBProgressHUD.h"
#import "ASIFormDataRequest.h"
#import "Constants.h"
#import "iShopServices.h"
#import "JSON.h"

@interface PaymentDetailViewController ()

@end

@implementation PaymentDetailViewController
@synthesize arrayCardType;
@synthesize doneButton;
@synthesize txtFldCvvNumber,arrayMonth,arrayYear;
@synthesize requestFor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [pickerViewCountry setHidden:YES];
    
   // [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.arrayYear = [[NSMutableArray alloc] initWithObjects:@"2014", @"2015", @"2016", @"2017",@"2018",@"2019",@"2020",@"2021",@"2022",@"2023", nil];

    self.arrayMonth = [[NSMutableArray alloc] initWithObjects:@"01", @"02", @"03", @"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    UIImage *image=[UIImage imageNamed:@"remove.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnCancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    UIImage *image1=[UIImage imageNamed:@"black_arrow.png"];
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.bounds = CGRectMake( 0, 0, image1.size.width, image1.size.height );
    [buttonBack setBackgroundImage:image1 forState:UIControlStateNormal];
    [buttonBack addTarget:self action:@selector(btnCancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonItemRight = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = barButtonItemRight;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    NSURL *onwURL = [[NSURL alloc] initWithString:set_card_type];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:onwURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0];
    [request addValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if( theConnection )
    {
        nsDataCardType = [NSMutableData data];
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
    
    NSLog(@"self.requestFor = %d",self.requestFor);
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    ObjAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.title =@"PAYMENT DETAIL";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSURLConnection Delegate

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [nsDataCardType setLength: 0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [nsDataCardType appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConenction");
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"DONE. Received Bytes: %d", [nsDataCardType length]);
    self.arrayCardType = [NSJSONSerialization JSONObjectWithData:nsDataCardType options:NSJSONReadingMutableContainers error:nil];
    [self.arrayCardType removeObjectAtIndex:0];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - UIPickerView Delegate methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    int count = 0;
    
    switch (isSelectedPickr)
    {
        case 0:
            count = [self.arrayCardType count];
            break;
        case 1:
            count = [self.arrayYear count];
            break;
        case 2:
            count = [self.arrayMonth count];
            break;
        default:
            break;
    }
    
   	return count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30.0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, pickerViewCountry.frame.size.width, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:16];
    
    NSString *strValue;
    switch (isSelectedPickr)
    {
        case 0:
            strValue = [[self.arrayCardType objectAtIndex:row]objectForKey:@"label"];
            break;
        case 1:
            strValue = [self.arrayYear objectAtIndex:row];
            break;
        case 2:
            strValue = [self.arrayMonth objectAtIndex:row];
            break;
        default:
            break;
    }
    
    label.text = [NSString stringWithFormat:@"%@", strValue];
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    iRow = row;
    [pickerViewCountry setHidden:YES];
    switch (isSelectedPickr)
    {
        case 0:
            lblCardType.text = [[self.arrayCardType objectAtIndex:row]objectForKey:@"label"];
            strCartType = [[self.arrayCardType objectAtIndex:row]objectForKey:@"value"];
            break;
        case 1:
            lblYear.text = [self.arrayYear objectAtIndex:row] ;
            break;
        case 2:
            lblmonth.text = [self.arrayMonth objectAtIndex:row];
            break;
        default:
            break;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	return 300;
}

#pragma mark - UIButton Action

- (IBAction)btnCardTypeAction:(id)sender
{
    [txtFldCardNumber resignFirstResponder];
    [txtFldExpiryMonth resignFirstResponder];
    [txtFldExpiryYear resignFirstResponder];
    [txtFldCvvNumber resignFirstResponder];
    isSelectedPickr = 0;
    [pickerViewCountry setHidden:NO];
    [pickerViewCountry reloadAllComponents];
}

- (IBAction) btnYearDDAction:(id)sender
{
    [txtFldCardNumber resignFirstResponder];
    [txtFldExpiryMonth resignFirstResponder];
    [txtFldExpiryYear resignFirstResponder];
    [txtFldCvvNumber resignFirstResponder];
    isSelectedPickr = 1;
    [pickerViewCountry setHidden:NO];
    [pickerViewCountry reloadAllComponents];

}
- (IBAction) btnMonthDDAction:(id)sender
{
    
    [txtFldCardNumber resignFirstResponder];
    [txtFldExpiryMonth resignFirstResponder];
    [txtFldExpiryYear resignFirstResponder];
    [txtFldCvvNumber resignFirstResponder];
    isSelectedPickr = 2;
    [pickerViewCountry setHidden:NO];
    [pickerViewCountry reloadAllComponents];
}

- (IBAction) btnContinueAction:(id)sender
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [NSDateComponents new];
    comps.month = 0;
    NSDate *date1 = [calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    NSDateComponents *components = [calendar components:NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date1]; // Get necessary date components
    NSLog(@"Previous month: %d",[components month]);

    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    NSString *dateString1 = [NSString stringWithFormat:@"%ld", (long)[components month]];
    
    NSString *strTypeYear = lblYear.text;
    NSString *strTypeMonth = lblmonth.text;
    
    int y = [dateString intValue];
    int year = [strTypeYear intValue];
    
    int m = [dateString1 intValue];
    int month = [strTypeMonth intValue];

    NSString *strCardExpiryYear = lblYear.text;
    
    //NSString *strCardExpiryMonth = lblmonth;
    
    if([lblCardType.text isEqualToString:@""] || [lblCardType.text isEqualToString:@"-Please Select-"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please select Card type." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtFldCardNumber.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter Card number." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtFldCvvNumber.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter Cvv number." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtFldLastOnwerName.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter Card Owner name." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([lblYear.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter Credit Card expiry year." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
        NSComparisonResult result;
        result = [dateString compare:strCardExpiryYear]; // comparing two dates
        if(result == NSOrderedAscending)
        {
            NSLog(@"today is less");
        }
        else if(result == NSOrderedDescending)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Your Credit Card has Expired." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            return;
            NSLog(@"newDate is less");
        }
        else
        {
            NSLog(@"Both dates are same");
        }
    }
    if([lblmonth.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter Credit Card expiry month." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
        if (y <= year)
        {
            if(y == year)
            {
                if (m <= month)
                {
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Your Credit Card has Expired." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    return;
                }
            }
        }
        else
        {
        }
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    iRequest = 0;
    
    NSURL *url = [NSURL URLWithString:set_Payment_Method];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.timeOutSeconds = 120;
    [request setPostValue:ObjAppDelegate.strCartId forKey:@"CartId"];
    [request setPostValue:txtFldCardNumber.text forKey:@"cc_number"];
    [request setPostValue:txtFldCvvNumber.text forKey:@"cc_cid"];
    [request setPostValue:txtFldLastOnwerName.text forKey:@"cc_owner"];
    [request setPostValue:strCartType forKey:@"cc_type"];
    [request setPostValue:lblYear.text forKey:@"cc_exp_year"];
    [request setPostValue:lblmonth.text forKey:@"cc_exp_month"];
    [request setDidFinishSelector:@selector(requestFinishedForService:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDelegate:self];
    [request startAsynchronous];
}

- (IBAction) btnCancelAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ASIHTTP Response

-(void)requestFinishedForService:(ASIHTTPRequest *)request
{
    if (iRequest == 0)
    {
        NSString *string =[request responseString];
        NSData *data = [request responseData];
        NSLog(@"string -- %@",string);
        
        NSDictionary *dictResponce = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        BOOL checkResponse = [[dictResponce objectForKey:@"value"] boolValue];
        if (checkResponse)
        {
            iRequest = 1;
            NSURL *url = [NSURL URLWithString:set_Cart_info];
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
            //[request setPostValue:@"getCrtInfo" forKey:@"methodName"];
            [request setPostValue:ObjAppDelegate.strCartId forKey:@"CartId"];
            request.timeOutSeconds = 120;
            [request setDidFinishSelector:@selector(requestFinishedForService:)];
            [request setDidFailSelector:@selector(requestFailed:)];
            [request setDelegate:self];
            [request startAsynchronous];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your Card number or Cvv number may be incorrect!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }
    else if (iRequest == 1)
    {
        NSString *string =[request responseString];
        NSData *data = [request responseData];
        NSLog(@"string -- %@",string);
        
       _mDictPaymentResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        if ([[_mDictPaymentResponse objectForKey:@"error"] isEqualToString:@""])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Thank you for your payment." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            alert.tag = 1001;
            [alert show];
            /*
             customer_id
             product_id
             sku
             product_name
             option_name 
             option_value
             increment_id
             */
//            if(self.requestFor == FINALPAYMENT)
//            {
//                NSString *customer_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"];
//                if(customer_id == nil)
//                {
//                    customer_id = @"";
//                }
//                //InsertSizeOptionValue to Admin
//                NSDictionary *parameters_size = @{@"customer_id":customer_id,@"product_id":[_mDictProductDetail valueForKeyPath:@"prd_id"],@"sku":[_mDictProductDetail valueForKey:@"prd_sku"],@"product_name":[_mDictProductDetail valueForKey:@"prd_name"],@"option_name":@"size",@"option_value":[_mDictProductDetail valueForKey:@"prd_size"],@"increment_id":[_mDictPaymentResponse valueForKey:@"value"]};
//                
//                if(![[_mDictProductDetail valueForKey:@"prd_size"] isEqualToString:@""])
//                {
//                    [iShopServices PostMethodWithApiMethod:@"InsertSizeOptionValue" Withparms:parameters_size WithSuccess:^(id response)
//                     {
//                         NSLog(@"response = %@",[response JSONValue]);
//                     } failure:^(NSError *error)
//                     {
//                         NSLog(@"Error =%@",[error description]);
//                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                     }];
//                }
//                //InsertSizeOptionValue to Admin
//                NSDictionary *parameters_color = @{@"customer_id":customer_id,@"product_id":[_mDictProductDetail valueForKeyPath:@"prd_id"],@"sku":[_mDictProductDetail valueForKey:@"prd_sku"],@"product_name":[_mDictProductDetail valueForKey:@"prd_name"],@"option_name":@"color",@"option_value":[_mDictProductDetail valueForKey:@"prd_color"],@"increment_id":[_mDictPaymentResponse valueForKey:@"value"]};
//                
//                if(![[_mDictProductDetail valueForKey:@"prd_color"] isEqualToString:@""])
//                {
//                    [iShopServices PostMethodWithApiMethod:@"InsertColorOptionValue" Withparms:parameters_color WithSuccess:^(id response)
//                     {
//                         NSLog(@"response = %@",[response JSONValue]);
//                         
//                     } failure:^(NSError *error)
//                     {
//                         NSLog(@"Error =%@",[error description]);
//                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                     }];
//                }
//            }
//            if(self.requestFor == ADDTOCARTPAYMENT)
//            {
//                for(int i = 0; i < [ObjAppDelegate.arrAddToBag count]; i++)
//                {
//                    
//                    NSMutableDictionary *mDictProduct = [ObjAppDelegate.arrAddToBag objectAtIndex:i];
//                    
//                    NSString *customer_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"];
//                    if(customer_id == nil)
//                    {
//                        customer_id = @"";
//                    }
//                    
//                    //InsertSizeOptionValue to Admin
//                    NSDictionary *parameters_size = @{@"customer_id":customer_id,@"product_id":[mDictProduct valueForKeyPath:@"prd_id"],@"sku":[mDictProduct valueForKey:@"prd_sku"],@"product_name":[mDictProduct valueForKey:@"prd_name"],@"option_name":@"size",@"option_value":[mDictProduct valueForKey:@"pro_size"],@"increment_id":[_mDictPaymentResponse valueForKey:@"value"]};
//                    
//                    if(![[_mDictProductDetail valueForKey:@"pro_size"] isEqualToString:@""])
//                    {
//                        [iShopServices PostMethodWithApiMethod:@"InsertSizeOptionValue" Withparms:parameters_size WithSuccess:^(id response)
//                         {
//                             NSLog(@"response = %@",[response JSONValue]);
//                             
//                         } failure:^(NSError *error)
//                         {
//                             NSLog(@"Error =%@",[error description]);
//                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                         }];
//                    }
//                    
//                    //InsertSizeOptionValue to Admin
//                    NSDictionary *parameters_color = @{@"customer_id":customer_id,@"product_id":[mDictProduct valueForKeyPath:@"prd_id"],@"sku":[mDictProduct valueForKey:@"prd_sku"],@"product_name":[mDictProduct valueForKey:@"prd_name"],@"option_name":@"color",@"option_value":[mDictProduct valueForKey:@"pro_color"],@"increment_id":[_mDictPaymentResponse valueForKey:@"value"]};
//                    
//                    if(![[_mDictProductDetail valueForKey:@"pro_color"] isEqualToString:@""])
//                    {
//                        [iShopServices PostMethodWithApiMethod:@"InsertColorOptionValue" Withparms:parameters_color WithSuccess:^(id response)
//                         {
//                             NSLog(@"response = %@",[response JSONValue]);
//                             
//                         } failure:^(NSError *error)
//                         {
//                             NSLog(@"Error =%@",[error description]);
//                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                         }];
//                    }
//                }
//            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
    else if (iRequest == 2)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [ObjAppDelegate getCheckOutHistory];
    }
}

#pragma mark - UIAlert Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1001)
    {
        ObjAppDelegate.isPaymentComplete = YES;
        
        //[ObjAppDelegate.arrAddToBag removeAllObjects];
        //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cart_array"];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Please wait...";
        hud.dimBackground = YES;
        
        for(int i = 0; i < [ObjAppDelegate.arrAddToBag count]; i++)
        {
            iRequest = 2;
            NSMutableDictionary *mDict = [ObjAppDelegate.arrAddToBag objectAtIndex:i];
            NSURL *url = [NSURL URLWithString:get_user_checkout_history];
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
            request.timeOutSeconds = 120;
            
            [request setPostValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"] forKey:@"customer_id"];
            [request setPostValue:[mDict objectForKey:@"prd_id"] forKey:@"product_id"];
            [request setPostValue:@"0" forKey:@"get"];
            [request setDidFinishSelector:@selector(requestFinishedForService:)];
            [request setDidFailSelector:@selector(requestFailed:)];
            [request setDelegate:self];
            [request startAsynchronous];
        }

        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
    }
}

#pragma mark - Uitextfield delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == txtFldCardNumber || textField == txtFldCvvNumber || textField == lblYear || textField == lblmonth)
    {
        [self performSelector:@selector(addButtonToKeyboard) withObject:nil afterDelay:0.1];
    }
    if(textField == txtFldLastOnwerName || textField == lblYear || textField == lblmonth)
    {
        if(IS_IPHONE5)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -160, 320 , 568)];
            [UIView commitAnimations];
        }
        else
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -175, 320 , 480)];
            [UIView commitAnimations];
        }
    }
    if(textField == txtFldLastOnwerName)
    {
        [doneButton removeFromSuperview];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == txtFldLastOnwerName || textField == lblYear || textField == lblmonth)
    {
        if(IS_IPHONE5)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 568)];
            [UIView commitAnimations];
        }
        else
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 480)];
            [UIView commitAnimations];
        }
    }
    [textField resignFirstResponder];
    return YES;
}

- (void)addButtonToKeyboard
{
    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton.frame = CGRectMake(0, 163, 106, 53);
    doneButton.adjustsImageWhenHighlighted = NO;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.0)
    {
        [doneButton setImage:[UIImage imageNamed:@"DoneUp3.png"] forState:UIControlStateNormal];
        [doneButton setImage:[UIImage imageNamed:@"DoneDown3.png"] forState:UIControlStateHighlighted];
    }
    else
    {
        [doneButton setImage:[UIImage imageNamed:@"DoneUp.png"] forState:UIControlStateNormal];
        [doneButton setImage:[UIImage imageNamed:@"DoneDown.png"] forState:UIControlStateHighlighted];
    }
    [doneButton addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
    // locate keyboard view
    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    UIView* keyboard;
    for(int i=0; i<[tempWindow.subviews count]; i++)
    {
        keyboard = [tempWindow.subviews objectAtIndex:i];
        // keyboard found, add the button
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2)
        {
            if([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
                [keyboard addSubview:doneButton];
        }
        else
        {
            if([[keyboard description] hasPrefix:@"<UIKeyboard"] == YES)
                [keyboard addSubview:doneButton];
        }
    }
}

- (void)doneButton:(id)sender
{
	NSLog(@"doneButton");
    if(lblYear || lblmonth)
    {
        if(IS_IPHONE5)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 568)];
            [UIView commitAnimations];
        }
        else
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 480)];
            [UIView commitAnimations];
        }
    }
    [txtFldCardNumber resignFirstResponder];
    [txtFldExpiryMonth resignFirstResponder];
    [txtFldExpiryYear resignFirstResponder];
    [txtFldCvvNumber resignFirstResponder];
    [doneButton removeFromSuperview];
}

@end
