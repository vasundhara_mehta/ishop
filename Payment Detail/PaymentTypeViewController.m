//
//  PaymentTypeViewController.m
//  IShop
//
//  Created by Avnish Sharma on 7/17/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "PaymentTypeViewController.h"
#import "CustomCell.h"
#import "PaymentDetailViewController.h"
#import "iShopServices.h"
#import "MBProgressHUD.h"
#import "JSON.h"

@interface PaymentTypeViewController ()

@end

@implementation PaymentTypeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self getPaymentTypeList];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_tableViewPaymentTypeList reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)actionBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma  mark -- GET PAYMENT TYPE LIST
-(void)getPaymentTypeList
{
    _mArrayPaymentType = [[NSMutableArray alloc]initWithObjects:@"Credit Cart",@"Cash On Delivery",@"Mony Order", nil];
    [_tableViewPaymentTypeList reloadData];
}

#pragma mark -- TABLEVIEW DELEGATE METHODS
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectio
{
    return [_mArrayPaymentType count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Prd_Attributes";
    CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *cellView = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
        cell = (CustomCell *)[cellView objectAtIndex:1];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    cell.lblPrdAttibuteTitle.text = [_mArrayPaymentType objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        //Credit Cart
        PaymentDetailViewController *objPaymentDetailViewController;
        if (IS_IPHONE5)
        {
            objPaymentDetailViewController = [[PaymentDetailViewController alloc] initWithNibName:@"PaymentDetailViewController" bundle:nil];
        }
        else
        {
            objPaymentDetailViewController = [[PaymentDetailViewController alloc] initWithNibName:@"PaymentDetailViewController_3.5screen" bundle:nil];
        }
       // objPaymentDetailViewController.requestFor = requestFor;
       // objPaymentDetailViewController.mDictProductDetail = _mDictProductDetail;
        [self.navigationController pushViewController:objPaymentDetailViewController animated:YES];
    }
    else if (indexPath.row == 1)
    {
        //Cash On Delivery
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Please wait...";
        hud.dimBackground = YES;
        objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSDictionary *parameters = @{@"CartId":objAppDelegate.strCartId};
       
        [iShopServices PostMethodWithApiMethod:@"CashOnDeliveryPayMethod" Withparms:parameters WithSuccess:^(id response)
         {
             NSLog(@"Mony Order Payment Response = %@",[response JSONValue]);
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Thank you for your payment." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [alert show];
         } failure:^(NSError *error)
         {
             NSLog(@"Error =%@",[error description]);
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }];
    }
    else if(indexPath.row == 2)
    {
        //Mony Order
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Please wait...";
        hud.dimBackground = YES;
        objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSDictionary *parameters = @{@"CartId":objAppDelegate.strCartId};
        
        [iShopServices PostMethodWithApiMethod:@"MonyOrderPayMethod" Withparms:parameters WithSuccess:^(id response)
        {
            NSLog(@"Mony Order Payment Response = %@",[response JSONValue]);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Thank you for your payment." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        } failure:^(NSError *error)
         {
             NSLog(@"Error =%@",[error description]);
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }];
    }
}


@end
