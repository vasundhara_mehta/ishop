//
//  PaymentTypeViewController.h
//  IShop
//
//  Created by Avnish Sharma on 7/17/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface PaymentTypeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *objAppDelegate;
 }
@property(nonatomic ,retain)IBOutlet UITableView *tableViewPaymentTypeList;
@property(nonatomic ,retain)NSMutableArray *mArrayPaymentType;
@end
