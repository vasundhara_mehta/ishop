//
//  ReviewAndRatingViewController.m
//  IShop
//
//  Created by Avnish Sharma on 7/21/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "ReviewAndRatingViewController.h"
#import "StarRatingView.h"
#import "iShopServices.h"
#import "MBProgressHUD.h"
#import "CustomCell.h"
#import "JSON.h"

@interface ReviewAndRatingViewController ()
{
    StarRatingView *priceRatingView,*valueRatingView,*qualityRatingView;
    int priceRating,valueRating,qualityRating;
}
@end

@implementation ReviewAndRatingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBarHidden = YES;
    _viewWriteReview.hidden = YES;
    _viewCustomerReview.hidden = NO;
    
    [self getReviewsAndRating];
    [self showRatingViews];
    
}

-(void)getReviewsAndRating
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    NSDictionary *parameters = @{@"product_id":_strProductID};
    [iShopServices PostMethodWithApiMethod:@"GetReviewsAndRating" Withparms:parameters WithSuccess:^(id response)
     {
         NSLog(@"Rating Response =%@",response);
         
         mArrayResponse = [response JSONValue];
         [_tableViewCustomerReviews reloadData];
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     } failure:^(NSError *error)
     {
         NSLog(@"Error = %@",[error description]);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     }];
}

-(void)showRatingViews
{
    float yPosPrice = 14,yPosValue = 43,yPosQuantity = 73;
    
    if(IS_IPHONE5)
    {
        yPosPrice = 23,yPosValue = 63,yPosQuantity = 103;
    }
    
    //Show  Price Rating view
    float rating = 0;
    priceRatingView = [[StarRatingView alloc]initWithFrame:CGRectMake(158, yPosPrice, 103, 18) andRating:rating withLabel:NO animated:YES withSetRating:YES];
    // userRatingView.delegate = self;
    priceRatingView.userInteractionEnabled = YES;
    priceRating = priceRatingView.userRating;
    // NSLog(@"User Rating = %d",userRatingView.userRating);
    [_viewWriteReview addSubview:priceRatingView];
    
    //Show Value Rating view
    valueRatingView = [[StarRatingView alloc]initWithFrame:CGRectMake(158, yPosValue, 103, 18) andRating:rating withLabel:NO animated:YES withSetRating:YES];
    // userRatingView.delegate = self;
    valueRatingView.userInteractionEnabled = YES;
    valueRating = valueRatingView.userRating;
    // NSLog(@"User Rating = %d",userRatingView.userRating);
    [_viewWriteReview addSubview:valueRatingView];
    
    //Show Quality Rating view
    qualityRatingView = [[StarRatingView alloc]initWithFrame:CGRectMake(158, yPosQuantity, 103, 18) andRating:rating withLabel:NO animated:YES withSetRating:YES];
    // userRatingView.delegate = self;
    qualityRatingView.userInteractionEnabled = YES;
    qualityRating = qualityRatingView.userRating;
    // NSLog(@"User Rating = %d",userRatingView.userRating);
    [_viewWriteReview addSubview:qualityRatingView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == _txtSummaryReview)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        self.view.frame = CGRectMake(0, -70, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
    return TRUE;
}

//return Keyboard
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqual:@"\n"])
    {
        [textView resignFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        return NO;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if(textView==_txtViewReview)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        self.view.frame = CGRectMake(0, -180, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
    return TRUE;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(textField == _txtSummaryReview)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
    return YES;
}

#pragma mark -- BACK 
-(IBAction)actionBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- SUBMIT REVIEWS
-(IBAction)actionSubmitReview:(id)sender
{
    qualityRating = qualityRatingView.userRating;
    priceRating = priceRatingView.userRating;
    valueRating = valueRatingView.userRating;
    NSDictionary *parameters = @{@"product_id":_strProductID,@"title":_txtSummaryReview.text,@"detail":_txtViewReview.text,@"nicname":_txtNickName.text,@"quality":[NSString stringWithFormat:@"%d",qualityRating],@"value":[NSString stringWithFormat:@"%d",valueRating],@"price":[NSString stringWithFormat:@"%d",priceRating]};
    [iShopServices PostMethodWithApiMethod:@"AddReviewsAndRating" Withparms:parameters WithSuccess:^(id response)
     {
         NSLog(@"Rating Response =%@",response);
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Reviews and Rating submitted successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
         [alert show];
     } failure:^(NSError *error)
     {
         NSLog(@"Error = %@",[error description]);
     }];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *mDict = [mArrayResponse objectAtIndex:indexPath.row] ;
    CustomCell *cell = (CustomCell *)[[[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil]objectAtIndex:3];
    
    
    id displayNameTypeValue = [mDict valueForKey:@"detail"];
    NSString *displayNameType = @"";
    if (displayNameTypeValue != [NSNull null])
    {
        displayNameType = [mDict valueForKey:@"detail"];
    }
    
    NSString *strComment = [@"" stringByAppendingString:displayNameType];
    cell.lblDetail.text = strComment;
    
    CGRect labelFrame = cell.lblDetail.frame;
    
    CGRect expectedFrame = [cell.lblDetail.text boundingRectWithSize:CGSizeMake(cell.lblDetail.frame.size.width, 9999)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                    cell.lblDetail.font, NSFontAttributeName,
                                                                    nil]
                                                           context:nil];
    
    labelFrame.size = expectedFrame.size;
    labelFrame.size.height = ceil(labelFrame.size.height);
    cell.lblDetail.frame = labelFrame;
    
    return cell.lblDetail.frame.size.height + 150;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [mArrayResponse count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Customerrating";
    CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil )
    {
        NSArray *nibCell = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
        cell = [nibCell objectAtIndex:3];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSMutableDictionary *mDict = [mArrayResponse objectAtIndex:indexPath.row];
    
    cell.lblNickName.text = [mDict valueForKey:@"nicname"];
    cell.lblTitle.text = [mDict valueForKey:@"title"];
    
    
    id displayNameTypeValue = [mDict valueForKey:@"detail"];
    NSString *displayNameType = @"";
    if (displayNameTypeValue != [NSNull null])
    {
        displayNameType = [mDict valueForKey:@"detail"];
    }
    cell.lblDetail.text = displayNameType;
    
    NSMutableDictionary *dict = [mDict valueForKey:@"rating"];
    NSString *starRating = [[dict objectForKey:@"Price"] objectAtIndex:0];
    float rating = [starRating floatValue];
    StarRatingView* priceStarview = [[StarRatingView alloc]initWithFrame:CGRectMake(180, 55, 100, 15) andRating:rating withLabel:NO animated:YES withIndexPath:indexPath.row];
    // starview.delegate = self;
    priceStarview.userInteractionEnabled = NO;
    [cell addSubview:priceStarview];
    
    NSString *valueRate = [[dict objectForKey:@"Value"] objectAtIndex:0];
    float valuerating = [valueRate floatValue];
    StarRatingView* valueStarview = [[StarRatingView alloc]initWithFrame:CGRectMake(180, 76, 100, 15) andRating:valuerating withLabel:NO animated:YES withIndexPath:indexPath.row];
    // starview.delegate = self;
    valueStarview.userInteractionEnabled = NO;
    [cell addSubview:valueStarview];
    
    NSString *quantityRate = [[dict objectForKey:@"Quality"] objectAtIndex:0];
    float quantityrating = [quantityRate floatValue];
    StarRatingView* quantityStarview = [[StarRatingView alloc]initWithFrame:CGRectMake(180, 98, 100, 15) andRating:quantityrating withLabel:NO animated:YES withIndexPath:indexPath.row];
    // starview.delegate = self;
    quantityStarview.userInteractionEnabled = NO;
    [cell addSubview:quantityStarview];
    
    return cell;
}



#pragma mark - Segment Control Method
- (IBAction)segmentAction:(id)sender
{
    // valuechanged connected function
    UISegmentedControl *segControll = (UISegmentedControl *)sender;
    if(segControll.selectedSegmentIndex == 0)
    {
        _viewWriteReview.hidden = YES;
        _viewCustomerReview.hidden = NO;
        [_tableViewCustomerReviews reloadData];
    }
    else
    {
        _viewWriteReview.hidden = NO;
        _viewCustomerReview.hidden = YES;
    }
}

@end
