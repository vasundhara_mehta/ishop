//
//  BillingAddressViewController.m
//  French Bakery
//
//  Created by Avnish Sharma on 10/4/13.
//  Copyright (c) 2013 Mayank. All rights reserved.
//

#import "BillingAddressViewController.h"
#import "MBProgressHUD.h"
#import "ASIFormDataRequest.h"
#import "PaymentDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PaymentTypeViewController.h"

@interface BillingAddressViewController ()

@end

@implementation BillingAddressViewController

@synthesize arrayCountry,arrayStates;
@synthesize keyboardToolbar;
@synthesize doneButton;
@synthesize requestFor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [pickerViewCountry setHidden:YES];
    self.arrayStates = [[NSMutableArray alloc] init];
    
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    UIImage *image=[UIImage imageNamed:@"remove.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnCancelAction:) forControlEvents:UIControlEventTouchUpInside];
   
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    UIImage *image1=[UIImage imageNamed:@"black_arrow.png"];
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.bounds = CGRectMake( 0, 0, image1.size.width, image1.size.height );
    [buttonBack setBackgroundImage:image1 forState:UIControlStateNormal];
    [buttonBack addTarget:self action:@selector(btnCancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonItemRight = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = barButtonItemRight;
    
    txtFldFirstName.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"firstname"];
    txtFldLastName.text =[[NSUserDefaults standardUserDefaults] valueForKey:@"lastname"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   // [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.title = @"BILLING ADDRESS";
    
    ObjAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self CountryAction];
}

#pragma mark - UIButton Action

- (void) CountryAction
{
    isCheckService = NO;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    NSURL *onwURL = [[NSURL alloc] initWithString:all_country];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:onwURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0];
    [request addValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if( theConnection )
    {
        nsDataCountry = [NSMutableData data];
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
}

- (IBAction) btnCountryAction:(id)sender
{
    if(IS_IPHONE5)
    {
        keyboardToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 308, 320, 44)];
    }
    else
    {
        keyboardToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 220, 320, 44)];
    }
    
    UIButton *done_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    done_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [done_Button setTitle:@"Done" forState:UIControlStateNormal];
    [done_Button addTarget:self action:@selector(doneBtnPressToGetValue:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *cancel_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    cancel_Button.frame=CGRectMake(0.0, 0, 60.0, 30.0);
    [cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel_Button addTarget:self action:@selector(CancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *myButton = [[UIBarButtonItem alloc] initWithCustomView:done_Button];
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithCustomView:cancel_Button];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [keyboardToolbar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Bar.png"]] atIndex:0];
    
    CALayer *l=keyboardToolbar.layer;
    [l setCornerRadius:0.5];
    [l setBorderColor:[[UIColor grayColor] CGColor]];
    [l setBorderWidth:0.5];
    [keyboardToolbar setItems:[NSArray arrayWithObjects:btnCancel, flex, myButton, nil]];
    [self.view addSubview:keyboardToolbar];
    
    [txtFldFirstName resignFirstResponder];
    [txtFldLastName resignFirstResponder];
    [txtFldContact resignFirstResponder];
    [txtFldState resignFirstResponder];
    [txtFldStreet resignFirstResponder];
    [txtFldZipCode resignFirstResponder];
    [txtFldCity resignFirstResponder];

    isCheckService = NO;
    [pickerViewCountry setHidden:NO];
    [self.view bringSubviewToFront:pickerViewCountry];
    [pickerViewCountry reloadAllComponents];
}

-(IBAction)CancelButtonPressed:(id)sender
{
    [pickerViewCountry setHidden:YES];
    [keyboardToolbar removeFromSuperview];
}

- (IBAction) btnCancelAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)doneBtnPressToGetValue:(id)sender
{
    [pickerViewCountry setHidden:YES];
    [keyboardToolbar removeFromSuperview];
    if (!isCheckService)
    {
        [self StateAction];
    }
}

- (void)StateAction
{
    isCheckService = YES;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    iRequest = 2;
    NSURL *url = [NSURL URLWithString:region_country];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.timeOutSeconds = 120;
    [request setPostValue:[[self.arrayCountry objectAtIndex:iRow] objectForKey:@"value"] forKey:@"value"];
    [request setDidFinishSelector:@selector(requestFinishedForService:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDelegate:self];
    [request startAsynchronous];
}

- (IBAction) btnStateAction:(id)sender
{
    isCheckService = YES;
    [pickerViewCountry reloadAllComponents];
    [pickerViewCountry setHidden:NO];
}

- (IBAction) btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) btnCountinueAction:(id)sender
{
    if([txtFldFirstName.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter First name." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtFldLastName.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter Last name." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtFldContact.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter Contact number." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtFldContact.text length] < 10)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"The contact no. should contain minimum 10 numbers." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([lblCountryName.text isEqualToString:@""] || [lblCountryName.text isEqualToString:@"Please Select"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please select Country." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if(isStateSelect == YES && ([lblStateName.text isEqualToString:@""] || [lblStateName.text isEqualToString:@"Please Select"]))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please select State." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if(isStateSelect == NO && [txtFldState.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter State." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtFldCity.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter City." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtFldStreet.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter Street address." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([txtFldZipCode.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Enter Zip code." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.dimBackground = YES;
    
    iRequest = 0;
    
    NSURL *url = [NSURL URLWithString:set_cart_Address];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setPostValue:ObjAppDelegate.strCartId forKey:@"CartId"];
    [request setPostValue:txtFldFirstName.text forKey:@"bl_firstname"];
    [request setPostValue:txtFldLastName.text forKey:@"bl_lastname"];
    [request setPostValue:txtFldStreet.text forKey:@"bl_street"];
    [request setPostValue:txtFldCity.text forKey:@"bl_city"];
    request.timeOutSeconds = 120;
    if(![self.arrayStates count] == 0)
    {
        [request setPostValue:[[self.arrayStates objectAtIndex:iRow]objectForKey:@"value"] forKey:@"bl_region"];
    }
    else
    {
        [request setPostValue:txtFldState.text forKey:@"bl_region"];
    }
    
    [request setPostValue:txtFldZipCode.text forKey:@"bl_postcode"];
    [request setPostValue:[[self.arrayCountry objectAtIndex:iRow] objectForKey:@"value"] forKey:@"bl_country_id"];
    [request setPostValue:txtFldContact.text forKey:@"bl_telephone"];
    
    [request setPostValue:txtFldFirstName.text forKey:@"sh_firstname"];
    [request setPostValue:txtFldLastName.text forKey:@"sh_lastname"];
    [request setPostValue:txtFldStreet.text forKey:@"sh_street"];
    [request setPostValue:txtFldCity.text forKey:@"sh_city"];
    
    if(![self.arrayStates count] == 0)
    {
        [request setPostValue:[[self.arrayStates objectAtIndex:iRow]objectForKey:@"value"] forKey:@"sh_region"];
    }
    else
    {
        [request setPostValue:txtFldState.text forKey:@"sh_region"];
    }

    [request setPostValue:txtFldZipCode.text forKey:@"sh_postcode"];
    [request setPostValue:[[self.arrayCountry objectAtIndex:iRow] objectForKey:@"value"] forKey:@"sh_country_id"];
    [request setPostValue:txtFldContact.text forKey:@"sh_telephone"];
    
    [request setDidFinishSelector:@selector(requestFinishedForService:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDelegate:self];
    [request startAsynchronous];
}

#pragma mark - ASIHTTP Response

-(void)requestFinishedForService:(ASIHTTPRequest *)request
{
    if (iRequest == 0)
    {
        NSString *string =[request responseString];
        NSData *data = [request responseData];
        NSLog(@"string -- %@",string);

        NSDictionary *dictResponce = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        BOOL checkResponse = [[dictResponce objectForKey:@"value"] boolValue];
        
        if (checkResponse)
        {
            iRequest = 3;
            
            NSURL *url = [NSURL URLWithString:free_shipMethod];
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
            request.timeOutSeconds = 120;
            [request setPostValue:ObjAppDelegate.strCartId forKey:@"CartId"];
            [request setDidFinishSelector:@selector(requestFinishedForService:)];
            [request setDidFailSelector:@selector(requestFailed:)];
            [request setDelegate:self];
            [request startAsynchronous];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if (iRequest == 1)
    {
        NSData *data = [request responseData];
        
        NSLog(@"%@", [request responseString]);
        NSDictionary *dictResponce = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        BOOL checkResponse = [[dictResponce objectForKey:@"value"] boolValue];
        
        if (checkResponse)
        {
            PaymentTypeViewController *paymentTypeViewController = [[PaymentTypeViewController alloc]initWithNibName:@"PaymentTypeViewController" bundle:nil];
            [self.navigationController pushViewController:paymentTypeViewController animated:YES];
        }else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    else if (iRequest == 2)
    {
        //NSString *string =[request responseString];
        NSData *data = [request responseData];
        //NSLog(@"string -- %@",string);
        
        self.arrayStates = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
         //NSLog(@"string -- %@",string);
        
        if ([self.arrayStates count] == 0 || self.arrayStates == nil )
        {
            btnState.hidden = YES;
            lblStateName.hidden = YES;
            txtFldState.hidden = NO;
            isStateSelect = NO;
        }
        else
        {
            btnState.hidden = NO;
            lblStateName.hidden = NO;
            txtFldState.hidden = YES;
            lblStateName.text = @"Please Select";
            isStateSelect = YES;
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    else if (iRequest == 3)
    {
        NSString *string =[request responseString];
        NSData *data = [request responseData];
        NSLog(@"string -- %@",string);
        
        NSDictionary *dictResponce = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        BOOL checkResponse = [[dictResponce objectForKey:@"value"] boolValue];
        
        if (checkResponse)
        {
            iRequest = 1;
            
            NSURL *url = [NSURL URLWithString:flat_shipMethod];
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
            request.timeOutSeconds = 120;
            [request setPostValue:ObjAppDelegate.strCartId forKey:@"CartId"];
            [request setDidFinishSelector:@selector(requestFinishedForService:)];
            [request setDidFailSelector:@selector(requestFailed:)];
            [request setDelegate:self];
            [request startAsynchronous];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }

    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Check your network connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - UITextfield Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == txtFldContact)
    {
        [self performSelector:@selector(addButtonToKeyboard) withObject:nil afterDelay:0.1];
    }
    if(!(textField == txtFldContact))
    {
        [doneButton removeFromSuperview];
    }
    if(textField == txtFldStreet || textField == txtFldZipCode)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [self.view setFrame:CGRectMake(0 , -150, 320 , 548)];
        [UIView commitAnimations];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == txtFldStreet || textField == txtFldZipCode)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [self.view setFrame:CGRectMake(0, 0, 320, 568)];
        [UIView commitAnimations];
    }
    [textField resignFirstResponder];
    return YES;
}

- (void)addButtonToKeyboard
{
    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton.frame = CGRectMake(0, 163, 106, 53);
    doneButton.adjustsImageWhenHighlighted = NO;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.0)
    {
        [doneButton setImage:[UIImage imageNamed:@"DoneUp3.png"] forState:UIControlStateNormal];
        [doneButton setImage:[UIImage imageNamed:@"DoneDown3.png"] forState:UIControlStateHighlighted];
    }
    else
    {
        [doneButton setImage:[UIImage imageNamed:@"DoneUp.png"] forState:UIControlStateNormal];
        [doneButton setImage:[UIImage imageNamed:@"DoneDown.png"] forState:UIControlStateHighlighted];
    }
    [doneButton addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
    // locate keyboard view
    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    UIView* keyboard;
    for(int i=0; i<[tempWindow.subviews count]; i++)
    {
        keyboard = [tempWindow.subviews objectAtIndex:i];
        // keyboard found, add the button
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2)
        {
            if([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
                [keyboard addSubview:doneButton];
        }
        else
        {
            if([[keyboard description] hasPrefix:@"<UIKeyboard"] == YES)
                [keyboard addSubview:doneButton];
        }
    }
}

- (void)doneButton:(id)sender
{
	NSLog(@"doneButton");
    [doneButton removeFromSuperview];
    [txtFldContact resignFirstResponder];
}

#pragma mark - NSURLConnection Delegate

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (!isCheckService)
    {
        [nsDataCountry setLength: 0];
    }
    else
    {
        [nsDataState setLength: 0];
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (!isCheckService)
    {
        [nsDataCountry appendData:data];
    }
    else
    {
        [nsDataState appendData:data];
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConenction");
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (!isCheckService)
    {
        NSLog(@"DONE. Received Bytes: %d", [nsDataCountry length]);
        self.arrayCountry = [NSJSONSerialization JSONObjectWithData:nsDataCountry options:NSJSONReadingMutableContainers error:nil];
        [self.arrayCountry removeObjectAtIndex:0];
    }
    else
    {
        NSLog(@"DONE. Received Bytes: %d", [nsDataState length]);
        self.arrayStates = [NSJSONSerialization JSONObjectWithData:nsDataState options:NSJSONReadingMutableContainers error:nil];
    }
   
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - UIPickerView Delegate methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    int count;
    if (!isCheckService)
    {
        count = [self.arrayCountry count];
    }
    else
    {
        count = [self.arrayStates count];
    }
	return count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30.0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, pickerViewCountry.frame.size.width, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:16];
    
    NSString *strValue;
    
    if (!isCheckService)
    {
        strValue = [[self.arrayCountry objectAtIndex:row] objectForKey:@"label"];
    }
    else
    {
        strValue = [[self.arrayStates objectAtIndex:row]objectForKey:@"label"];
    }
    
    label.text = [NSString stringWithFormat:@"%@", strValue];
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    iRow = row;
    
    if (!isCheckService)
    {
        lblCountryName.text = [[self.arrayCountry objectAtIndex:row] objectForKey:@"label"];
    }
    else
    {
        lblStateName.text = [[self.arrayStates objectAtIndex:row]objectForKey:@"label"];
        [pickerViewCountry setHidden:YES];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	return 300;
}

@end
