//
//  RegistrationViewController.m
//  IShop
//
//  Created by Avnish Sharma on 5/8/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "RegistrationViewController.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "MBProgressHUD.h"

@interface RegistrationViewController ()

@end

@implementation RegistrationViewController
@synthesize viewEmailValidation,viewTxtValidation,viewContent;
@synthesize mainScrollVIew;
@synthesize imgValidaation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(!IS_IPHONE5)
    {
        [self.mainScrollVIew setContentSize:CGSizeMake(self.view.frame.size.width, 530)];
    }
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    self.title = @"JOIN";
    UIImage *image=[UIImage imageNamed:@"remove.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnBackAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButton Methods

- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnNextStepAction:(id)sender
{
    NSString *password = txtPassword.text;
    NSString *confirmPassword = txtCnfmPassword.text;
    
    
    NSArray *viewsToRemove = [self.viewTxtValidation subviews];
    for (UIView *viewIn in viewsToRemove)
    {
        if([viewIn isKindOfClass:[UILabel class]])
        {
            UILabel *lbl = (UILabel *)viewIn;
            if(![lbl.text isEqualToString:@"The Following errors have occurred:"])
            {
                [lbl removeFromSuperview];
            }
        }
    }
    
    [self.viewTxtValidation removeFromSuperview];
    [self.viewEmailValidation removeFromSuperview];
    NSString *strNameValidation = @"";
    
    if([txtFName.text isEqualToString:@""])
    {
        strNameValidation = [strNameValidation stringByAppendingString:@"• First name has not been specified.^"];
    }
    
    if([txtLName.text isEqualToString:@""])
    {
        strNameValidation = [strNameValidation stringByAppendingString:@"• Last name has not been specified.^"];
    }
    if ([txtEmail.text length] > 0 )
    {
        if (![self emailValidation:txtEmail.text])
        {
            //show email validation view
            strNameValidation = [strNameValidation stringByAppendingString:@"• Email address has not been specified.^"];
        }
    }
    else
    {
        strNameValidation = [strNameValidation stringByAppendingString:@"• Email address has not been specified.^"];
    }
    if([txtPassword.text isEqualToString:@""])
    {
        strNameValidation = [strNameValidation stringByAppendingString:@"• The Password has not been specified.^"];
    }
    if ([txtPassword.text length] < 6 )
    {
        if([txtFName.text isEqualToString:@""] || [txtLName.text isEqualToString:@""] || !([txtEmail.text length] > 0) || ![self emailValidation:txtEmail.text] || [txtPassword.text isEqualToString:@""] )
        {
        }
        else
        {
            [self.mainScrollVIew setContentSize:CGSizeMake(self.view.frame.size.width, 650)];
            CGRect viewFrame = self.viewEmailValidation.frame;
            viewFrame.origin.x = 18;
            viewFrame.origin.y = 15;
            [self.viewEmailValidation setFrame:viewFrame];
            
            [self.mainScrollVIew addSubview:self.viewEmailValidation];
            
            CGRect viewContentFrame = self.viewContent.frame;
            viewContentFrame.origin.y = self.viewEmailValidation.frame.size.height + 10;
            self.viewContent.frame = viewContentFrame;
            return;
        }
    }
    if([txtCnfmPassword.text isEqualToString:@""])
    {
        strNameValidation = [strNameValidation stringByAppendingString:@"• The Confirm Password has not been specified.^"];
    }
    
    if(![password isEqualToString: confirmPassword])
    {
        strNameValidation = [strNameValidation stringByAppendingString:@"• Password Do not match."];
    }
    
    if(![strNameValidation isEqualToString:@""])
    {
        [self.mainScrollVIew setContentSize:CGSizeMake(self.view.frame.size.width, 650)];
        
        NSArray *newArray = [[NSArray alloc]init];
        newArray = [strNameValidation componentsSeparatedByString:@"^"];
        
        float yPosition = 25;
        
        for (int i = 0; i < [newArray count]; i++)
        {
            UILabel *lblMessage = [[UILabel alloc]initWithFrame:CGRectMake(6, yPosition, 280, 20)];
            lblMessage.text = [newArray objectAtIndex:i];
            yPosition = yPosition + 25;
            [lblMessage setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
            
            [self.viewTxtValidation addSubview:lblMessage];
        }
        
        //set validation view frame
        CGRect viewFrame = self.viewTxtValidation.frame;
        viewFrame.size.height = [newArray count] * 20;
        viewFrame.origin.x = 18;
        viewFrame.origin.y = 15;
        [self.viewTxtValidation setFrame:viewFrame];
        
        //set background image frame
        CGRect imgFrame = self.imgValidaation.frame;
        imgFrame.origin.x = 0;
        imgFrame.origin.y = 0;
        imgFrame.size.width =self.viewTxtValidation.frame.size.width;
        imgFrame.size.height =self.viewTxtValidation.frame.size.height + 25;
        self.imgValidaation.frame = imgFrame;
        [self.mainScrollVIew addSubview:self.viewTxtValidation];
        
        CGRect viewContentFrame = self.viewContent.frame;
        viewContentFrame.origin.y = self.viewTxtValidation.frame.size.height + 25;
        self.viewContent.frame = viewContentFrame;
        return;
    }
    
    if([password isEqualToString: confirmPassword])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading ...";
        hud.dimBackground = YES;
        
        NSURL *url = [NSURL URLWithString:user_registration];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue:txtEmail.text forKey:@"email"];
        [request setPostValue:txtPassword.text forKey:@"password"];
        [request setPostValue:txtFName.text forKey:@"fname"];
        [request setPostValue:txtLName.text forKey:@"lname"];
        [request setPostValue:@1 forKey:@"website_id"];
        [request setPostValue:@1 forKey:@"store_id"];
        [request setPostValue:@1 forKey:@"group_id"];
        [request setDidFinishSelector:@selector(requestFinishedForService:)];
        [request setDidFailSelector:@selector(requestFailed:)];
        [request setDelegate:self];
        [request startAsynchronous];
    }
}

#pragma mark - ASIHTTP Response

-(void)requestFinishedForService:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSString *string =[request responseString];
    NSData *data = [request responseData];
    
    NSLog(@"string -- %@",string);
    NSLog(@"data -- %@",data);
    
    NSMutableDictionary *mutableDictResponce = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
   // NSString *strAlert = [mutableDictResponce valueForKey:@"error"];
    
    if ([[mutableDictResponce objectForKey:@"error"] isEqualToString:@""] || [mutableDictResponce objectForKey:@"error"] == nil)
    {
        objAppDelegate.dictUserInfo = [mutableDictResponce copy];
        
        if ([[mutableDictResponce objectForKey:@"message"] isEqualToString:@"Invalid login or password."])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Invalid Login Id or password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            objAppDelegate.isUserLogin = YES;
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:txtEmail.text forKey:@"emailSave"];
            [userDefaults setValue:txtPassword.text forKey:@"passwordSave"];
            
            [userDefaults setObject:txtFName.text forKey:@"firstname"];
            [userDefaults setObject:txtLName.text forKey:@"lastname"];            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRemember"];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Thank you for your Registration on iShop." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if(![[mutableDictResponce objectForKey:@"error"] isEqualToString:@""])
    {
        [self.mainScrollVIew setContentSize:CGSizeMake(self.view.frame.size.width, 650)];
        CGRect viewFrame = self.viewEmailValidation.frame;
        viewFrame.origin.x = 18;
        viewFrame.origin.y = 15;
        [self.viewEmailValidation setFrame:viewFrame];
        
        [self.mainScrollVIew addSubview:self.viewEmailValidation];
        CGRect viewContentFrame = self.viewContent.frame;
        viewContentFrame.origin.y = self.viewEmailValidation.frame.size.height + 10;
        self.viewContent.frame = viewContentFrame;
        return;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}


-(void)requestFailed:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Check your network connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Uitextfield delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(!IS_IPHONE5)
    {
        if(textField == txtPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -120, 320 , 460)];
            [UIView commitAnimations];
        }
        else if(textField == txtCnfmPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -150, 320 , 460)];
            [UIView commitAnimations];
        }
    }
    else
    {
        if(textField == txtPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -120, 320 , 548)];
            [UIView commitAnimations];
        }
        else if(textField == txtCnfmPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0 , -150, 320 , 548)];
            [UIView commitAnimations];
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(!IS_IPHONE5)
    {
        if(textField == txtPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 480)];
            [UIView commitAnimations];
        }
        else if(textField == txtCnfmPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 480)];
            [UIView commitAnimations];
        }
    }
    else
    {
        if(textField == txtPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 568)];
            [UIView commitAnimations];
        }
        else if(textField == txtCnfmPassword)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [self.view setFrame:CGRectMake(0, 0, 320, 568)];
            [UIView commitAnimations];
        }
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - validations

-(BOOL)emailValidation:(NSString *)string
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:string];
}

#pragma mark - NSURLConnection Delegate

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [webData setLength: 0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [webData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConenction");
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"DONE. Received Bytes: %d", [webData length]);
}

@end
