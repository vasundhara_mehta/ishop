//
//  RegistrationViewController.h
//  IShop
//
//  Created by Avnish Sharma on 5/8/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface RegistrationViewController : UIViewController<UITextFieldDelegate>
{
    AppDelegate *objAppDelegate;
    //iVar uitextfield
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtFName;
    IBOutlet UITextField *txtLName;
    IBOutlet UITextField *txtCnfmPassword;
    
    //Mutable Data iVar
    NSMutableData *webData;
}
@property(nonatomic ,retain)IBOutlet UIScrollView *mainScrollVIew;
@property(nonatomic ,retain)IBOutlet UIView *viewEmailValidation,*viewTxtValidation,*viewContent;
@property(nonatomic ,retain)IBOutlet UIImageView *imgValidaation;
- (IBAction) btnNextStepAction:(id)sender;
- (IBAction) btnBackAction:(id)sender;

@end
