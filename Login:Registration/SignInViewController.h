//
//  SignInViewController.h
//  IShop
//
//  Created by Avnish Sharma on 5/8/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SignInViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>{
    
    AppDelegate *objAppDelegate;
    
    //iVar uitextfield
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    
    NSMutableDictionary *mutableDictResponce;
}

- (IBAction) actionSignIn:(id)sender;
- (IBAction) btnForgetPasswordAction:(id)sender;
- (IBAction) btnSignUpTodayAction:(id)sender;


@end
