//
//  SignInViewController.m
//  IShop
//
//  Created by Avnish Sharma on 5/8/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "SignInViewController.h"
#import "ASIFormDataRequest.h"
#import "MBProgressHUD.h"
#import "RegistrationViewController.h"
#import "AppDelegate.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    self.title = @"SIGN IN";
    UIImage *image=[UIImage imageNamed:@"remove.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnBackAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButton Methods

- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) actionSignIn:(id)sender
{
    if ([txtEmail.text length] > 0)
    {
        if (![self emailValidation:txtEmail.text])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please enter valid E-mail." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please enter E-mail." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if(![txtPassword.text length] > 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please provide Password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please Wait ...";
    hud.dimBackground = YES;
    
    [self login];
}

- (IBAction) btnForgetPasswordAction:(id)sender
{
   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://inspirefnb.com/fashionworld/customer/account/forgotpassword/"]];
}

-(void)login
{
    objAppDelegate.strCustomerPassword = txtPassword.text;
    
    NSString *strUrl = user_login;
    NSURL *url=[NSURL URLWithString:strUrl];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setPostValue:txtEmail.text forKey:@"email"];
    [request setPostValue:txtPassword.text forKey:@"password"];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDidFinishSelector:@selector(requestFinishedForService:)];
    [request startAsynchronous];
}

- (IBAction) btnSignUpTodayAction:(id)sender
{
    RegistrationViewController *objRegistrationViewController;
    if (IS_IPHONE5)
    {
        objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil];
    }
    else
    {
        objRegistrationViewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController_iphone3.5" bundle:nil];
    }
    [self.navigationController pushViewController:objRegistrationViewController animated:YES];
}

#pragma mark - ASIHTTP Response

-(void)requestFailed:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Check your network connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

- (void)requestFinishedForService:(ASIHTTPRequest *)theRequest
{
    //[self performSelector:@selector(killHUD) withObject:nil];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    // NSString *strResponse = [theRequest responseString];
    NSData *responseData = [theRequest responseData];
    
    NSMutableDictionary *mDictdata = [[NSMutableDictionary alloc] init];
    
    mDictdata = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    mutableDictResponce = [mDictdata valueForKey:@"customer_data"];
    
    objAppDelegate.dictUserInfo = [mutableDictResponce copy];
    
    if ([[mDictdata objectForKey:@"message"] isEqualToString:@"Invalid login or password."])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Invalid Login Id or password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        objAppDelegate.isUserLogin = YES;
        
        NSString *strCustid = [mDictdata valueForKey:@"customer_id"];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:txtEmail.text forKey:@"emailSave"];
        [userDefaults setValue:txtPassword.text forKey:@"passwordSave"];
        [userDefaults setBool:YES forKey:@"isRemember"];
        
        [userDefaults setObject:strCustid forKey:@"customer_id"];
        
        [userDefaults setObject:[mutableDictResponce valueForKey:@"firstname"] forKey:@"firstname"];
        [userDefaults setObject:[mutableDictResponce valueForKey:@"lastname"] forKey:@"lastname"];
        [userDefaults setObject:[mutableDictResponce valueForKey:@"email"] forKey:@"email"];
        [userDefaults synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Login Successful" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alert.tag = 101;
        
        [alert show];
    }
}

-(void)uploadFailed:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Check your network connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - validations

-(BOOL)emailValidation:(NSString *)string
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:string];
}

#pragma mark - Uitextfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        [appDelegate getCheckOutHistory];
        [appDelegate getFavoriteHistory];
        [self.navigationController popViewControllerAnimated:YES];

    }
}


@end
